package demos;
/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wallpaperfx.design.artifact.wallpaper.Wallpaper;
import wallpaperfx.design.control.MotifDesignControl;
import wallpaperfx.design.control.WallpaperDesignControl;
import wallpaperfx.design.core.Enums.SymmetryModelType;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P1;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P31M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4MG;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.designer.Designer;
import wallpaperfx.designrules.artfactory.ASCIArtFactory;
import wallpaperfx.designrules.artfactory.AnyArtFactory;
import wallpaperfx.designrules.artfactory.ArtFactory;
import wallpaperfx.designrules.rules.DesignContextBasic;
import wallpaperfx.designrules.rules.DesignContextCA;
import wallpaperfx.designrules.rules.DesignRule;
import wallpaperfx.designrules.rules.DesignRulesFactory;
import wallpaperfx.display.ControlPane;
import wallpaperfx.display.DesignPane;

/**
 *
 * @author Maher Elkhaldi
 */
public class RulePresets extends Application {

    public static double uSize = 50;
    public static double vSize = 50;
    public static int uCount = 5;
    public static int vCount = 5;

    public static Point2D oPoint = new Point2D(100, 100);
    public static Point2D uPoint = new Point2D(oPoint.getX() + uSize, oPoint.getY());
    public static Point2D vPoint = new Point2D(oPoint.getX(), oPoint.getY() + vSize);

    public ControlPane controlPane = new ControlPane();
    public DesignPane designPane = new DesignPane();

    public Scene artifactScene;

   
    Lattice lattice;
    Wallpaper wallpaper;
    DesignRule solidRule;
    DesignRule[] checkeredRule;
   
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    
    AnchorPane artifact = new AnchorPane();
    VBox sliderControlPanes = new VBox();
    Scene sliderScene = new Scene(sliderControlPanes);
    Stage sliderStage = new Stage();

    Button listArtwork = new Button("List Artwork");

    Button solid = new Button("Solid P31M");
    Button checkered = new Button("Checkered P1 and P4");

    Button anyRule = new Button("Any Rule");

    Button CARuleP4M = new Button("Apply CARule");
    Button shuffleP1P4 = new Button("Shuffle P1 & P4");
    Button randomizeSymmetry = new Button("RandomizeSymmetry");
    VBox ruleBox = new VBox(new Label("Preset Rules"), solid, checkered, CARuleP4M, anyRule, shuffleP1P4, randomizeSymmetry);
    TextArea consol = new TextArea();
    Button customRule = new Button("Run Solid Custom Rule");
    
    TextField ascii = new TextField("F");
    TextField style = new TextField("-fx-fill: black; -fx-font-size: 24;");
    
    SymmetryModelType type = P1;
    DesignRule customeRule;
    String res = "";
    Stage primaryStage;
    int lines = 0;
    DesignRule CARule;
    DesignContextCA CAcontext;
    
    ArtFactory factory = new ASCIArtFactory(new Text("∆"), "-fx-fill: gray;-fx-font-size: 24;");

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.consol.setWrapText(true);
        consol.prefHeightProperty().bind(sliderStage.heightProperty().divide(2));
        lattice = new Lattice(oPoint, uPoint, vPoint, uCount, vCount);

        //define example rules
        solidRule = new DesignRule(P4MG, new DesignContextBasic(1, 0, 1, 0), true, true, factory);
        checkeredRule = DesignRulesFactory.getCheckeredDesignRuleSet(P4, factory, P1, factory);

        CAcontext = new DesignContextCA(P31M, P31M, P31M, new AnyArtFactory(), new AnyArtFactory(), new AnyArtFactory());
        CARule = new DesignRule(P4M, CAcontext, true, true, factory);

        wallpaper = Designer.makeWallpaper(lattice, solidRule);

      
       
        //add wallpaper and lattice views to design pane
        designPane.addWallpaper(wallpaper.view);
        designPane.addLattice(lattice.view);

        //build control gui
        buildcustomRuleControlAndPrintSVG();
        buildPrebuiltRules();
        buildSliders();
        buildPanes();

        //prepare primary stage
        setPrimary();
        primaryStage.show();
        sliderStage.show();

    }

    private void setPrimary() {
        primaryStage.setScene(artifactScene);
        primaryStage.setWidth(700);
        primaryStage.setHeight(700);

        primaryStage.setX(600);
        primaryStage.setTitle("WallpaperFX Demo. Rules Presets. Design Panel");
    }

    

    private void buildPanes() {
        artifactScene = new Scene(artifact);
        artifact.getChildren().add(designPane);
        artifact.getChildren().add(controlPane);
    }

    private void buildcustomRuleControlAndPrintSVG() {
        
        customRule.setOnMouseClicked((MouseEvent event) -> {

            customeRule = DesignRulesFactory.getSolidDesignRule(type, true, true, new ASCIArtFactory(new Text(ascii.getText()), style.getText()));
            Designer.changeAndRunDesignRules(wallpaper, customeRule);
        });
        listArtwork.setOnMouseClicked((MouseEvent event) -> {
            consol.clear();
            lines = 0;
            ArrayList<Shape> shapes = Designer.flattenWallpaper(wallpaper);
            shapes.forEach(shape -> {
                lines += 1;
                consol.appendText("\n\n*" + lines + ":\tshape is " + shape);
            });
        });

    }

    private void buildPrebuiltRules() {
        solid.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, solidRule);
        });
        checkered.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, checkeredRule);
        });

        CARuleP4M.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, 1, CARule);
        });
        anyRule.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, DesignRulesFactory.getSolidAnyDesignRule(true, true, factory));
        });

        shuffleP1P4.setOnMouseClicked((MouseEvent event) -> {
            Designer.shuffleDesignRules(wallpaper, DesignRulesFactory.getSolidDesignRule(P4, true, true, factory), DesignRulesFactory.getSolidDesignRule(P1, true, true,factory));
        });

        randomizeSymmetry.setOnMouseClicked((MouseEvent event) -> {
            Designer.runRandomSymmetry(wallpaper, wallpaper.model.designRulesReadOnly().get(0).artFactory);
        });
       

    }

    private void buildSliders() {
        sliderControlPanes.getChildren().addAll(ruleBox, new Separator(Orientation.HORIZONTAL));
        sliderControlPanes.getChildren().addAll(new Label("Transformations"), new WallpaperDesignControl(controlPane, wallpaper), new MotifDesignControl(controlPane, wallpaper));
        sliderControlPanes.getChildren().addAll(new Separator(Orientation.HORIZONTAL), listArtwork, consol);
        sliderStage.setScene(sliderScene);
        sliderControlPanes.setSpacing(5);
        sliderStage.setWidth(300);
        sliderStage.setHeight(700);
        sliderStage.setX(300);
        sliderStage.setTitle("WallpaperFX Demo. Control Panel");

    }

    
}
