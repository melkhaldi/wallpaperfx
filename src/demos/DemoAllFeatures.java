package demos;
/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wallpaperfx.design.artifact.wallpaper.Wallpaper;
import wallpaperfx.design.control.CellDesignControl;
import wallpaperfx.design.control.LatticeDesignControl;
import wallpaperfx.design.control.MotifDesignControl;
import wallpaperfx.design.control.WallpaperDesignControl;
import wallpaperfx.design.core.Enums.Order;
import wallpaperfx.design.core.Enums.SymmetryModelType;
import static wallpaperfx.design.core.Enums.SymmetryModelType.CM;
import static wallpaperfx.design.core.Enums.SymmetryModelType.CMM;
import static wallpaperfx.design.core.Enums.SymmetryModelType.EMPTY;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P1;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P2;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P3;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P31M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P3M1;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4MG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P6;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P6M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PGG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PM;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PMG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PMM;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.designer.Designer;
import wallpaperfx.designrules.artfactory.ASCIArtFactory;
import wallpaperfx.designrules.artfactory.ArtFactory;
import wallpaperfx.designrules.rules.DesignContextBasic;
import wallpaperfx.designrules.rules.DesignContextCA;
import wallpaperfx.designrules.rules.DesignRule;
import wallpaperfx.designrules.rules.DesignRulesFactory;
import wallpaperfx.display.ControlPane;
import wallpaperfx.display.DesignPane;
import wallpaperfx.graphicrules.css.rules.CssRuleBasic;
import wallpaperfx.graphicrules.displayorder.rules.DisplayOrderRuleBasic;

/**
 *
 * @author Maher Elkhaldi
 */
public class DemoAllFeatures extends Application {

    public static double uSize = 50;
    public static double vSize = 50;
    public static int uCount = 2;
    public static int vCount = 2;

    public static Point2D oPoint = new Point2D(100, 100);
    public static Point2D uPoint = new Point2D(oPoint.getX() + uSize, oPoint.getY());
    public static Point2D vPoint = new Point2D(oPoint.getX(), oPoint.getY() + vSize);

    public ControlPane controlPane = new ControlPane();
    public DesignPane designPane = new DesignPane();

    public Scene artifactScene;

    public static CellDesignControl cellController;
    public static LatticeDesignControl latticeController;

    Lattice lattice;
    Wallpaper wallpaper;
    DesignRule solidRule;
    DesignRule[] checkeredRule;
    TextField CssRuleStyle = new TextField("-fx-fill: blue;");
    TextField CssRuleSeries = new TextField("-1");
    Button applyCss = new Button("Apply CSS Rule");

    TextField orderRuleDirection = new TextField("TOFRONT");
    TextField orderRuleSeries = new TextField("-1");
    Button applyOrder = new Button("Apply Order Rule");

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }


    AnchorPane artifact = new AnchorPane();
    VBox sliderControlPanes = new VBox();
    Scene sliderScene = new Scene(sliderControlPanes);
    Stage sliderStage = new Stage();

    Button listArtwork = new Button("List Artwork");

    Button solid = new Button("Solid P31M");
    Button checkered = new Button("Checkered P1 and P4M");

    Button anyRule = new Button("Any Rule");

    Button runCA = new Button("Apply CARule");
    Button shuffleP1P4 = new Button("Shuffle P1 & P4M");
    Button randomizeSymmetry = new Button("RandomizeSymmetry");
    VBox ruleBox = new VBox(new Label("Preset Rules"), solid, checkered, runCA, anyRule, shuffleP1P4, randomizeSymmetry);
    TextArea consol = new TextArea();
    ToggleButton cellViewToggle = new ToggleButton("Cell View Toggle Grid");
    ToggleButton latticeViewToggle = new ToggleButton("Lattice View Toggle Grid");
    VBox toggleBox = new VBox(new Label("Structural View"), cellViewToggle, latticeViewToggle);

    Button customRule = new Button("Run Solid Custom Rule");
    MenuItem mP1 = new MenuItem("P1");
    MenuItem mP2 = new MenuItem("P2");
    MenuItem mP3 = new MenuItem("P3");
    MenuItem mP3M1 = new MenuItem("P3M1");
    MenuItem mP31M = new MenuItem("P31M");
    MenuItem mP4 = new MenuItem("P4");
    MenuItem mP4M = new MenuItem("P4M");
    MenuItem mP4MG = new MenuItem("P4MG");
    MenuItem mP6 = new MenuItem("P6");
    MenuItem mP6M = new MenuItem("P6M");
    MenuItem mPM = new MenuItem("PM");
    MenuItem mPMM = new MenuItem("PMM");
    MenuItem mCMM = new MenuItem("CMM");
    MenuItem mPG = new MenuItem("PG");
    MenuItem mPMG = new MenuItem("PMG");
    MenuItem mPGG = new MenuItem("PGG");
    MenuItem mCM = new MenuItem("CM");
    MenuItem mEMPTY = new MenuItem("EMPTY");
    TextField ascii = new TextField("F");
    TextField style = new TextField("-fx-fill: black; -fx-font-size: 24;");
    SplitMenuButton symmetryType = new SplitMenuButton(mP1);

    VBox customRuleBox = new VBox(new Label("Custom Rule"), customRule, symmetryType, ascii, style, new Separator(Orientation.HORIZONTAL), new VBox(applyCss, CssRuleSeries, CssRuleStyle), new Separator(Orientation.HORIZONTAL), new VBox(applyOrder, orderRuleSeries, orderRuleDirection));
    SymmetryModelType type = P1;
    DesignRule customeRule;
    String res = "";
    Stage primaryStage;
    int lines = 0;
    DesignRule CARule;
    DesignContextCA CAcontext;

    ArtFactory factory = new ASCIArtFactory(new Text("∆"), "-fx-fill: gray;-fx-font-size: 24;");
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.consol.setWrapText(true);
        customRuleBox.setSpacing(10);
        consol.prefHeightProperty().bind(sliderStage.heightProperty().divide(2));
        lattice = new Lattice(oPoint, uPoint, vPoint, uCount, vCount);

        //define example rules
        solidRule = new DesignRule(P31M, new DesignContextBasic(1, 0, 1, 0), true, true, factory);
        checkeredRule = DesignRulesFactory.getCheckeredDesignRuleSet(P4M, factory, P1, factory);

        CAcontext = new DesignContextCA(P1, P1, P4M, factory, factory, factory);
        CARule = new DesignRule(P4M, CAcontext, true, true, factory);

        //build wallpaper
        wallpaper = Designer.makeWallpaper(lattice, solidRule);

        //set event on wallpaper
        wallpaper.view.setOnMousePressed((MouseEvent event) -> {
            if (event.getClickCount() > 1) {
                switchView();
            }
        });

        //add controllers
        latticeController = new LatticeDesignControl(controlPane, lattice);
        cellController = new CellDesignControl(controlPane, lattice);

        //bind lattice view to toggles.
        lattice.view.boundaryLoaded.bind(cellViewToggle.selectedProperty());
        lattice.view.orthoAxesLoaded.bind(cellViewToggle.selectedProperty());
        lattice.view.internalAxesLoaded.bind(cellViewToggle.selectedProperty());
        lattice.view.medianAxesLoaded.bind(cellViewToggle.selectedProperty());
        lattice.view.diagonalAxesLoaded.bind(cellViewToggle.selectedProperty());
        lattice.view.borderLoaded.bind(latticeViewToggle.selectedProperty());

        //add wallpaper and lattice views to design pane
        designPane.addWallpaper(wallpaper.view);
        designPane.addLattice(lattice.view);

        //build control gui
        buildSymmetryModelTypeMenu();
        buildcustomRuleControlAndPrintSVG();
        buildPrebuiltRules();
        buildSliders();
        buildPanes();

        //prepare primary stage
        setPrimary();
        switchView();
        primaryStage.show();
        sliderStage.show();

        lattice.model.cellModelLists.get(0).get(1).setPopup(new Group(new Text("I am Popup")), 0, 10, designPane, true);
    }

    private void setPrimary() {
        primaryStage.setScene(artifactScene);
        primaryStage.setWidth(1200);
        primaryStage.setHeight(1200);

        primaryStage.setX(600);
        primaryStage.setTitle("WallpaperFX Demo. Design Panel");
    }

    private void buildSymmetryModelTypeMenu() {
        symmetryType.getItems().addAll(mP2, mP3, mP3M1, mP31M, mP4, mP4M, mP4MG, mP6, mP6M, mPM, mPMM, mCMM, mCM, mPG, mPGG, mPMG, mEMPTY);

        symmetryType.setPrefWidth(120);
        type = P1;
        symmetryType.setText("P1");
        mP1.setOnAction((ActionEvent event1) -> {
            type = P1;
            symmetryType.setText("P1");
        });
        mP2.setOnAction((ActionEvent event1) -> {
            type = P2;
            symmetryType.setText("P2");
        });
        mP3.setOnAction((ActionEvent event1) -> {
            type = P3;
            symmetryType.setText("P3");
        });
        mP3M1.setOnAction((ActionEvent event1) -> {
            type = P3M1;
            symmetryType.setText("P3M1");
        });
        mP31M.setOnAction((ActionEvent event1) -> {
            type = P31M;
            symmetryType.setText("P31M");
        });
        mP4.setOnAction((ActionEvent event1) -> {
            type = P4;
            symmetryType.setText("P4");
        });
        mP4M.setOnAction((ActionEvent event1) -> {
            type = P4M;
            symmetryType.setText("P4M");
        });
        mP4MG.setOnAction((ActionEvent event1) -> {
            type = P4MG;
            symmetryType.setText("P4MG");

        });
        mP6.setOnAction((ActionEvent event1) -> {
            type = P6;
            symmetryType.setText("P6");
        });
        mP6M.setOnAction((ActionEvent event1) -> {
            type = P6M;
            symmetryType.setText("P6M");
        });
        mPM.setOnAction((ActionEvent event1) -> {
            type = PM;
            symmetryType.setText("PM");
        });
        mPMM.setOnAction((ActionEvent event1) -> {
            type = PMM;
            symmetryType.setText("PMM");
        });
        mCMM.setOnAction((ActionEvent event1) -> {
            type = CMM;
            symmetryType.setText("CMM");
        });
        mCM.setOnAction((ActionEvent event1) -> {
            type = CM;
            symmetryType.setText("CM");
        });
        mPG.setOnAction((ActionEvent event1) -> {
            type = PG;
            symmetryType.setText("PG");
        });
        mPMG.setOnAction((ActionEvent event1) -> {
            type = PMG;
            symmetryType.setText("PMG");
        });

        mPGG.setOnAction((ActionEvent event1) -> {
            type = PGG;
            symmetryType.setText("PGG ");
        });
        mEMPTY.setOnAction((ActionEvent event1) -> {
            type = EMPTY;
            symmetryType.setText("EMPTY");
        });
    }

    private void buildPanes() {
        artifactScene = new Scene(artifact);
        artifact.getChildren().add(designPane);
        artifact.getChildren().add(controlPane);
        designPane.getChildren().add(new Label("Double click on pattern to bring up interactive controls"));

    }

    private void buildcustomRuleControlAndPrintSVG() {
        customRuleBox.setSpacing(5);

        customRule.setOnMouseClicked((MouseEvent event) -> {
            customeRule = DesignRulesFactory.getSolidDesignRule(type, true, true, new ASCIArtFactory(new Text(ascii.getText()), style.getText()));
            Designer.changeAndRunDesignRules(wallpaper, customeRule);
        });
        listArtwork.setOnMouseClicked((MouseEvent event) -> {
            consol.clear();
            lines = 0;
            ArrayList<Shape> shapes = Designer.flattenWallpaper(wallpaper);
            shapes.forEach(shape -> {
                lines += 1;
                consol.appendText("\n\n*" + lines + ":\tshape is " + shape);
            });
        });

    }

    private void buildPrebuiltRules() {
        solid.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, solidRule);
        });
        checkered.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, checkeredRule);
        });

        runCA.setOnMouseClicked((MouseEvent event) -> {
            System.out.println("symmetry before");
            wallpaper.model.tileModelLists.forEach(list->{
                list.forEach(tile->{
                    System.out.println(tile.symmetryModel.symmetryModelType + "@ <" + tile.cellModel.getIndexI() +","+ tile.cellModel.getIndexJ()+">");
                });
            });
            Designer.changeAndRunDesignRules(wallpaper, 1, CARule);
            System.out.println("symmetry after");
            wallpaper.model.tileModelLists.forEach(list->{
                list.forEach(tile->{
                    System.out.println(tile.symmetryModel.symmetryModelType + "@ <" + tile.cellModel.getIndexI() +","+ tile.cellModel.getIndexJ()+">");
                });
            });
        });
        anyRule.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, DesignRulesFactory.getSolidAnyDesignRule(true, true, factory));
        });

        shuffleP1P4.setOnMouseClicked((MouseEvent event) -> {
            Designer.shuffleDesignRules(wallpaper, DesignRulesFactory.getSolidDesignRule(P4M, true, true, factory), DesignRulesFactory.getSolidDesignRule(P1, true, true, factory));
        });

        randomizeSymmetry.setOnMouseClicked((MouseEvent event) -> {
            Designer.runRandomSymmetry(wallpaper, wallpaper.model.designRulesReadOnly().get(0).artFactory);
        });
        applyCss.setOnMouseClicked((MouseEvent event) -> {
            String[] chops = CssRuleSeries.getText().split(",");
            int[] series = new int[chops.length];
            for (int i = 0; i < chops.length; i++) {
                String chop = chops[i];
                series[i] = Integer.valueOf(chop);
            }
            Designer.runCssRules(wallpaper, new CssRuleBasic(CssRuleStyle.getText(), series));

        });

        applyOrder.setOnMouseClicked((MouseEvent event) -> {
            Order order;
            if ("TOFRONT".equals(orderRuleDirection.getText())) {
                order = Order.TOFRONT;
            } else {
                order = Order.TOBACK;
            }
            String[] chops = orderRuleSeries.getText().split(",");
            int[] series = new int[chops.length];
            for (int i = 0; i < chops.length; i++) {
                String chop = chops[i];
                series[i] = Integer.valueOf(chop);
            }
            Designer.runDisPlayOrderRules(wallpaper, new DisplayOrderRuleBasic(order, series));

        });

    }

    private void buildSliders() {
        sliderControlPanes.getChildren().addAll(toggleBox, new Separator(Orientation.HORIZONTAL), ruleBox, new Separator(Orientation.HORIZONTAL));
        sliderControlPanes.getChildren().addAll(new Label("Transformations"), new WallpaperDesignControl(controlPane, wallpaper), new MotifDesignControl(controlPane, wallpaper));
        sliderControlPanes.getChildren().addAll(new Separator(Orientation.HORIZONTAL), customRuleBox, new Separator(Orientation.HORIZONTAL), listArtwork, consol);
        sliderStage.setScene(sliderScene);
        sliderControlPanes.setSpacing(5);
        sliderStage.setWidth(300);
        sliderStage.setHeight(1200);
        sliderStage.setX(300);
        sliderStage.setTitle("WallpaperFX Demo. Control Panel");

    }

    private void switchView() {
        cellController.setLoaded(!cellController.isLoaded());
        latticeController.setLoaded(!latticeController.isLoaded());
        if (!lattice.model.cellModelLists.get(0).get(1).popup.isShowing()) {
            lattice.model.cellModelLists.get(0).get(1).showPopup();
        } else {
            lattice.model.cellModelLists.get(0).get(1).hidePopup();
        }
    }
}
