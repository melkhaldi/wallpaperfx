package demos;
/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wallpaperfx.design.artifact.wallpaper.Wallpaper;
import wallpaperfx.design.control.CellDesignControl;
import wallpaperfx.design.control.LatticeDesignControl;
import wallpaperfx.design.control.MotifDesignControl;
import wallpaperfx.design.control.WallpaperDesignControl;
import wallpaperfx.design.core.Enums.Order;
import wallpaperfx.design.core.Enums.SymmetryModelType;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P6M;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.designer.Designer;
import wallpaperfx.designrules.artfactory.ASCIArtFactory;
import wallpaperfx.designrules.rules.DesignRule;
import wallpaperfx.designrules.rules.DesignRulesFactory;
import wallpaperfx.display.ControlPane;
import wallpaperfx.display.DesignPane;
import wallpaperfx.graphicrules.css.rules.CssRuleBasic;
import wallpaperfx.graphicrules.displayorder.rules.DisplayOrderRuleBasic;

/**
 *
 * @author Maher Elkhaldi
 */
public class FactoryAndOtherRules extends Application {

    public static double uSize = 50;
    public static double vSize = 50;
    public static int uCount = 2;
    public static int vCount = 2;

    public static Point2D oPoint = new Point2D(100, 100);
    public static Point2D uPoint = new Point2D(oPoint.getX() + uSize, oPoint.getY());
    public static Point2D vPoint = new Point2D(oPoint.getX(), oPoint.getY() + vSize);

    public ControlPane controlPane = new ControlPane();
    public DesignPane designPane = new DesignPane();

    public Scene artifactScene;

    public static CellDesignControl cellController;
    public static LatticeDesignControl latticeController;

    Lattice lattice;
    Wallpaper wallpaper;
    DesignRule solidRule;
    TextField CssRuleStyle = new TextField("-fx-fill: blue;");
    TextField CssRuleSeries = new TextField("-1");
    Button applyCss = new Button("Apply CSS Rule");

    TextField orderRuleDirection = new TextField("TOFRONT");
    TextField orderRuleSeries = new TextField("-1");
    Button applyOrder = new Button("Apply Order Rule");

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    
    AnchorPane artifact = new AnchorPane();
    VBox sliderControlPanes = new VBox();
    Scene sliderScene = new Scene(sliderControlPanes);
    Stage sliderStage = new Stage();

    Button customRule = new Button("Run Solid Custom Rule");
    
    TextField ascii = new TextField("F");
    TextField style = new TextField("-fx-fill: black; -fx-font-size: 24;");

    VBox customRuleBox = new VBox(new Label("Custom Rule"), customRule, ascii, style, new Separator(Orientation.HORIZONTAL), new VBox(applyCss, CssRuleSeries, CssRuleStyle), new Separator(Orientation.HORIZONTAL), new VBox(applyOrder, orderRuleSeries, orderRuleDirection));
    SymmetryModelType type = P6M;
    String res = "";
    Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        customRuleBox.setSpacing(10);
        lattice = new Lattice(oPoint, uPoint, vPoint, uCount, vCount);

        solidRule=  DesignRulesFactory.getSolidDesignRule(type, true, true, new ASCIArtFactory(new Text(ascii.getText()), style.getText()));
        
//build wallpaper
        wallpaper = Designer.makeWallpaper(lattice, solidRule);

        //set event on wallpaper
        wallpaper.view.setOnMousePressed((MouseEvent event) -> {
            if (event.getClickCount() > 1) {
                switchView();
            }
        });

        //add controllers
        latticeController = new LatticeDesignControl(controlPane, lattice);
        cellController = new CellDesignControl(controlPane, lattice);

        
        //add wallpaper and lattice views to design pane
        designPane.addWallpaper(wallpaper.view);
        designPane.addLattice(lattice.view);

        //build control gui
        buildcustomRuleControlAndPrintSVG();
        buildPrebuiltRules();
        buildSliders();
        buildPanes();

        //prepare primary stage
        setPrimary();
        switchView();
        primaryStage.show();
        sliderStage.show();

        lattice.model.cellModelLists.get(0).get(1).setPopup(new Group(new Text("I am Popup")), 0, 10, designPane, true);
    }

    private void setPrimary() {
        primaryStage.setScene(artifactScene);
        primaryStage.setWidth(600);
        primaryStage.setHeight(600);

        primaryStage.setX(600);
        primaryStage.setTitle("WallpaperFX Demo. Factory And Other Rules. Design Panel");
    }

   

    private void buildPanes() {
        artifactScene = new Scene(artifact);
        artifact.getChildren().add(designPane);
        artifact.getChildren().add(controlPane);
        designPane.getChildren().add(new Label("Double click on pattern to bring up interactive controls"));

    }

    private void buildcustomRuleControlAndPrintSVG() {
        customRuleBox.setSpacing(5);

        customRule.setOnMouseClicked((MouseEvent event) -> {

            Designer.changeAndRunDesignRules(wallpaper, DesignRulesFactory.getSolidDesignRule(type, true, true, new ASCIArtFactory(new Text(ascii.getText()), style.getText())));
        });
       

    }

    private void buildPrebuiltRules() {
        
        applyCss.setOnMouseClicked((MouseEvent event) -> {
            String[] chops = CssRuleSeries.getText().split(",");
            int[] series = new int[chops.length];
            for (int i = 0; i < chops.length; i++) {
                String chop = chops[i];
                series[i] = Integer.valueOf(chop);
            }
            Designer.runCssRules(wallpaper, new CssRuleBasic(CssRuleStyle.getText(), series));

        });

        applyOrder.setOnMouseClicked((MouseEvent event) -> {
            Order order;
            if ("TOFRONT".equals(orderRuleDirection.getText())) {
                order = Order.TOFRONT;
            } else {
                order = Order.TOBACK;
            }
            String[] chops = orderRuleSeries.getText().split(",");
            int[] series = new int[chops.length];
            for (int i = 0; i < chops.length; i++) {
                String chop = chops[i];
                series[i] = Integer.valueOf(chop);
            }
            Designer.runDisPlayOrderRules(wallpaper, new DisplayOrderRuleBasic(order, series));

        });

    }

    private void buildSliders() {
        sliderControlPanes.getChildren().addAll(new Label("Transformations"), new WallpaperDesignControl(controlPane, wallpaper), new MotifDesignControl(controlPane, wallpaper));
        sliderControlPanes.getChildren().addAll(new Separator(Orientation.HORIZONTAL), customRuleBox);
        sliderStage.setScene(sliderScene);
        sliderControlPanes.setSpacing(5);
        sliderStage.setWidth(300);
        sliderStage.setHeight(600);
        sliderStage.setX(300);
        sliderStage.setTitle("WallpaperFX Demo. Control Panel");

    }

    private void switchView() {
        cellController.setLoaded(!cellController.isLoaded());
        latticeController.setLoaded(!latticeController.isLoaded());
        if (!lattice.model.cellModelLists.get(0).get(1).popup.isShowing()) {
            lattice.model.cellModelLists.get(0).get(1).showPopup();
        } else {
            lattice.model.cellModelLists.get(0).get(1).hidePopup();
        }
    }
}
