package demos;

/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import static javafx.scene.input.MouseButton.SECONDARY;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wallpaperfx.design.artifact.wallpaper.Wallpaper;
import wallpaperfx.design.control.CellDesignControl;
import wallpaperfx.design.control.LatticeDesignControl;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P31M;
import wallpaperfx.design.core.MouseController;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.designer.Designer;
import wallpaperfx.designrules.artfactory.ASCIArtFactory;
import wallpaperfx.designrules.rules.DesignContextBasic;
import wallpaperfx.designrules.rules.DesignRule;
import wallpaperfx.display.ControlPane;
import wallpaperfx.display.DesignPane;

/**
 *
 * @author Maher Elkhaldi
 */
public class Teleporation extends Application {

    public static double uSize = 50;
    public static double vSize = 50;
    public static int uCount = 2;
    public static int vCount = 2;

    public static Point2D oPoint = new Point2D(100, 100);
    public static Point2D uPoint = new Point2D(oPoint.getX() + uSize, oPoint.getY());
    public static Point2D vPoint = new Point2D(oPoint.getX(), oPoint.getY() + vSize);

    public ControlPane controlPane = new ControlPane();
    public DesignPane designPane = new DesignPane();

    public Scene artifactScene;

    public static CellDesignControl cellController;
    public static LatticeDesignControl latticeController;

    Lattice lattice;
    Wallpaper wallpaper;
    DesignRule solidRule;
    
    Button teleportBlack = new Button("Teleport Mouse To Black Circle");
    Button teleportBlue = new Button("Teleport Mouse To Blue Circle");
    Button teleportToCell00 = new Button("Teleport Mouse To Cell 0,0");
    
    Circle blueAnchor = new Circle(10);
    Circle blackAnchor = new Circle(10);
    
    VBox toggleBox = new VBox(new Label("Teleporation"), teleportBlack, teleportBlue, teleportToCell00);

    AnchorPane artifact = new AnchorPane();
    Scene sliderScene = new Scene(toggleBox);
    Stage sliderStage = new Stage();

    Stage primaryStage;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        lattice = new Lattice(oPoint, uPoint, vPoint, uCount, vCount);

        //define example rules
        solidRule = new DesignRule(P31M, new DesignContextBasic(1, 0, 1, 0), true, true, new ASCIArtFactory(new Text("3"), "-fx-fill: gray;-fx-font-size: 24;"));

        //build wallpaper
        wallpaper = Designer.makeWallpaper(lattice, solidRule);

        //set event on wallpaper
        wallpaper.view.setOnMousePressed((MouseEvent event) -> {
            if (event.getClickCount() > 1) {
                switchView();
            }
        });

        //add controllers
        latticeController = new LatticeDesignControl(controlPane, lattice);
        cellController = new CellDesignControl(controlPane, lattice);

        //bind lattice view to toggles.
        lattice.view.setElementVisible(true);
        lattice.view.setSubElementsVisible(false);

        //add wallpaper and lattice views to design pane
        designPane.addWallpaper(wallpaper.view);
        designPane.addLattice(lattice.view);

        //build control gui
        buildSliders();
        buildPanes();

        //prepare primary stage
        setPrimary();
        switchView();
        primaryStage.show();
        sliderStage.show();

        designPane.getChildren().add(blackAnchor);
        designPane.getChildren().add(blueAnchor);

        designPane.prefWidthProperty().bind(primaryStage.widthProperty());
        designPane.prefHeightProperty().bind(primaryStage.heightProperty());
        
        blackAnchor.setTranslateX(lattice.model.cellModelLists.get(0).get(0).getPointCX());
        blackAnchor.setTranslateY(lattice.model.cellModelLists.get(0).get(0).getPointCY());
        
        
        blueAnchor.setTranslateX(lattice.model.cellModelLists.get(1).get(1).getPointCX());
        blueAnchor.setTranslateY(lattice.model.cellModelLists.get(1).get(1).getPointCY());
        
        
        teleportBlack.setOnMouseClicked((MouseEvent event) -> {
            MouseController.positionMouseAtNode(blackAnchor);
        });
        teleportBlue.setOnMouseClicked((MouseEvent event) -> {
            MouseController.positionMouseAtNode(blueAnchor);
        });
        blueAnchor.setStyle("-fx-fill:blue;");
        designPane.setOnMouseClicked((MouseEvent event) -> {
            MouseButton b = event.getButton();
            if(b.equals(SECONDARY)){
                MouseController.addAndPositionNodeAtMouse(blueAnchor, designPane);
            }
        });
        
        teleportToCell00.setOnMouseClicked((MouseEvent event) -> {
            MouseController.positionMouseAtCellModel(lattice, 0,0, 5, 5);
        });

    }

    private void setPrimary() {
        primaryStage.setScene(artifactScene);
        primaryStage.setWidth(400);
        primaryStage.setHeight(400);

        primaryStage.setX(600);
        primaryStage.setTitle("WallpaperFX Demo: View And Controls. Design Panel");
    }

    private void buildPanes() {
        artifactScene = new Scene(artifact);
        artifact.getChildren().add(designPane);
        artifact.getChildren().add(controlPane);
        designPane.getChildren().add(new Label("Double click on pattern to bring up interactive controls.\nRight Click to Posiyion Blue Anchor"));

    }

    private void buildSliders() {
        sliderStage.setScene(sliderScene);
        sliderStage.setWidth(300);
        sliderStage.setHeight(200);
        sliderStage.setX(300);
        sliderStage.setTitle("WallpaperFX Demo. Control Panel");

    }

    private void switchView() {
        cellController.setLoaded(!cellController.isLoaded());
        latticeController.setLoaded(!latticeController.isLoaded());
    }
}
