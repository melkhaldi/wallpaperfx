package demos;
/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wallpaperfx.design.artifact.wallpaper.Wallpaper;
import wallpaperfx.design.control.CellDesignControl;
import wallpaperfx.design.control.LatticeDesignControl;
import wallpaperfx.design.control.MotifDesignControl;
import wallpaperfx.design.control.WallpaperDesignControl;
import wallpaperfx.design.core.Enums.SymmetryModelType;
import static wallpaperfx.design.core.Enums.SymmetryModelType.CM;
import static wallpaperfx.design.core.Enums.SymmetryModelType.CMM;
import static wallpaperfx.design.core.Enums.SymmetryModelType.EMPTY;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P1;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P2;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P3;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P31M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P3M1;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4MG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P6;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P6M;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PGG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PM;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PMG;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PMM;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.designer.Designer;
import wallpaperfx.designrules.artfactory.ASCIArtFactory;
import wallpaperfx.designrules.artfactory.ArtFactory;
import wallpaperfx.designrules.rules.DesignRule;
import wallpaperfx.designrules.rules.DesignRulesFactory;
import wallpaperfx.display.ControlPane;
import wallpaperfx.display.DesignPane;

/**
 *
 * @author Maher Elkhaldi
 */
public class SymmetryGroupSelector extends Application {

    public static double uSize = 50;
    public static double vSize = 50;
    public static int uCount = 5;
    public static int vCount = 5;

    public static Point2D oPoint = new Point2D(150, 150);
    public static Point2D uPoint = new Point2D(oPoint.getX() + uSize, oPoint.getY());
    public static Point2D vPoint = new Point2D(oPoint.getX(), oPoint.getY() + vSize);

    public ControlPane controlPane = new ControlPane();
    public DesignPane designPane = new DesignPane();

    public Scene artifactScene;

    public static CellDesignControl cellController;
    public static LatticeDesignControl latticeController;

    Lattice lattice;
    Wallpaper wallpaper;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    AnchorPane artifact = new AnchorPane();
    VBox sliderControlPanes = new VBox();
    Scene sliderScene = new Scene(sliderControlPanes);
    Stage sliderStage = new Stage();

    MenuItem mP1 = new MenuItem("P1");
    MenuItem mP2 = new MenuItem("P2");
    MenuItem mP3 = new MenuItem("P3");
    MenuItem mP3M1 = new MenuItem("P3M1");
    MenuItem mP31M = new MenuItem("P31M");
    MenuItem mP4 = new MenuItem("P4");
    MenuItem mP4M = new MenuItem("P4M");
    MenuItem mP4MG = new MenuItem("P4MG");
    MenuItem mP6 = new MenuItem("P6");
    MenuItem mP6M = new MenuItem("P6M");
    MenuItem mPM = new MenuItem("PM");
    MenuItem mPMM = new MenuItem("PMM");
    MenuItem mCMM = new MenuItem("CMM");
    MenuItem mPG = new MenuItem("PG");
    MenuItem mPMG = new MenuItem("PMG");
    MenuItem mPGG = new MenuItem("PGG");
    MenuItem mCM = new MenuItem("CM");
    MenuItem mEMPTY = new MenuItem("EMPTY");
    SplitMenuButton symmetryType = new SplitMenuButton(mP1);
    Button runRule = new Button("Run");
    VBox groupList = new VBox(new Label("Select Symmetry Group"), new HBox(symmetryType, runRule));
    SymmetryModelType type = P1;
    String res = "";
    Stage primaryStage;
    DesignRule firstRule;

    ArtFactory factory = new ASCIArtFactory(new Text("∆"), "-fx-fill: gray;-fx-font-size: 24;");
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        lattice = new Lattice(oPoint, uPoint, vPoint, uCount, vCount);

        firstRule = DesignRulesFactory.getSolidAnyDesignRule(true, true, factory);
        type = firstRule.symmetryType;
        symmetryType.setText(type.name());

        wallpaper = Designer.makeWallpaper(lattice, firstRule);
        //set event on wallpaper
        wallpaper.view.setOnMousePressed((MouseEvent event) -> {
            if (event.getClickCount() > 1) {
                switchView();
            }
        });

        //add controllers
        latticeController = new LatticeDesignControl(controlPane, lattice);
        cellController = new CellDesignControl(controlPane, lattice);

        //add wallpaper and lattice views to design pane
        designPane.addWallpaper(wallpaper.view);
        designPane.addLattice(lattice.view);

        //build control gui
        buildSymmetryModelTypeMenu();
        buildSliders();
        buildPanes();

        //prepare primary stage
        setPrimary();
        switchView();
        primaryStage.show();
        sliderStage.show();
        
        lattice.view.setSubElementsVisible(false);

    }

    private void setPrimary() {
        primaryStage.setScene(artifactScene);
        primaryStage.setWidth(600);
        primaryStage.setHeight(600);

        primaryStage.setX(600);
        primaryStage.setTitle("WallpaperFX Demo. Symmetry Group Selector. Design Panel");
    }

    private void buildSymmetryModelTypeMenu() {
        symmetryType.getItems().addAll(mP2, mP3, mP3M1, mP31M, mP4, mP4M, mP4MG, mP6, mP6M, mPM, mPMM, mCMM, mCM, mPG, mPGG, mPMG, mEMPTY);

        symmetryType.setPrefWidth(120);
        runRule.setOnMouseClicked((MouseEvent event) -> {
            Designer.changeAndRunDesignRules(wallpaper, DesignRulesFactory.getSolidDesignRule(type, true, true, factory));
        });
        mP1.setOnAction((ActionEvent event1) -> {
            type = P1;
            symmetryType.setText("P1");
        });
        mP2.setOnAction((ActionEvent event1) -> {
            type = P2;
            symmetryType.setText("P2");
        });
        mP3.setOnAction((ActionEvent event1) -> {
            type = P3;
            symmetryType.setText("P3");
        });
        mP3M1.setOnAction((ActionEvent event1) -> {
            type = P3M1;
            symmetryType.setText("P3M1");
        });
        mP31M.setOnAction((ActionEvent event1) -> {
            type = P31M;
            symmetryType.setText("P31M");
        });
        mP4.setOnAction((ActionEvent event1) -> {
            type = P4;
            symmetryType.setText("P4");
        });
        mP4M.setOnAction((ActionEvent event1) -> {
            type = P4M;
            symmetryType.setText("P4M");
        });
        mP4MG.setOnAction((ActionEvent event1) -> {
            type = P4MG;
            symmetryType.setText("P4MG");

        });
        mP6.setOnAction((ActionEvent event1) -> {
            type = P6;
            symmetryType.setText("P6");
        });
        mP6M.setOnAction((ActionEvent event1) -> {
            type = P6M;
            symmetryType.setText("P6M");
        });
        mPM.setOnAction((ActionEvent event1) -> {
            type = PM;
            symmetryType.setText("PM");
        });
        mPMM.setOnAction((ActionEvent event1) -> {
            type = PMM;
            symmetryType.setText("PMM");
        });
        mCMM.setOnAction((ActionEvent event1) -> {
            type = CMM;
            symmetryType.setText("CMM");
        });
        mCM.setOnAction((ActionEvent event1) -> {
            type = CM;
            symmetryType.setText("CM");
        });
        mPG.setOnAction((ActionEvent event1) -> {
            type = PG;
            symmetryType.setText("PG");
        });
        mPMG.setOnAction((ActionEvent event1) -> {
            type = PMG;
            symmetryType.setText("PMG");
        });

        mPGG.setOnAction((ActionEvent event1) -> {
            type = PGG;
            symmetryType.setText("PGG ");
        });
        mEMPTY.setOnAction((ActionEvent event1) -> {
            type = EMPTY;
            symmetryType.setText("EMPTY");
        });
    }

    private void buildPanes() {
        artifactScene = new Scene(artifact);
        artifact.getChildren().add(designPane);
        artifact.getChildren().add(controlPane);
        designPane.getChildren().add(new Label("Double click on pattern to bring up interactive controls"));

    }

    private void buildSliders() {
        sliderControlPanes.getChildren().addAll(groupList, new Label("Transformations"), new WallpaperDesignControl(controlPane, wallpaper), new MotifDesignControl(controlPane, wallpaper));
        sliderStage.setScene(sliderScene);
        sliderControlPanes.setSpacing(5);
        sliderStage.setWidth(300);
        sliderStage.setHeight(600);
        sliderStage.setX(300);
        sliderStage.setTitle("WallpaperFX Demo. Control Panel");

    }

    private void switchView() {
        cellController.setLoaded(!cellController.isLoaded());
        latticeController.setLoaded(!latticeController.isLoaded());
        if (!lattice.model.cellModelLists.get(0).get(1).popup.isShowing()) {
            lattice.model.cellModelLists.get(0).get(1).showPopup();
        } else {
            lattice.model.cellModelLists.get(0).get(1).hidePopup();
        }
    }
}
