/*
 * The MIT License
 *
 * Copyright 2016 melkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.designer;

import java.util.ArrayList;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.shape.Shape;
import wallpaperfx.design.artifact.tile.TileController;
import wallpaperfx.design.artifact.wallpaper.Wallpaper;
import static wallpaperfx.design.core.Enums.Order.TOFRONT;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.designrules.artfactory.ArtFactory;
import wallpaperfx.designrules.rules.DesignRule;
import wallpaperfx.designrules.rules.DesignRulesFactory;
import wallpaperfx.graphicrules.css.rules.CssRule;
import wallpaperfx.graphicrules.displayorder.rules.DisplayOrderRuleBasic;

/**
 *
 * @author melkhaldi
 */
public class Designer {

    private final static ArrayList<Wallpaper> MANAGED = new ArrayList<>();

    public static Wallpaper makeWallpaper(Lattice lattice, DesignRule initDesignRule) {
        final Wallpaper wallpaper = new Wallpaper(lattice, initDesignRule);
        addToManagaed(wallpaper);
        applyDesignRules(wallpaper);
        fillWallpaperView(wallpaper);
        return wallpaper;
    }

    public static Wallpaper makeWallpaper(Lattice lattice, DesignRule initDesignRule, DesignRule... rules) {
        final Wallpaper wallpaper = new Wallpaper(lattice, initDesignRule, rules);
        addToManagaed(wallpaper);
        applyDesignRules(wallpaper);
        fillWallpaperView(wallpaper);
        return wallpaper;
    }

    public static final void changeAndRunDesignRules(Wallpaper wallpaper, DesignRule... newRules) {
        wallpaper.model.changeDesignRules(newRules);
        applyDesignRules(wallpaper);
        fillWallpaperView(wallpaper);
        wallpaper.controller.updateRelease();
    }

    public static final void shuffleDesignRules(Wallpaper wallpaper, DesignRule... newRules) {
        wallpaper.model.changeDesignRules(newRules);
        DesignRule designRule;
        for (int i = 0; i < wallpaper.controller.tileControllerLists.size(); i++) {
            for (int j = 0; j < wallpaper.controller.tileControllerLists.get(i).size(); j++) {
                designRule = wallpaper.model.designRulesReadOnly().get((int) Math.round((Math.random() * (wallpaper.model.designRulesReadOnly().size() - 1))));
                if (designRule.overriding) {
                    wallpaper.controller.tileControllerLists.get(i).get(j).view.dispose();
                    wallpaper.controller.tileControllerLists.get(i).get(j).motifController.dispose();
                    wallpaper.setDesignRule(i, j, designRule);
                    wallpaper.controller.tileControllerLists.get(i).set(j, new TileController(wallpaper.model.tileModelLists.get(i).get(j)));
                }
            }
        }
        fillWallpaperView(wallpaper);
        wallpaper.controller.updateRelease();
    }

    public static final void runRandomSymmetry(Wallpaper wallpaper, ArtFactory factory) {
        wallpaper.model.clearDesignRuleListExceptDefaultDesignRule();
        for (int i = 0; i < wallpaper.controller.tileControllerLists.size(); i++) {
            for (int j = 0; j < wallpaper.controller.tileControllerLists.get(i).size(); j++) {
                DesignRule designRule = DesignRulesFactory.getSolidAnyDesignRule(true, true, factory);
                wallpaper.model.addDesignRuleToList(designRule);
                if (designRule.overriding) {
                    wallpaper.controller.tileControllerLists.get(i).get(j).view.dispose();
                    wallpaper.controller.tileControllerLists.get(i).get(j).motifController.dispose();
                    wallpaper.setDesignRule(i, j, designRule);
                    wallpaper.controller.tileControllerLists.get(i).set(j, new TileController(wallpaper.model.tileModelLists.get(i).get(j)));
                }
            }
        }
        fillWallpaperView(wallpaper);
        wallpaper.controller.updateRelease();
    }

    public static final void changeAndRunDesignRules(Wallpaper wallpaper, int iterations, DesignRule... newRules) {
        wallpaper.model.changeDesignRules(newRules);
        for (int i = 0; i < iterations; i++) {
            applyDesignRules(wallpaper);
        }
        fillWallpaperView(wallpaper);
        wallpaper.controller.updateRelease();
    }

    public static final void addAndRunDesignRules(Wallpaper wallpaper, int iterations, DesignRule... newRules) {
        wallpaper.model.addDesignRules(newRules);
        for (int i = 0; i < iterations; i++) {
            applyDesignRules(wallpaper);
        }
        fillWallpaperView(wallpaper);
        wallpaper.controller.updateRelease();
    }

    public static void runCssRules(Wallpaper wallpaper, CssRule... cssRules) {
        for (CssRule cssRule : cssRules) {
            applyCssRule(wallpaper, cssRule);
        }
    }

    public static void runDisPlayOrderRules(Wallpaper wallpaper, DisplayOrderRuleBasic... displayOrderRules) {
        for (DisplayOrderRuleBasic displayOrderRule : displayOrderRules) {
            applyDisplayOrderRule(wallpaper, displayOrderRule);
        }
    }

    public static ArrayList<Shape> flattenWallpaper(Wallpaper wallpaper) {
        ArrayList<Shape> shapes = new ArrayList();
        wallpaper.controller.tileControllerLists.forEach(controllerList -> {
            controllerList.forEach(tileController -> {
                tileController.motifController.artworkControllers.forEach(artController -> {
                    artController.view.getChildren().forEach(child -> {
                        shapes.add((Shape) child);
                    });
                });
            });
        });
        return shapes;
    }

    private static void addToManagaed(Wallpaper wallpaper) {
        if (!MANAGED.contains(wallpaper)) {
            MANAGED.add(wallpaper);
            wallpaper.controller.releaseProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                fillWallpaperView(wallpaper);
            });
        }
    }

    private static void applyDesignRules(Wallpaper wallpaper) {
        wallpaper.model.designRulesReadOnly().forEach(designRule -> {
            applyDesignRule(wallpaper, designRule);
        });
    }

    private static void applyDesignRule(Wallpaper wallpaper, DesignRule designRule) {
        for (int i = 0; i < wallpaper.controller.tileControllerLists.size(); i++) {
            for (int j = 0; j < wallpaper.controller.tileControllerLists.get(i).size(); j++) {
                if (designRule.overriding && designRule.context.isSatisfied(i, j, wallpaper.controller)) {
                    wallpaper.controller.tileControllerLists.get(i).get(j).view.dispose();
                    wallpaper.controller.tileControllerLists.get(i).get(j).motifController.dispose();
                    wallpaper.setDesignRule(i, j, designRule);
                    wallpaper.controller.tileControllerLists.get(i).set(j, new TileController(wallpaper.model.tileModelLists.get(i).get(j)));
                }
            }
        }
    }

    private static void applyCssRule(Wallpaper wallpaper, CssRule cssRule) {
        for (int i = 0; i < wallpaper.controller.tileControllerLists.size(); i++) {
            for (int j = 0; j < wallpaper.controller.tileControllerLists.get(i).size(); j++) {
                if (cssRule.overriding) {
                    if (cssRule.itemSeries.length == 1 && cssRule.itemSeries[0] == -1) {
                        for (int l = 0; l < wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.size(); l++) {
                            for (int m = 0; m < wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.getChildren().size(); m++) {
                                wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.getChildren().get(m).setStyle(cssRule.cssStyle);
                            }
                        }
                    } else {
                        for (int k = 0; k < cssRule.itemSeries.length; k++) {
                            for (int l = 0; l < wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.size(); l++) {
                                for (int m = 0; m < wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.getChildren().size(); m++) {
                                    if (l == cssRule.itemSeries[k]) {
                                        wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.getChildren().get(m).setStyle(cssRule.cssStyle);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void applyDisplayOrderRule(Wallpaper wallpaper, DisplayOrderRuleBasic displayOrderRule) {
        for (int i = 0; i < wallpaper.controller.tileControllerLists.size(); i++) {
            for (int j = 0; j < wallpaper.controller.tileControllerLists.get(i).size(); j++) {
                if (displayOrderRule.overriding) {
                    if (displayOrderRule.itemSeries.length == 1 && displayOrderRule.itemSeries[0] == -1) {
                        for (int l = 0; l < wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.size(); l++) {
                            if (displayOrderRule.order.equals(TOFRONT)) {
                                wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.toFront();
                            } else {
                                wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.toBack();
                            }
                        }
                    } else {
                        for (int k = 0; k < displayOrderRule.itemSeries.length; k++) {
                            for (int l = 0; l < wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.size(); l++) {
                                if (l == displayOrderRule.itemSeries[k]) {
                                    if (displayOrderRule.order.equals(TOFRONT)) {
                                        wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.toFront();
                                    } else {
                                        wallpaper.controller.tileControllerLists.get(i).get(j).motifController.artworkControllers.get(l).view.toBack();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void fillWallpaperView(Wallpaper wallpaper) {
        for (int i = 0; i < wallpaper.controller.tileControllerLists.size(); i++) {
            wallpaper.controller.view.tileViews.getChildren().add(new Group());
            for (int j = 0; j < wallpaper.controller.tileControllerLists.get(i).size(); j++) {
                Group tileViewList = (Group) wallpaper.controller.view.tileViews.getChildren().get(i);
                if (!tileViewList.getChildren().contains(wallpaper.controller.tileControllerLists.get(i).get(j).view)) {
                    tileViewList.getChildren().add(wallpaper.controller.tileControllerLists.get(i).get(j).view);
                }
            }
        }
    }

}
