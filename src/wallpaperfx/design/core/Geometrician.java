/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.core;

import java.util.ArrayList;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.shape.Line;

/**
 * Object with methods to perform geometric operations.
 *
 * @author Maher Elkhaldi
 */
public class Geometrician {

    /**
     * Constructor
     */
    public Geometrician() {

    }

    /**
     * Returns an {@link ArrayList} containing the four corners of a node using
     * the {@link Node#getBoundsInParent()}, corners are organized: Top Left,
     * Bottom left, Bottom Right, and Top Right.
     *
     * @param node node to get corners for
     * @return {@link ArrayList} of {@link Point2D} objects.
     */
    public ArrayList<Point2D> getAllCorners(Node node) {
        ArrayList<Point2D> points = new ArrayList<>();
        Bounds bounds = node.getBoundsInParent();
        double xMax = bounds.getMaxX();
        double yMax = bounds.getMaxY();
        double xMin = bounds.getMinX();
        double yMin = bounds.getMinY();
        points.add(node.localToScene(new Point2D(xMin, yMin)));
        points.add(node.localToScene(new Point2D(xMin, yMax)));
        points.add(node.localToScene(new Point2D(xMax, yMax)));
        points.add(node.localToScene(new Point2D(xMax, yMin)));
        return points;
    }

    /**
     * returns {@link Point2D} holding the coordinates of bottom left corner of
     * a node, which is minimum X and maximum Y using the {@link Node#getBoundsInParent()
     * }.
     *
     * @param node node to return bottom left corner of
     * @return {@link Point2D}
     */
    public Point2D getBottomLeft(Node node) {
        Bounds bounds = node.getBoundsInParent();
        double yMax = bounds.getMaxY();
        double xMin = bounds.getMinX();
        return new Point2D(xMin, yMax);
    }

    /**
     * returns {@link Point2D} holding the coordinates of bottom right corner of
     * a node, which is maximum X and maximum Y using the {@link Node#getBoundsInParent()
     * }.
     *
     * @param node node to return bottom right corner of
     * @return {@link Point2D}
     */
    public Point2D getBottomRight(Node node) {
        Bounds bounds = node.getBoundsInParent();
        double xMax = bounds.getMaxX();
        double yMax = bounds.getMaxY();
        return new Point2D(xMax, yMax);
    }

    /**
     * returns {@link Point2D} holding the coordinates of center of a node,
     * which is the average of maximum and minimum X and Y that are computed
     * from the {@link Node#getBoundsInParent() }
     *
     * @param node node to get center of
     * @return {@link Point2D}
     */
    public Point2D getCenter(Node node) {
        Bounds bounds = node.getBoundsInParent();
        double xMax = bounds.getMaxX();
        double yMax = bounds.getMaxY();
        double xMin = bounds.getMinX();
        double yMin = bounds.getMinY();
        double x = (xMax + xMin) / 2;
        double y = (yMax + yMin) / 2;
        return new Point2D(x, y);
    }

    /**
     * Returns closest point representing the closest position to a reference
     * point along a line segment. Code for this method was adapted from:
     * <a href="http://www.java2s.com/Code/Java/2D-Graphics-GUI/Returnsclosestpointonsegmenttopoint.htm">Java2s.com</a>
     *
     * @param lineSegment line to get a point on
     * @param refPoint reference point
     * @return {@link Point2D}
     */
    public Point2D getClosestPointOnSegment(Line lineSegment, Point2D refPoint) {
        return getClosestPointOnSegment(new Point2D(lineSegment.getStartX(), lineSegment.getStartY()), new Point2D(lineSegment.getEndX(), lineSegment.getEndY()), refPoint);
    }

    /**
     * Returns closest point representing the closest position to reference
     * point along a distance/virtual line defined by two points. Code for this
     * method was adapted from:
     * <a href="http://www.java2s.com/Code/Java/2D-Graphics-GUI/Returnsclosestpointonsegmenttopoint.htm">
     * Java2s.com</a>
     *
     * @param segStartPoint the start point of the virtual line
     * @param segEndPoint the end point of the virtual line
     * @param refPoint the reference point
     * @return {@link Point2D}
     */
    public Point2D getClosestPointOnSegment(Point2D segStartPoint, Point2D segEndPoint, Point2D refPoint) {
        return getClosestPointOnSegment(segStartPoint.getX(), segStartPoint.getY(), segEndPoint.getX(), segEndPoint.getY(), refPoint.getX(), refPoint.getY());
    }

    /**
     * Returns closest point within a virtual line defined between two points
     * coordinates to a reference point coordinates. Code for this method was
     * adapted from:
     * <a href="http://www.java2s.com/Code/Java/2D-Graphics-GUI/Returnsclosestpointonsegmenttopoint.htm">Java2s.com</a>
     *
     * @param segStartPointX the start point X coordinate of the virtual line
     * @param segStartPointY the start point Y coordinate of the virtual line
     * @param segEndPointX the end point X coordinate of the virtual line
     * @param segEndPointY the end point Y coordinate of the virtual line
     * @param refPointX the reference point X coordinate
     * @param refPointY the reference point Y coordinate
     * @return {@link Point2D}
     */
    public Point2D getClosestPointOnSegment(double segStartPointX, double segStartPointY, double segEndPointX, double segEndPointY, double refPointX, double refPointY) {
        double xDelta = segEndPointX - segStartPointX;
        double yDelta = segEndPointY - segStartPointY;

        if ((xDelta == 0) && (yDelta == 0)) {
            throw new IllegalArgumentException("Segment start equals segment end");
        }

        double u = ((refPointX - segStartPointX) * xDelta + (refPointY - segStartPointY) * yDelta) / (xDelta * xDelta + yDelta * yDelta);

        final Point2D closestPoint;
        if (u < 0) {
            closestPoint = new Point2D(segStartPointX, segStartPointY);
        } else if (u > 1) {
            closestPoint = new Point2D(segEndPointX, segEndPointY);
        } else {
            closestPoint = new Point2D((int) Math.round(segStartPointX + u * xDelta), (int) Math.round(segStartPointY + u * yDelta));
        }

        return closestPoint;
    }

    /**
     * Produces {@link ArrayList} containing {@link Point2D points} distributed
     * along a {@link Line} between its start and end points.
     *
     * @param line Line to distribute point on.
     * @param count number of points to generate
     * @param option whether to include line's start and/or end points with the
     * result.
     * @return {@link ArrayList} of {@link Point2D} objects.
     */
    public ArrayList<Point2D> getDivPoints(Line line, int count, IncludeOmit option) {

        return getDivPoints(new Point2D(line.getStartX(), line.getStartY()), new Point2D(line.getEndX(), line.getEndY()), count, option);
    }

    /**
     * Produces points distributed (Point2D) that are distributed equally
     * between two points
     *
     * @param start start point
     * @param end end point
     * @param count number of points to generate
     * @param option whether to include line's start and/or end points with the
     * result.
     * @return {@link ArrayList} of {@link Point2D} objects.
     */
    public ArrayList<Point2D> getDivPoints(Point2D start, Point2D end, int count, IncludeOmit option) {
        ArrayList<Point2D> points = new ArrayList();
        double deltaX = end.getX() - start.getX();
        double deltaY = end.getY() - start.getY();
        double incX = deltaX / count;
        double incY = deltaY / count;

        for (int i = 1; i < count; i++) {
            points.add(new Point2D(start.getX() + incX * i, start.getY() + incY * i));
        }
        switch (option) {
            case INCLDUE_START_AND_END:
                points.add(0, start);
                points.add(end);
                break;
            case INCLUDE_START:
                points.add(0, start);
                break;
            case INCLUDE_END:
                points.add(end);
                break;
            case OMIT_START_AND_END:
                break;
        }

        return points;
    }

    /**
     * returns {@link Point2D} holding the coordinates of top left corner of a
     * node, which is minimum X and minimum Y using the {@link Node#getBoundsInParent()
     * }
     *
     * @param node node to get top left corner of
     * @return {@link Point2D}
     */
    public Point2D getTopLeft(Node node) {
        Bounds bounds = node.getBoundsInParent();
        double xMin = bounds.getMinX();
        double yMin = bounds.getMinY();
        return new Point2D(xMin, yMin);
    }

    /**
     * returns {@link Point2D} holding the coordinates of top right corner of a
     * node, which is maximum X and minimum Y using the {@link Node#getBoundsInParent()
     * }.
     *
     * @param node node to return top right corner of
     * @return {@link Point2D}
     */
    public Point2D getTopRight(Node node) {
        Bounds bounds = node.getBoundsInParent();
        double xMax = bounds.getMaxX();
        double yMin = bounds.getMinY();
        return new Point2D(xMax, yMin);
    }

    /**
     * Whether to include or omit Start and End points in the resulting Point2D
     * list.
     */
    public static enum IncludeOmit {

        INCLDUE_START_AND_END, INCLUDE_START, INCLUDE_END, OMIT_START_AND_END
    }

}
