/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.core;

import java.util.Collection;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Affine;
import wallpaperfx.design.artifact.motif.MotifView;
import wallpaperfx.design.core.Enums.Axis;
import wallpaperfx.design.structure.cell.CellModel;

/**
 *
 * @author Maher Elkhaldi
 */
public abstract class AffineView extends Group {

    private SimpleDoubleProperty reflectStartX = new SimpleDoubleProperty(0);
    private SimpleDoubleProperty reflectStartY = new SimpleDoubleProperty(0);
    private SimpleDoubleProperty reflectEndX = new SimpleDoubleProperty(0);
    private SimpleDoubleProperty reflectEndY = new SimpleDoubleProperty(10);

    private Affine reflect = new Affine();
    private Affine rotate = new Affine();
    private Affine scale = new Affine();
    private Affine position = new Affine();

    public SimpleDoubleProperty positionX = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty positionY = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty scaleX = new SimpleDoubleProperty(1);
    public SimpleDoubleProperty scaleY = new SimpleDoubleProperty(1);
    public SimpleDoubleProperty rotatePivotX = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty rotatePivotY = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty rotateRadians = new SimpleDoubleProperty(0);
    public SimpleBooleanProperty reflection = new SimpleBooleanProperty(false);
    public SimpleObjectProperty<Axis> reflectionAxis = new SimpleObjectProperty<>();
    public CellModel cellModel;

    public AffineView(CellModel cellModel) {
        this.cellModel = cellModel;
        affineAppend();
        setup();
    }

    public AffineView(CellModel cellModel, Node... children) {
        this.cellModel = cellModel;
        super.getChildren().addAll(children);
        affineAppend();
        setup();
    }

    public AffineView(CellModel cellModel, Collection<Node> children) {
        this.cellModel = cellModel;
        super.getChildren().addAll(children);
        affineAppend();
        setup();
    }

    public void dispose() {
        clear();
        unbindAllVals();
        nullify();
    }

    public void clear() {
        super.getChildren().clear();
    }

    
    public void addChild(Shape... shapes) {
        super.getChildren().addAll(shapes);
    }
    
    public void addChild(MotifView motifView) {
        super.getChildren().add(motifView);
    }


     private void affineAppend() {

        //Reflection
        this.getTransforms().add(reflect);
        //Translation
        this.getTransforms().add(position);
        //Rotation
        this.getTransforms().add(rotate);
        //Scale
        this.getTransforms().add(scale);
    }

    private void setup() {

        //bindTranslateXY
        position.txProperty().bind(positionX);
        position.tyProperty().bind(positionY);

//        //bindScale
        scale.mxxProperty().bind(scaleX);
        scale.myyProperty().bind(scaleY);
        scale.txProperty().bind(positionX);
        scale.tyProperty().bind(positionY);
//
        //bindRotationAngle
        rotate.mxxProperty().bind(MathFX.cos(rotateRadians));
        rotate.mxyProperty().bind(MathFX.sin(rotateRadians.negate()));
        rotate.myxProperty().bind(MathFX.sin(rotateRadians));
        rotate.myyProperty().bind(MathFX.cos(rotateRadians));
//        //bindRotatePivot
        rotate.txProperty().bind(rotatePivotX.subtract(positionX).subtract(rotatePivotX.subtract(positionX).multiply(MathFX.cos(rotateRadians))).add(rotatePivotY.subtract(positionY).multiply(MathFX.sin(rotateRadians))));
        rotate.tyProperty().bind(rotatePivotY.subtract(positionY).subtract(rotatePivotX.subtract(positionX).multiply(MathFX.sin(rotateRadians))).subtract(rotatePivotY.subtract(positionY).multiply(MathFX.cos(rotateRadians))));

        reflection.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                bindAffineReflect(reflectStartX, reflectStartY, reflectEndX, reflectEndY);
            } else {
                resetReflection();
            }
        });

        reflectionAxis.addListener((ObservableValue<? extends Axis> observable, Axis oldValue, Axis newValue) -> {

            setReflectionAxis();

        });

        reflectionAxis.set(Axis.IK_AXIS);
    }

    private void setReflectionAxis() {
        switch (reflectionAxis.get()) {
            case CPOINT_HORIZONTAL_AXIS:
                reflectStartX.bind(cellModel.pointCXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointCYProperty().subtract(cellModel.pointCXProperty()));
                reflectEndX.bind(cellModel.pointCXProperty().subtract(cellModel.pointCXProperty()).add(1));
                reflectEndY.bind(cellModel.pointCYProperty().subtract(cellModel.pointCXProperty()));
                break;
            case CPOINT_VERTICAL_AXIS:
                reflectStartX.bind(cellModel.pointCXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointCYProperty().subtract(cellModel.pointCXProperty()));
                reflectEndX.bind(cellModel.pointCXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointCYProperty().subtract(cellModel.pointCXProperty()).add(1));
                break;
            case OU_AXIS:
                reflectStartX.bind(cellModel.pointOXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointOYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointUXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointUYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case OV_AXIS:
                reflectStartX.bind(cellModel.pointOXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointOYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointVXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointVYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case OM_AXIS:
                reflectStartX.bind(cellModel.pointOXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointOYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointMXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointMYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case ON_AXIS:
                reflectStartX.bind(cellModel.pointOXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointOYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointNXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointNYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case OW_AXIS:
                reflectStartX.bind(cellModel.pointOXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointOYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointWXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointWYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case UW_AXIS:
                reflectStartX.bind(cellModel.pointUXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointUYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointWXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointWYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case VW_AXIS:
                reflectStartX.bind(cellModel.pointVXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointVYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointWXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointWYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case VU_AXIS:
                reflectStartX.bind(cellModel.pointVXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointVYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointUXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointUYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case WM_AXIS:
                reflectStartX.bind(cellModel.pointWXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointWYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointMXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointMYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case WN_AXIS:
                reflectStartX.bind(cellModel.pointWXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointWYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointNXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointNYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case IK_AXIS:
                reflectStartX.bind(cellModel.pointIXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointIYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointKXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointKYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case JL_AXIS:
                reflectStartX.bind(cellModel.pointJXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointJYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointLXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointLYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case IJ_AXIS:
                reflectStartX.bind(cellModel.pointIXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointIYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointJXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointJYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case JK_AXIS:
                reflectStartX.bind(cellModel.pointJXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointJYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointKXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointKYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case KL_AXIS:
                reflectStartX.bind(cellModel.pointKXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointKYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointLXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointLYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case LI_AXIS:
                reflectStartX.bind(cellModel.pointLXProperty().subtract(cellModel.pointCXProperty()));
                reflectStartY.bind(cellModel.pointLYProperty().subtract(cellModel.pointCYProperty()));
                reflectEndX.bind(cellModel.pointIXProperty().subtract(cellModel.pointCXProperty()));
                reflectEndY.bind(cellModel.pointIYProperty().subtract(cellModel.pointCYProperty()));
                break;
            case NONE:
                reflectStartX.unbind();
                reflectStartY.unbind();
                reflectEndX.unbind();
                reflectEndY.unbind();
                break;

        }
    }

    private void resetReflection() {
        unbindAffine(reflect);
        reflect.setToIdentity();
    }

    private void bindAffineReflect(DoubleProperty startX, DoubleProperty startY, DoubleProperty endX, DoubleProperty endY) {

        SimpleDoubleProperty A = new SimpleDoubleProperty();
        SimpleDoubleProperty B = new SimpleDoubleProperty();
        SimpleDoubleProperty C = new SimpleDoubleProperty();
        SimpleDoubleProperty abSQRT = new SimpleDoubleProperty();

        A.bind(startY.subtract(endY));
        B.bind(endX.subtract(startX));
        C.bind((startX.multiply(endY)).subtract((endX.multiply(startY))));

        abSQRT.bind((A.multiply(A)).add(B.multiply(B)));

        SimpleDoubleProperty mxx = new SimpleDoubleProperty();
        SimpleDoubleProperty mxy = new SimpleDoubleProperty();
        SimpleDoubleProperty tx = new SimpleDoubleProperty();

        mxx.bind((B.multiply(B)).subtract(A.multiply(A)));
        mxy.bind((A.multiply(B).multiply(-2)));
        tx.bind((A.multiply(C).multiply(-2)));

        SimpleDoubleProperty myx = new SimpleDoubleProperty();
        SimpleDoubleProperty myy = new SimpleDoubleProperty();
        SimpleDoubleProperty ty = new SimpleDoubleProperty();

        //second row
        myx.bind((A.multiply(B).multiply(-2)));
        myy.bind((A.multiply(A)).subtract(B.multiply(B)));

        ty.bind((B.multiply(C).multiply(-2)));

        reflect.mxxProperty().bind(mxx.divide(abSQRT));
        reflect.mxyProperty().bind(mxy.divide(abSQRT));
        reflect.txProperty().bind(tx.divide(abSQRT));

        reflect.myxProperty().bind(myx.divide(abSQRT));
        reflect.myyProperty().bind(myy.divide(abSQRT));
        reflect.tyProperty().bind(ty.divide(abSQRT));

    }

    private void unbindAffine(Affine affine){
        affine.mxxProperty().unbind();
        affine.mxyProperty().unbind();
        affine.mxzProperty().unbind();
        affine.myxProperty().unbind();
        affine.myyProperty().unbind();
        affine.myzProperty().unbind();
        affine.mzxProperty().unbind();
        affine.mzyProperty().unbind();
        affine.mzzProperty().unbind();
        affine.txProperty().unbind();
        affine.tyProperty().unbind();
        affine.tzProperty().unbind();
    }



    public final void unbindAllVals() {
        unbindAffine(reflect);
        unbindAffine(rotate);
        unbindAffine(scale);
        unbindAffine(position);

        positionX.unbind();
        positionY.unbind();
        scaleX.unbind();
        scaleY.unbind();
        rotatePivotX.unbind();
        rotatePivotY.unbind();
        rotateRadians.unbind();
        reflectStartX.unbind();
        reflectStartY.unbind();
        reflectEndX.unbind();
        reflectEndY.unbind();
        reflection.unbind();
    }
    
    private void nullify(){
        reflectStartX 	= null;
        reflectStartY 	= null;
        reflectEndX	 	= null;
        reflectEndY 	= null;
        reflect			= null;
        rotate 			= null;
        scale 			= null;
        position 		= null;
        positionX 		= null;
        positionY 		=null;
        scaleX 			= null;
        scaleY 			= null;
        rotatePivotX 	= null;
        rotatePivotY	= null;
        rotateRadians 	= null;
        reflection 		= null;
        reflectionAxis 	= null;
        cellModel		=null;
    }

}
