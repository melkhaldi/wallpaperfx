/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.core;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import wallpaperfx.design.structure.lattice.Lattice;

/**
 * Mouse Controller Objects that includes methods to query Mouse position, and
 * to move Mouse to specific positions.
 *
 * @author Maher Elkhaldi
 */
public class MouseController {

    /**
     * Constructor
     */
    public MouseController() {

    }

    /**
     * Move mouse cursor to the origin of a target node
     *
     * @param target target to move cursor to.
     */
    public static void positionMouseAtNode(Node target) {
        try {
            double xPixel = target.getScene().getX() + target.getScene().getWindow().getX() + target.localToScene(Point2D.ZERO).getX();
            double yPixel = target.getScene().getY() + target.getScene().getWindow().getY() + target.localToScene(Point2D.ZERO).getY();

            new Robot().mouseMove((int) xPixel, (int) yPixel);
        } catch (AWTException ex) {
            Logger.getLogger(MouseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * get mouse position on screen
     *
     * @return Point2D
     */
    private static Point2D getMouseScreenPosition() {
        Point2D position = new Point2D(MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y);
        return position;
    }

    /**
     * Get mouse position in terms of a a region.
     *
     * @param anchorPane the region in terms of which to get the mouse position.
     * region can be a Pane, Anchor-Pane, etc.
     * @return Point2D
     */
    public static Point2D getMouseScenePosition(AnchorPane anchorPane) {
        Point2D screenPosition = getMouseScreenPosition();

        double x = screenPosition.getX() - anchorPane.localToScene(Point2D.ZERO).getX() - anchorPane.getScene().getX() - anchorPane.getScene().getWindow().getX();
        double y = screenPosition.getY() - anchorPane.localToScene(Point2D.ZERO).getY() - anchorPane.getScene().getY() - anchorPane.getScene().getWindow().getY();

        return new Point2D(x, y);
    }

    public static void addAndPositionNodeAtMouse(Node node, AnchorPane anchorPane) {
        if (!anchorPane.getChildren().contains(node)) {
            anchorPane.getChildren().add(node);
        }
        positionNodeAtMouse(node, anchorPane);
    }

    public static void positionNodeAtMouse(Node node, AnchorPane anchorPane) {
        node.setTranslateX(MouseController.getMouseScenePosition(anchorPane).getX());
        node.setTranslateY(MouseController.getMouseScenePosition(anchorPane).getY());
    }

    public static void positionMouseAtCellModel(Lattice lattice, int i, int j, int offsetX, int offsetY) {
        try {
            double xPixel = offsetX+lattice.view.getParent().getScene().getX() + lattice.view.getParent().getScene().getWindow().getX() + lattice.model.cellModelLists.get(i).get(j).getPointCX();
            double yPixel = offsetY+lattice.view.getParent().getScene().getY() + lattice.view.getParent().getScene().getWindow().getY() + lattice.model.cellModelLists.get(i).get(j).getPointCY();

            new Robot().mouseMove((int) xPixel, (int) yPixel);
        } catch (AWTException ex) {
            Logger.getLogger(MouseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
