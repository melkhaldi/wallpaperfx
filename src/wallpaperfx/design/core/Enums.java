/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.core;
/**
 *
 * @author Maher Elkhaldi
 */
public class Enums {

    public static enum ElementType{
        MOTIF, TILE, WALLPAPER, CELL, LATTICE;
    }
    public static enum SymmetryModelType {
        EMPTY, P1, P2, P3, P3M1,P31M, P4, P4M, P4MG, PG, PM, P6, P6M, PMM, PGG, PMG, CM, CMM;
    }
    
    public static enum CellType {
        HEXAGONAL, SQUARE, RECTANGULAR, RHOMBIC, OBLIQUE;
    }

    public static enum Pivot {
        OPOINT, UPOINT, VPOINT, WPOINT, CPOINT, MPOINT, NPOINT, IPOINT, LPOINT, KPOINT, JPOINT, CRPOINT, CLPOINT;
    }

    public static enum Axis {
        NONE, OU_AXIS, OV_AXIS, VW_AXIS, UW_AXIS, OW_AXIS, VU_AXIS, IK_AXIS, JL_AXIS, OM_AXIS, WM_AXIS, ON_AXIS, WN_AXIS, CPOINT_VERTICAL_AXIS, CPOINT_HORIZONTAL_AXIS, LI_AXIS, IJ_AXIS, JK_AXIS, KL_AXIS;
    }
    
    public static enum Order{
        TOFRONT, TOBACK;
    }


}
