/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.control;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import wallpaperfx.design.artifact.wallpaper.Wallpaper;
import wallpaperfx.design.artifact.wallpaper.WallpaperController;
import wallpaperfx.display.ControlPane;

/**
 *
 * @author Maher Elkhaldi
 */
public class MotifDesignControl extends DesignControlVBox {

      public Slider sliderRadians = new Slider(-Math.PI * 2, Math.PI * 2, 0);
    public Slider sliderPosX = new Slider(-15, 15, 0);
    public Slider sliderPosY = new Slider(-15, 15, 0);

    public Slider sliderScaleX = new Slider(-5, 5, 1);
    public Slider sliderScaleY = new Slider(-5, 5, 1);
    private final Label xPos = new Label();
    private final Label yPos = new Label();
    private final Label xScale = new Label();
    private final Label yScale = new Label();
    private final Label radians = new Label();

    public MotifDesignControl(ControlPane controlPane, Wallpaper wallpaper) {
        super(controlPane);
        wallpaper.controller.releaseProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            controlControllers(wallpaper.controller);
        });

        controlControllers(wallpaper.controller);
        super.setAlignment(Pos.CENTER);
        super.getChildren().add(new Label("MotifeController--Motif Level"));
         super.getChildren().add(new HBox(sliderRadians, radians));
        super.getChildren().add(new HBox(sliderPosX, xPos));
        super.getChildren().add(new HBox(sliderPosY, yPos));
        super.getChildren().add(new HBox(sliderRadians, radians));
        super.getChildren().add(new HBox(sliderScaleX, xScale));
        super.getChildren().add(new HBox(sliderScaleY, yScale));
        this.setMaxWidth(300);
        this.setPrefWidth(300);
        sliderRadians.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            radians.setText("Radians:" + newValue.toString().concat("0").substring(0,4));
        });
        sliderPosX.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            xPos.setText("XPos:" + newValue.toString().concat("0").substring(0,4));
        });
        sliderPosY.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            yPos.setText("YPos:" + newValue.toString().concat("0").substring(0,4));
        });
        sliderScaleX.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            xScale.setText("XScale:" + newValue.toString().concat("0").substring(0,4));
        });
        sliderScaleY.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            yScale.setText("YScale:" + newValue.toString().concat("0").substring(0,4));
        });

        radians.setText("Radians:" + sliderRadians.getValue());
        xPos.setText("XPos:" + sliderPosX.getValue());
        yPos.setText("YPos:" + sliderPosY.getValue());
        xScale.setText("XScale:" + sliderScaleX.getValue());
        yScale.setText("YScale:" + sliderScaleY.getValue());

   
    }

    private void controlControllers(WallpaperController wallpaperController) {
        wallpaperController.tileControllerLists.forEach(list -> {
            list.stream().forEach(tileController -> {
                tileController.motifController.artworkControllers.forEach(motifController -> {
                    motifController.bindDeltaRadiansPropertyTo(sliderRadians.valueProperty());
                    motifController.bindDeltaPositionXPropertyTo(sliderPosX.valueProperty());
                    motifController.bindDeltaPositionYPropertyTo(sliderPosY.valueProperty());
                    motifController.bindScaleXPropertyTo(sliderScaleX.valueProperty());
                    motifController.bindScaleYPropertyTo(sliderScaleY.valueProperty());

                });
            });
        });
    }
}
