/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.control;

import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.HandleEnums.HandleShape.ARROW;
import static wallpaperfx.design.control.HandleEnums.HandleShape.CIRCLE;
import static wallpaperfx.design.control.HandleEnums.HandleShape.LINE;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.design.structure.lattice.LatticeModel;
import wallpaperfx.display.ControlPane;
import wallpaperfx.design.core.MathFX;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;

/**
 *
 * @author Maher Elkhaldi
 */
public class LatticeDesignControl extends DesignControlGroup {

    protected final LatticeModel latticeModel;

    double deltaX;
    double deltaY;
    double sensitivity = 0.75;
    double threasholdFactorX = 0.2;
    double threasholdFactorY = 0.2;
    double threasholdX;
    double threasholdY;
    double xStart;
    double yStart;

    private double posXOPoint;
    private double posYOPoint;
    private boolean wXAfterOPoint, wYAfterOPoint;
    private final Group handles = new Group();

    private Line VU;
    private Circle grabW, grabU, grabV;
    private Polygon addU, removeU, addV, removeV;

    Group uControls = new Group();
    Group vControls = new Group();
    Group boundary = new Group();

    public final static String OPOINT_IDLE = "-fx-alignment: center; -fx-fill: royalblue ;-fx-stroke: royalblue; -fx-stroke-width: 4;";
    public final static String OPOINT_ACTIVE = "-fx-alignment: center; -fx-fill: lightskyblue ;-fx-stroke: lightskyblue; -fx-stroke-width: 5;";
    public final static String UPOINT_IDLE = "-fx-alignment: center; -fx-fill: red ;-fx-stroke: red; -fx-stroke-width: 4;";
    public final static String UPOINT_ACTIVE = "-fx-alignment: center; -fx-fill: salmon ;-fx-stroke: salmon; -fx-stroke-width: 5;";
    public final static String VPOINT_IDLE = "-fx-alignment: center; -fx-fill: green ;-fx-stroke: green; -fx-stroke-width: 4;";
    public final static String VPOINT_ACTIVE = "-fx-alignment: center; -fx-fill: limegreen ;-fx-stroke: limegreen; -fx-stroke-width: 5;";
    public final static String COUNT_DRAG_IDLE_VU = "-fx-alignment: center; -fx-fill: dimgray ;-fx-stroke: dimgray; -fx-stroke-width: 3;-fx-stroke-dash-array: 5 10.0";
    public final static String COUNT_DRAG_ACTIVE_VU = "-fx-alignment: center; -fx-fill: darkgray ;-fx-stroke: darkgray; -fx-stroke-width: 5;-fx-stroke-dash-array: 5 10.0";

    public final static String COUNT_DRAG_IDLE_W = "-fx-alignment: center; -fx-fill: dimgray ;-fx-stroke: dimgray; -fx-stroke-width: 3;";
    public final static String COUNT_DRAG_ACTIVE_W = "-fx-alignment: center; -fx-fill: darkgray ;-fx-stroke: darkgray; -fx-stroke-width: 5;";

    double radius = 3;
    double size = 3;
    double spacing = 5;

    public LatticeDesignControl(ControlPane controlPane, Lattice lattice) {
        super(controlPane);
        this.latticeModel = lattice.model;

        latticeModel.releaseProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            setHandles();
        });
        addU = (Polygon) build(ARROW, size, UPOINT_IDLE, UPOINT_ACTIVE);
        addU.setRotate(270);
        removeU = (Polygon) build(ARROW, size, UPOINT_IDLE, UPOINT_ACTIVE);
        removeU.setRotate(90);
        addV = (Polygon) build(ARROW, size, VPOINT_IDLE, VPOINT_ACTIVE);
        removeV = (Polygon) build(ARROW, size, VPOINT_IDLE, VPOINT_ACTIVE);
        removeV.setRotate(180);

        VU = (Line) build(LINE, 2, COUNT_DRAG_IDLE_VU, COUNT_DRAG_ACTIVE_VU);
        grabV = (Circle) build(CIRCLE, radius, VPOINT_IDLE, VPOINT_ACTIVE);
        grabU = (Circle) build(CIRCLE, radius, UPOINT_IDLE, UPOINT_ACTIVE);
        grabW = (Circle) build(CIRCLE, radius, COUNT_DRAG_IDLE_W, COUNT_DRAG_ACTIVE_W);

        handles.getChildren().addAll(VU, grabW);

        uControls.getChildren().add(addU);
        uControls.getChildren().add(grabU);
        grabU.setLayoutY(size * 2.8 + radius + spacing);
        grabU.setLayoutX(radius / 4);
        uControls.getChildren().add(removeU);
        removeU.setLayoutY(size * 4 + 2 * radius + 2 * spacing);

        vControls.getChildren().add(addV);
        vControls.getChildren().add(grabV);
        grabV.setLayoutX(size * 2 + radius + spacing);
        grabV.setLayoutY(radius / 4);
        vControls.getChildren().add(removeV);
        removeV.setLayoutX(size * 4 + 2 * radius + 2 * spacing);

        super.getChildren().add(handles);
        super.getChildren().add(uControls);
        super.getChildren().add(vControls);
        super.getChildren().add(boundary);
        setHandles();

        setGrabVBehavior();
        setGrabUBehavior();
        setDiagonalBehavior();
        setWWBehavior();

        removeU.setOnMouseClicked((MouseEvent event) -> {
            setUCount(latticeModel.getUCount() - 1);
        });
        addU.setOnMouseClicked((MouseEvent event) -> {
            setUCount(latticeModel.getUCount() + 1);
        });

        removeV.setOnMouseClicked((MouseEvent event) -> {
            setVCount(latticeModel.getVCount() - 1);
        });
        addV.setOnMouseClicked((MouseEvent event) -> {
            setVCount(latticeModel.getVCount() + 1);
        });

        this.setOnMouseEntered(this::recordPositions);

    }

    public final void setUCount(int val) {
        latticeModel.setUCount(val);
    }

    public final void setVCount(int val) {
        latticeModel.setVCount(val);
    }

    private void processUWDrag(double sFactor) {
        if (sFactor * deltaX > threasholdX && Math.round(sFactor * deltaX % threasholdX) == 0) {
            if (wXAfterOPoint) {
                setUCount(latticeModel.getUCount() + 1);
            } else {
                setUCount(latticeModel.getUCount() - 1);
            }
        } else if (sFactor * deltaX < threasholdX && Math.round(sFactor * deltaX % threasholdX) == 0) {
            if (wXAfterOPoint) {
                setUCount(latticeModel.getUCount() - 1);
            } else {
                setUCount(latticeModel.getUCount() + 1);
            }
        }
    }

    private void processVWDrag(double sFactor) {
        if (sFactor * deltaY > threasholdY && Math.round(sFactor * deltaY % threasholdY) == 0) {
            if (wYAfterOPoint) {
                setVCount(latticeModel.getVCount() + 1);
            } else {
                setVCount(latticeModel.getVCount() - 1);
            }
        } else if (sFactor * deltaY < threasholdY && Math.round(sFactor * deltaY % threasholdY) == 0) {
            if (wYAfterOPoint) {
                setVCount(latticeModel.getVCount() - 1);
            } else {
                setVCount(latticeModel.getVCount() + 1);
            }
        }
    }

    private void processVUDrag(double sFactor) {

        double aveSize = 0.5 * (threasholdX + threasholdY);
        double aveDelta = 0.5 * deltaX + 0.5 * deltaY;
        if (sFactor * aveDelta >= aveSize && Math.round(sFactor * aveDelta % aveSize) == 0) {
            if (wYAfterOPoint || wXAfterOPoint) {
                setVCount(latticeModel.getVCount() + 1);
                setUCount(latticeModel.getUCount() + 1);
            } else {
                setVCount(latticeModel.getVCount() - 1);
                setUCount(latticeModel.getUCount() - 1);
            }

        } else if (sFactor * aveDelta <= aveSize && Math.round(sFactor * aveDelta % aveSize) == 0) {
            if (wYAfterOPoint || wXAfterOPoint) {
                setVCount(latticeModel.getVCount() - 1);
                setUCount(latticeModel.getUCount() - 1);

            } else {
                setVCount(latticeModel.getVCount() + 1);
                setUCount(latticeModel.getUCount() + 1);

            }
        }
    }

    private void recordPositions(MouseEvent event) {
        xStart = event.getSceneX();
        yStart = event.getSceneY();
        posXOPoint = latticeModel.uMinvMinCellModelProperty().get().getPointOX();
        posYOPoint = latticeModel.uMinvMinCellModelProperty().get().getPointOY();

        wXAfterOPoint = latticeModel.uMaxvMaxCellModelProperty().get().getPointWX() > posXOPoint;
        wYAfterOPoint = latticeModel.uMaxvMaxCellModelProperty().get().getPointWY() > posYOPoint;

        threasholdX = threasholdFactorX * (latticeModel.uMinvMinCellModelProperty().get().pointUXProperty().get() - latticeModel.uMinvMinCellModelProperty().get().pointOXProperty().get());

        threasholdY = threasholdFactorY * (latticeModel.uMinvMinCellModelProperty().get().pointVYProperty().get() - latticeModel.uMinvMinCellModelProperty().get().pointOYProperty().get());
    }

    private void setHandles() {

        VU.startXProperty().bind(latticeModel.uMinvMaxCellModelProperty().get().pointVXProperty().add(3));
        VU.startYProperty().bind(latticeModel.uMinvMaxCellModelProperty().get().pointVYProperty().add(3));
        VU.endXProperty().bind(latticeModel.uMaxvMinCellModelProperty().get().pointUXProperty().add(3));
        VU.endYProperty().bind(latticeModel.uMaxvMinCellModelProperty().get().pointUYProperty().add(3));

        grabW.centerXProperty().bind(latticeModel.uMaxvMaxCellModelProperty().get().pointWXProperty().add(15));
        grabW.centerYProperty().bind(latticeModel.uMaxvMaxCellModelProperty().get().pointWYProperty().add(15));

        uControls.layoutXProperty().bind(latticeModel.uMaxvMinCellModelProperty().get().pointUXProperty().add(15));
        uControls.layoutYProperty().bind(latticeModel.uMaxvMinCellModelProperty().get().pointUYProperty().add(-size / 2));
        uControls.rotateProperty().bind(MathFX.toDegrees(latticeModel.uMinvMaxCellModelProperty().get().radiansIKProperty()).add(180));

        vControls.layoutXProperty().bind(latticeModel.uMinvMaxCellModelProperty().get().pointVXProperty());
        vControls.layoutYProperty().bind(latticeModel.uMinvMaxCellModelProperty().get().pointVYProperty().add(15));
        vControls.rotateProperty().bind(MathFX.toDegrees(latticeModel.uMinvMaxCellModelProperty().get().radiansOUProperty()));

    }

    private void setGrabUBehavior() {
        grabU.setOnMousePressed(this::recordPositions);
        grabU.setOnMouseDragged((MouseEvent event) -> {

            deltaX = event.getSceneX() - xStart;

            processUWDrag(sensitivity);

        });
    }

    private void setGrabVBehavior() {
        grabV.setOnMousePressed(this::recordPositions);
        grabV.setOnMouseDragged((MouseEvent event) -> {

            deltaY = event.getSceneY() - yStart;

            processVWDrag(sensitivity);
        });
    }

    private void setWWBehavior() {

        grabW.setOnMousePressed(this::recordPositions);
        grabW.setOnMouseDragged((MouseEvent event) -> {

            deltaX = event.getSceneX() - xStart;

            processUWDrag(sensitivity);

            deltaY = event.getSceneY() - yStart;

            processVWDrag(sensitivity);

        });

    }

    private void setDiagonalBehavior() {
        VU.setOnMousePressed(this::recordPositions);
        VU.setOnMouseDragged((MouseEvent event) -> {
            deltaX = event.getSceneX() - xStart;
            deltaY = event.getSceneY() - yStart;
            processVUDrag(sensitivity * 0.9);
        });

    }

    public Polygon arrow(double size) {
        Polygon arrow = new Polygon();
        arrow.getPoints().add(-size / 2.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(size / 5.0);
        arrow.getPoints().add(size / 2.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(size);

        return arrow;
    }
}
