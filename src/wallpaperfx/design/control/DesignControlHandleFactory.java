/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.control;

import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import wallpaperfx.design.control.HandleEnums.HandleShape;
import static wallpaperfx.design.control.HandleEnums.HandleShape.ARROW;
import static wallpaperfx.design.control.HandleEnums.HandleShape.CIRCLE;
import static wallpaperfx.design.control.HandleEnums.HandleShape.LINE;

/**
 *
 * @author Maher Elkhaldi
 */
public final class DesignControlHandleFactory {

    public static final Shape build(HandleShape handleShape, double size, String idleStyle, String activeStyle) {
        switch (handleShape) {
            case LINE:
                Line line = line(size);
                setElementStyleBehavior(line, idleStyle, activeStyle);
                return line;
            case ARROW:
                Polygon arrow = arrow(size);
                setElementStyleBehavior(arrow, idleStyle, activeStyle);
                return arrow;
            case CIRCLE:
                Circle circle = circle(size);
                setElementStyleBehavior(circle, idleStyle, activeStyle);
                return circle;
            default:
                Polygon square = square(size);
                setElementStyleBehavior(square, idleStyle, activeStyle);
                return square;
        }
    }
    
    public static final Shape build(HandleShape handleShape, double size, String idleStyle) {
        switch (handleShape) {
            case LINE:
                Line line = line(size);
                setElementStyleBehavior(line,  idleStyle);
                return line;
            case ARROW:
                Polygon arrow = arrow(size);
                setElementStyleBehavior(arrow, idleStyle);
                return arrow;
            case CIRCLE:
                Circle circle = circle(size);
                setElementStyleBehavior(circle, idleStyle);
                return circle;
            default:
                Polygon square = square(size);
                setElementStyleBehavior(square, idleStyle);
                return square;
        }
    }

    public static final Line line(double length) {
        Line line = new Line();
        line.setStartX(length);
        line.setStartY(0);
        line.setEndX(length);
        line.setEndY(length);
        return new Line();
    }

    public static final Circle circle(double size) {
        Circle circle = new Circle();
        circle.setRadius(size);
        circle.setCenterX(0);
        circle.setCenterY(0);
        return circle;
    }

    public static final Polygon square(double size) {
        Polygon square = new Polygon();
        square.getPoints().add(-size / 2.0);
        square.getPoints().add(0.0);

        square.getPoints().add(0.0);
        square.getPoints().add(size / 5.0);

        square.getPoints().add(size / 2.0);
        square.getPoints().add(0.0);

        square.getPoints().add(0.0);
        square.getPoints().add(size);

        return square;
    }

    public static final Polygon arrow(double size) {
        Polygon arrow = new Polygon();
        arrow.getPoints().add(-size / 2.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(size / 5.0);
        arrow.getPoints().add(size / 2.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(0.0);
        arrow.getPoints().add(size);

        return arrow;
    }

    public static final void setElementStyleBehavior(Shape shape, String idleStyle, String activeStyle) {
        shape.setStyle(idleStyle);
        shape.setOnMouseEntered((MouseEvent event) -> {
            shape.setStyle(activeStyle);
        });
        shape.setOnMouseExited((MouseEvent event) -> {
            if (event.isPrimaryButtonDown()) {
                shape.setStyle(activeStyle);
            } else {
                shape.setStyle(idleStyle);
            }
        });
        shape.setOnMouseReleased((MouseEvent event) -> {
            shape.setStyle(idleStyle);
        });
    }
    
     public static final void setElementStyleBehavior(Shape shape, String idleStyle) {
        shape.setStyle(idleStyle);
       
    }

}
