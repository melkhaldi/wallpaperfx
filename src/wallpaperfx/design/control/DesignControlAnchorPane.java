/*
 * The MIT License
 *
 * Copyright 2016 melkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.control;

import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.AnchorPane;
import wallpaperfx.display.ControlPane;

/**
 *
 * @author melkhaldi
 */
public class DesignControlAnchorPane extends AnchorPane {

    ReadOnlyBooleanWrapper loaded = new ReadOnlyBooleanWrapper(true);
    ReadOnlyObjectWrapper<ControlPane> controlPane = new ReadOnlyObjectWrapper();

    public DesignControlAnchorPane(ControlPane pane) {
        setControlPane(pane);

    }
public final ReadOnlyBooleanProperty loadedProperty() {
        return loaded.getReadOnlyProperty();
    }


    public final boolean isLoaded() {
        return loaded.get();
    }

    public final void setLoaded(boolean val) {
        if (val) {
            load();
        } else {
            unload();
        }
    }
    public final void setLoaded(ControlPane controlPane, boolean val) {
        if (val) {
            load(controlPane);
        } else {
            unload(controlPane);
        }
    }

   public final void setControlPane(ControlPane pane) {
        controlPane.set(pane);
        controlPane.addListener((ObservableValue<? extends ControlPane> observable, ControlPane oldValue, ControlPane newValue) -> {
            if (newValue == null) {
                oldValue.getChildren().remove(this);
                loaded.set(false);
            } else {
                setLoaded(newValue, loaded.get());
            }
        });
        load(controlPane.get());
    }

    private void load(ControlPane controlPane) {
        if (controlPane != null) {
            if (!controlPane.getChildren().contains(this)) {
                controlPane.getChildren().add(this);
                loaded.set(true);
            }
        }
    }

    private void unload(ControlPane controlPane) {
        if (controlPane != null) {
            controlPane.getChildren().remove(this);
            loaded.set(false);
        }
    }

    private void load() {
        if (controlPane.get() != null) {
            if (!controlPane.get().getChildren().contains(this)) {
                controlPane.get().getChildren().add(this);
                loaded.set(true);
            }
        }
    }

    private void unload() {
        if (controlPane.get() != null) {
            controlPane.get().getChildren().remove(this);
            loaded.set(false);
        }
    }
}
