/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.control;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;
import static wallpaperfx.design.control.DesignControlHandleFactory.*;
import static wallpaperfx.design.control.HandleEnums.HandleShape.ARROW;
import static wallpaperfx.design.control.HandleEnums.HandleShape.CIRCLE;
import static wallpaperfx.design.control.HandleEnums.HandleShape.LINE;
import wallpaperfx.design.structure.cell.CellModel;
import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.display.ControlPane;

/**
 *
 * @author Maher Elkhaldi
 */
public final class CellDesignControl extends DesignControlGroup {

    public final CellModel cellModel;
    private final Group background = new Group();

    private final Line edgeOU;
    private final Line edgeOV;
    private final Line edgeOW;
    private final Line edgeUW;
    private final Line edgeVU;
    private final Line edgeVW;
    private final Group edges = new Group();
    private final Polygon handleBackground = new Polygon();
    private final Shape handleOPoint;
    private final Shape handleUPoint;
    private final Shape handleVPoint;
    private final Shape handleWPoint;
    private final Group handles = new Group();
    private double lenU;
    private double lenV;
    private double mouseDeltaX;
    private double mouseDeltaY;
    private double mouseStartX;
    private double mouseStartY;
    private final String polyActiveStyle = "-fx-fill: rgba(1,1,0.5,0.2); -fx-stroke: transparent;";
    private final String polyIdleStyle = "-fx-fill: rgba(1,1,0.5,0.1); -fx-stroke: transparent;";
    private double posUXPoint;
    private double posUYPoint;
    private double posVXPoint;
    private double posVYPoint;
    private double posOXPoint;
    private double posOYPoint;
    private double radiansU;
    private double radiansV;
    private boolean wXAfterOPoint, wYAfterOPoint;
    private double uX, uY, vX, vY;
    public final static String OPOINT_IDLE = "-fx-alignment: center; -fx-fill: royalblue ;-fx-stroke: royalblue; -fx-stroke-width: 3;";
    public final static String OPOINT_ACTIVE = "-fx-alignment: center; -fx-fill: lightskyblue ;-fx-stroke: lightskyblue; -fx-stroke-width: 5;";
    public final static String UPOINT_IDLE = "-fx-alignment: center; -fx-fill: red ;-fx-stroke: red; -fx-stroke-width: 3;";
    public final static String UPOINT_ACTIVE = "-fx-alignment: center; -fx-fill: salmon ;-fx-stroke: salmon; -fx-stroke-width: 5;";
    public final static String VPOINT_IDLE = "-fx-alignment: center; -fx-fill: green ;-fx-stroke: green; -fx-stroke-width: 3;";
    public final static String VPOINT_ACTIVE = "-fx-alignment: center; -fx-fill: limegreen ;-fx-stroke: limegreen; -fx-stroke-width: 5;";
    public final static String WPOINT_IDLE = "-fx-alignment: center; -fx-fill: royalblue ;-fx-stroke: royalblue; -fx-stroke-width: 3;";
    public final static String WPOINT_ACTIVE = "-fx-alignment: center; -fx-fill: lightskyblue ;-fx-stroke: lightskyblue; -fx-stroke-width: 5;";

    public final static String DIAGONALS_IDLE = "-fx-alignment: center; -fx-fill: mediumorchid ;-fx-stroke: mediumorchid; -fx-stroke-width: 3;-fx-stroke-dash-array: 4 6.0";
    public final static String DIAGONALS_ACTIVE = "-fx-alignment: center; -fx-fill: plum ;-fx-stroke: plum; -fx-stroke-width: 4;-fx-stroke-dash-array: 4 6.0";

    public CellDesignControl(ControlPane controlPane, Lattice lattice, Shape handleOPoint, Shape handleUPoint, Shape handleVPoint, Shape handleWPoint, Line edgeOU, Line edgeOV, Line edgeOW, Line edgeUW, Line edgeVW, Line edgeVU) {
        super(controlPane);
        this.cellModel = lattice.model.uMinvMinCellModelProperty.get();
        this.handleOPoint = handleOPoint;
        this.handleUPoint = handleUPoint;
        this.handleVPoint = handleVPoint;
        this.handleWPoint = handleWPoint;
        this.edgeOU = edgeOU;
        this.edgeOV = edgeOV;
        this.edgeOW = edgeOW;
        this.edgeUW = edgeUW;
        this.edgeVW = edgeVW;
        this.edgeVU = edgeVU;
        set();
    }

    public CellDesignControl(ControlPane controlPane, Lattice lattice) {
        super(controlPane);
        this.cellModel = lattice.model.uMinvMinCellModelProperty.get();
        this.handleOPoint = build(CIRCLE, 5, OPOINT_IDLE, OPOINT_ACTIVE);
        this.handleUPoint = build(ARROW, 10, UPOINT_IDLE, UPOINT_ACTIVE);
        this.handleVPoint = build(ARROW, 10, VPOINT_IDLE, VPOINT_ACTIVE);
        this.handleWPoint = build(ARROW, 10, WPOINT_IDLE, WPOINT_ACTIVE);

        this.edgeOU = (Line) build(LINE, 0, UPOINT_IDLE, UPOINT_ACTIVE);
        this.edgeOV = (Line) build(LINE, 0, VPOINT_IDLE, VPOINT_ACTIVE);
        this.edgeOW = (Line) build(LINE, 0, WPOINT_IDLE, WPOINT_ACTIVE);
        this.edgeUW = (Line) build(LINE, 0, DIAGONALS_IDLE, DIAGONALS_ACTIVE);
        this.edgeVW = (Line) build(LINE, 0, DIAGONALS_IDLE, DIAGONALS_ACTIVE);
        this.edgeVU = (Line) build(LINE, 0, DIAGONALS_IDLE, DIAGONALS_ACTIVE);
        set();
    }

    public ReadOnlyObjectProperty handleOPointProperty() {
        return new ReadOnlyObjectWrapper(handleOPoint).getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty handleUPointProperty() {
        return new ReadOnlyObjectWrapper(handleUPoint).getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty handleVPointProperty() {
        return new ReadOnlyObjectWrapper(handleVPoint).getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty backgroundPolygonProperty() {
        return new ReadOnlyObjectWrapper(handleBackground).getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty handleWPointProperty() {
        return new ReadOnlyObjectWrapper(handleWPoint).getReadOnlyProperty();
    }

    private void alignHandle(double pivotX, double pivotY, double handleX, double handleY, Shape handle) {
        double curRad = Math.atan2(handleY - pivotY, handleX - pivotX);
        handle.getTransforms().clear();

        Rotate rW = new Rotate(Math.toDegrees(curRad) - 90, 0, 0);
        handle.getTransforms().add(rW);
    }

    private void alignHandleAll() {
        alignHandleU();
        alignHandleV();
        alignHandleW();
    }

    private void alignHandleU() {
        alignHandle(cellModel.getPointOX(), cellModel.getPointOY(), cellModel.getPointUX(), cellModel.getPointUY(), handleUPoint);
    }

    private void alignHandleV() {
        alignHandle(cellModel.getPointOX(), cellModel.getPointOY(), cellModel.getPointVX(), cellModel.getPointVY(), handleVPoint);
    }

    private void alignHandleW() {
        alignHandle(cellModel.getPointOX(), cellModel.getPointOY(), cellModel.getPointWX(), cellModel.getPointWY(), handleWPoint);
    }

    private void bindEdgesPositionAll() {

        edgeOU.startXProperty().bind(cellModel.pointOXProperty());
        edgeOU.startYProperty().bind(cellModel.pointOYProperty());
        edgeOU.endXProperty().bind(cellModel.pointUXProperty());
        edgeOU.endYProperty().bind(cellModel.pointUYProperty());

        edgeOV.startXProperty().bind(cellModel.pointOXProperty());
        edgeOV.startYProperty().bind(cellModel.pointOYProperty());
        edgeOV.endXProperty().bind(cellModel.pointVXProperty());
        edgeOV.endYProperty().bind(cellModel.pointVYProperty());

        edgeUW.startXProperty().bind(cellModel.pointUXProperty());
        edgeUW.startYProperty().bind(cellModel.pointUYProperty());
        edgeUW.endXProperty().bind(cellModel.pointWXProperty());
        edgeUW.endYProperty().bind(cellModel.pointWYProperty());

        edgeVW.startXProperty().bind(cellModel.pointVXProperty());
        edgeVW.startYProperty().bind(cellModel.pointVYProperty());
        edgeVW.endXProperty().bind(cellModel.pointWXProperty());
        edgeVW.endYProperty().bind(cellModel.pointWYProperty());

        edgeOW.startXProperty().bind(cellModel.pointOXProperty());
        edgeOW.startYProperty().bind(cellModel.pointOYProperty());
        edgeOW.endXProperty().bind(cellModel.pointWXProperty());
        edgeOW.endYProperty().bind(cellModel.pointWYProperty());

        edgeVU.startXProperty().bind(cellModel.pointVXProperty());
        edgeVU.startYProperty().bind(cellModel.pointVYProperty());
        edgeVU.endXProperty().bind(cellModel.pointUXProperty());
        edgeVU.endYProperty().bind(cellModel.pointUYProperty());
    }

    private void buildHandleBackground() {
        handleBackground.getPoints().add(0, cellModel.pointOXProperty().get());
        handleBackground.getPoints().add(1, cellModel.pointOYProperty().get());
        handleBackground.getPoints().add(2, cellModel.pointUXProperty().get());
        handleBackground.getPoints().add(3, cellModel.pointUYProperty().get());
        handleBackground.getPoints().add(4, cellModel.pointWXProperty().get());
        handleBackground.getPoints().add(5, cellModel.pointWYProperty().get());
        handleBackground.getPoints().add(6, cellModel.pointVXProperty().get());
        handleBackground.getPoints().add(7, cellModel.pointVYProperty().get());
    }

    private void bundleShapes() {
        handles.getChildren().add(handleOPoint);
        handles.getChildren().add(handleUPoint);
        handles.getChildren().add(handleVPoint);
        handles.getChildren().add(handleWPoint);
        background.getChildren().add(handleBackground);

        edges.getChildren().add(edgeVU);
        edges.getChildren().add(edgeUW);
        edges.getChildren().add(edgeVW);
        edges.getChildren().add(edgeOW);
        edges.getChildren().add(edgeOU);
        edges.getChildren().add(edgeOV);

        this.getChildren().add(handleBackground);

        this.getChildren().add(edges);
        this.getChildren().add(handles);
    }

    private void listenToModel() {

        handleOPoint.translateXProperty().bind(cellModel.pointOXProperty());
        handleOPoint.translateYProperty().bind(cellModel.pointOYProperty());
        handleUPoint.translateXProperty().bind(cellModel.pointUXProperty());
        handleUPoint.translateYProperty().bind(cellModel.pointUYProperty());
        handleVPoint.translateXProperty().bind(cellModel.pointVXProperty());
        handleVPoint.translateYProperty().bind(cellModel.pointVYProperty());

        handleWPoint.translateXProperty().bind(cellModel.pointWXProperty());
        handleWPoint.translateYProperty().bind(cellModel.pointWYProperty());

        cellModel.pointWXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(0);
            handleBackground.getPoints().add(0, cellModel.pointOXProperty().get());
        });
        cellModel.pointOYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(1);
            handleBackground.getPoints().add(1, cellModel.pointOYProperty().get());
        });

        cellModel.pointUXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(2);
            handleBackground.getPoints().add(2, cellModel.pointUXProperty().get());
        });
        cellModel.pointUYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(3);
            handleBackground.getPoints().add(3, cellModel.pointUYProperty().get());
        });

        cellModel.pointWXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(4);
            handleBackground.getPoints().add(4, cellModel.pointWXProperty().get());
        });

        cellModel.pointWYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(5);
            handleBackground.getPoints().add(5, cellModel.pointWYProperty().get());
        });

        cellModel.pointVXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(6);
            handleBackground.getPoints().add(6, cellModel.pointVXProperty().get());
        });
        cellModel.pointVYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            handleBackground.getPoints().remove(7);
            handleBackground.getPoints().add(7, cellModel.pointVYProperty().get());
        });

        cellModel.releaseProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            alignHandleAll();
        });

    }

    private void recordPositions(MouseEvent event) {
        mouseStartX = event.getSceneX();
        mouseStartY = event.getSceneY();

        posOXPoint = handleOPoint.getTranslateX();
        posOYPoint = handleOPoint.getTranslateY();

        posUXPoint = handleUPoint.getTranslateX();
        posUYPoint = handleUPoint.getTranslateY();

        posVXPoint = handleVPoint.getTranslateX();
        posVYPoint = handleVPoint.getTranslateY();

        radiansU = cellModel.getRadiansOU();
        radiansV = cellModel.getRadiansOV();

        lenU = cellModel.getLengthOU();
        lenV = cellModel.getLengthOV();

        wXAfterOPoint = cellModel.getPointWX() >= cellModel.getPointOX();
        wYAfterOPoint = cellModel.getPointWY() >= cellModel.getPointOY();

    }

    private void set() {
        unbindEdgesPositionsAll();
        bindEdgesPositionAll();
        buildHandleBackground();
        bundleShapes();
        setHandleDragAll();
        setHandleBackgroundDrag();
        setEdgeDragAll();
        listenToModel();
        alignHandleAll();
        setElementStyleBehavior(handleBackground, polyIdleStyle, polyActiveStyle);

    }

    private void setEdgeDragAll() {
        setEdgeDragOU();
        setEdgeDragOV();
        setEdgeDragUW();
        setEdgeDragVW();
        setEdgeDragOW();
        setEdgeDragVU();
    }

    private void setEdgeDragOU() {
        edgeOU.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        edgeOU.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - posOXPoint;
            mouseDeltaY = event.getSceneY() - posOYPoint;
            cellModel.setRadiansOU(Math.atan2(mouseDeltaY, mouseDeltaX));
        });
    }

    private void setEdgeDragOV() {
        edgeOV.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        edgeOV.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - posOXPoint;
            mouseDeltaY = event.getSceneY() - posOYPoint;
            cellModel.setRadiansOV(Math.atan2(mouseDeltaY, mouseDeltaX));
        });
    }

    private void setEdgeDragOW() {
        edgeOW.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        edgeOW.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - posOXPoint;
            mouseDeltaY = event.getSceneY() - posOYPoint;
            cellModel.setRadiansOW(Math.atan2(mouseDeltaY, mouseDeltaX));
        });

    }

    private void setEdgeDragUW() {
        edgeUW.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        edgeUW.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaY = event.getSceneY() - mouseStartY;
            if (wXAfterOPoint && wYAfterOPoint) {
                if (posVXPoint <= posUXPoint && posUYPoint <= posVYPoint) {
                    cellModel.setPointUX(posUXPoint + mouseDeltaX * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint + mouseDeltaX * Math.sin(radiansU));
                } else if (posVXPoint >= posUXPoint && posUYPoint <= posVYPoint) {
                    cellModel.setPointUX(posUXPoint + mouseDeltaX * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint + mouseDeltaX * Math.sin(radiansU));
                } else if (posVXPoint <= posUXPoint && posUYPoint >= posVYPoint) {
                    cellModel.setPointUX(posUXPoint + mouseDeltaY * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint + mouseDeltaY * Math.sin(radiansU));
                } else {
                    cellModel.setPointUX(posUXPoint + mouseDeltaY * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint + mouseDeltaY * Math.sin(radiansU));
                }
            } else if (!wXAfterOPoint && !wYAfterOPoint) {
                if (posVXPoint <= posUXPoint && posUYPoint <= posVYPoint) {
                    cellModel.setPointUX(posUXPoint - mouseDeltaY * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint - mouseDeltaY * Math.sin(radiansU));
                } else {
                    cellModel.setPointUX(posUXPoint - mouseDeltaX * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint - mouseDeltaX * Math.sin(radiansU));
                }
            } else if (wXAfterOPoint && !wYAfterOPoint) {
                if (posVXPoint <= posUXPoint && posUYPoint <= posVYPoint) {
                    cellModel.setPointUX(posUXPoint - mouseDeltaY * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint - mouseDeltaY * Math.sin(radiansU));
                } else if (posVXPoint >= posUXPoint && posUYPoint <= posVYPoint) {
                    cellModel.setPointUX(posUXPoint - mouseDeltaY * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint - mouseDeltaY * Math.sin(radiansU));
                } else if (posVXPoint <= posUXPoint && posUYPoint >= posVYPoint) {
                    cellModel.setPointUX(posUXPoint + mouseDeltaX * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint + mouseDeltaX * Math.sin(radiansU));
                } else {
                    cellModel.setPointUX(posUXPoint - mouseDeltaX * Math.cos(radiansU));
                    cellModel.setPointUY(posUYPoint - mouseDeltaX * Math.sin(radiansU));
                }
            } else if (posVXPoint <= posUXPoint && posUYPoint <= posVYPoint) {
                cellModel.setPointUX(posUXPoint - mouseDeltaY * Math.cos(radiansU));
                cellModel.setPointUY(posUYPoint - mouseDeltaY * Math.sin(radiansU));
            } else if (posVXPoint >= posUXPoint && posUYPoint <= posVYPoint) {
                cellModel.setPointUX(posUXPoint - mouseDeltaX * Math.cos(radiansU));
                cellModel.setPointUY(posUYPoint - mouseDeltaX * Math.sin(radiansU));
            } else if (posVXPoint <= posUXPoint && posUYPoint >= posVYPoint) {
                cellModel.setPointUX(posUXPoint + mouseDeltaY * Math.cos(radiansU));
                cellModel.setPointUY(posUYPoint + mouseDeltaY * Math.sin(radiansU));
            } else {
                cellModel.setPointUX(posUXPoint + mouseDeltaY * Math.cos(radiansU));
                cellModel.setPointUY(posUYPoint + mouseDeltaY * Math.sin(radiansU));
            }
        });
    }

    private void setEdgeDragVU() {
        edgeVU.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        edgeVU.setOnMouseDragged((MouseEvent event) -> {

            if (!wXAfterOPoint && !wYAfterOPoint) {
                mouseDeltaX = event.getSceneX() - mouseStartX;
                mouseDeltaY = event.getSceneY() - mouseStartY;
                double rate = (mouseDeltaX + mouseDeltaY) / ((lenU + lenV) / 2);
                double incU = (lenU * rate);
                double incV = (lenV * rate);
                cellModel.setPointUX(posUXPoint + incU * Math.cos(radiansU) * -1);
                cellModel.setPointUY(posUYPoint + incU * Math.sin(radiansU) * -1);
                cellModel.setPointVX(posVXPoint + incV * Math.cos(radiansV) * -1);
                cellModel.setPointVY(posVYPoint + incV * Math.sin(radiansV) * -1);
            } //in quarter: -x + y
            else if (wXAfterOPoint && wYAfterOPoint) {
                mouseDeltaX = event.getSceneX() - mouseStartX;
                mouseDeltaY = event.getSceneY() - mouseStartY;
                double rate = (mouseDeltaX + mouseDeltaY) / ((lenU + lenV) / 2);
                double incU = (lenU * rate);
                double incV = (lenV * rate);
                cellModel.setPointUX(posUXPoint + incU * Math.cos(radiansU));
                cellModel.setPointUY(posUYPoint + incU * Math.sin(radiansU));
                cellModel.setPointVX(posVXPoint + incV * Math.cos(radiansV));
                cellModel.setPointVY(posVYPoint + incV * Math.sin(radiansV));
            } else if (!wXAfterOPoint && wYAfterOPoint) {
                mouseDeltaX = mouseStartX - event.getSceneX();
                mouseDeltaY = event.getSceneY() - mouseStartY;
                double rate = (mouseDeltaX + mouseDeltaY) / ((lenU + lenV) / 2);
                double incU = (lenU * rate);
                double incV = (lenV * rate);
                cellModel.setPointUX(posUXPoint + incU * Math.cos(radiansU));
                cellModel.setPointUY(posUYPoint + incU * Math.sin(radiansU));
                cellModel.setPointVX(posVXPoint + incV * Math.cos(radiansV));
                cellModel.setPointVY(posVYPoint + incV * Math.sin(radiansV));
            } else {
                mouseDeltaX = event.getSceneX() - mouseStartX;
                mouseDeltaY = mouseStartY - event.getSceneY();
                double rate = (mouseDeltaX + mouseDeltaY) / ((lenU + lenV) / 2);
                double incU = (lenU * rate);
                double incV = (lenV * rate);
                cellModel.setPointUX(posUXPoint + incU * Math.cos(radiansU));
                cellModel.setPointUY(posUYPoint + incU * Math.sin(radiansU));
                cellModel.setPointVX(posVXPoint + incV * Math.cos(radiansV));
                cellModel.setPointVY(posVYPoint + incV * Math.sin(radiansV));
            }

        });
    }

    private void setEdgeDragVW() {
        edgeVW.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        edgeVW.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaY = event.getSceneY() - mouseStartY;
            if (wXAfterOPoint && wYAfterOPoint) {
                if (posVXPoint >= posUXPoint && posUYPoint >= posVYPoint) {
                    cellModel.setPointVX(posVXPoint + mouseDeltaX * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint + mouseDeltaX * Math.sin(radiansV));
                } else {
                    cellModel.setPointVX(posVXPoint + mouseDeltaY * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint + mouseDeltaY * Math.sin(radiansV));
                }
            } else if (!wXAfterOPoint && !wYAfterOPoint) {
                if (posVXPoint >= posUXPoint && posUYPoint >= posVYPoint) {
                    cellModel.setPointVX(posVXPoint - mouseDeltaY * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint - mouseDeltaY * Math.sin(radiansV));
                } else {
                    cellModel.setPointVX(posVXPoint - mouseDeltaX * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint - mouseDeltaX * Math.sin(radiansV));
                }
            } else if (wXAfterOPoint && !wYAfterOPoint) {
                if (posVXPoint >= posUXPoint && posUYPoint >= posVYPoint) {
                    cellModel.setPointVX(posVXPoint - mouseDeltaY * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint - mouseDeltaY * Math.sin(radiansV));
                } else if (posVXPoint >= posUXPoint && posUYPoint <= posVYPoint) {
                    cellModel.setPointVX(posVXPoint + mouseDeltaX * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint + mouseDeltaX * Math.sin(radiansV));
                } else if (posVXPoint <= posUXPoint && posUYPoint >= posVYPoint) {
                    cellModel.setPointVX(posVXPoint - mouseDeltaX * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint - mouseDeltaX * Math.sin(radiansV));
                } else {
                    cellModel.setPointVX(posVXPoint + mouseDeltaY * Math.cos(radiansV));
                    cellModel.setPointVY(posVYPoint + mouseDeltaY * Math.sin(radiansV));
                }
            } else if (posVXPoint >= posUXPoint && posUYPoint >= posVYPoint) {
                cellModel.setPointVX(posVXPoint - mouseDeltaY * Math.cos(radiansV));
                cellModel.setPointVY(posVYPoint - mouseDeltaY * Math.sin(radiansV));
            } else if (posVXPoint >= posUXPoint && posUYPoint <= posVYPoint) {
                cellModel.setPointVX(posVXPoint + mouseDeltaY * Math.cos(radiansV));
                cellModel.setPointVY(posVYPoint + mouseDeltaY * Math.sin(radiansV));
            } else if (posVXPoint <= posUXPoint && posUYPoint >= posVYPoint) {
                cellModel.setPointVX(posVXPoint - mouseDeltaX * Math.cos(radiansV));
                cellModel.setPointVY(posVYPoint - mouseDeltaX * Math.sin(radiansV));
            } else {
                cellModel.setPointVX(posVXPoint + mouseDeltaY * Math.cos(radiansV));
                cellModel.setPointVY(posVYPoint + mouseDeltaY * Math.sin(radiansV));
            }
        });
    }

    private void setHandleBackgroundDrag() {

        handleBackground.setOnMouseEntered((MouseEvent event) -> {
            handleBackground.setCursor(Cursor.OPEN_HAND);
        });
        handleBackground.setOnMouseExited((MouseEvent event) -> {
            if (event.isPrimaryButtonDown()) {
                handleBackground.setStyle(polyActiveStyle);
            } else {
                handleBackground.setStyle(polyIdleStyle);
            }
        });
        handleBackground.setOnMouseReleased((MouseEvent event) -> {
            handleBackground.setStyle(polyIdleStyle);
            handleBackground.setCursor(Cursor.OPEN_HAND);
        });
        handleBackground.setOnMousePressed((MouseEvent event) -> {
            handleBackground.setStyle(polyActiveStyle);
            recordPositions(event);
        });
        handleBackground.setOnMouseDragged((MouseEvent event) -> {

            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaY = event.getSceneY() - mouseStartY;

            cellModel.setPointOX(posOXPoint + mouseDeltaX);
            cellModel.setPointOY(posOYPoint + mouseDeltaY);

            cellModel.setPointUX(posUXPoint + mouseDeltaX);
            cellModel.setPointUY(posUYPoint + mouseDeltaY);

            cellModel.setPointVX(posVXPoint + mouseDeltaX);
            cellModel.setPointVY(posVYPoint + mouseDeltaY);

        });

    }

    private void setHandleDragAll() {
        setHandleDragOPoint();
        setHandleDragUPoint();
        setHandleDragVPoint();
        setHandleDragWPoint();
    }

    private void setHandleDragOPoint() {
        handleOPoint.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        handleOPoint.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaY = event.getSceneY() - mouseStartY;

            cellModel.setPointOX(posOXPoint + mouseDeltaX);
            cellModel.setPointOY(posOYPoint + mouseDeltaY);

        });
    }

    private void setHandleDragUPoint() {
        handleUPoint.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        handleUPoint.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaY = event.getSceneY() - mouseStartY;

            cellModel.setPointUX(posUXPoint + mouseDeltaX);
            cellModel.setPointUY(posUYPoint + mouseDeltaY);

        });
    }

    private void setHandleDragVPoint() {
        handleVPoint.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        handleVPoint.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaY = event.getSceneY() - mouseStartY;

            cellModel.setPointVX(posVXPoint + mouseDeltaX);
            cellModel.setPointVY(posVYPoint + mouseDeltaY);

        });
    }

    private void setHandleDragWPoint() {
        handleWPoint.setOnMousePressed((MouseEvent event) -> {
            recordPositions(event);
        });
        handleWPoint.setOnMouseDragged((MouseEvent event) -> {
            mouseDeltaX = event.getSceneX() - mouseStartX;
            mouseDeltaY = event.getSceneY() - mouseStartY;
            uX = mouseDeltaX * Math.cos(radiansU);
            uY = mouseDeltaX * Math.sin(radiansU);
            vX = mouseDeltaY * Math.cos(radiansV);
            vY = mouseDeltaY * Math.sin(radiansV);
            if (wXAfterOPoint && wYAfterOPoint) {
                if (posUXPoint >= posVXPoint) {
                    cellModel.setPointUX(posUXPoint + uX);
                    cellModel.setPointVX(posVXPoint + uY);
                } else {
                    cellModel.setPointUX(posUXPoint + uX);
                    cellModel.setPointVX(posVXPoint + uY);
                }

                if (posUYPoint >= posVYPoint) {
                    cellModel.setPointUY(posUYPoint + vX);
                    cellModel.setPointVY(posVYPoint + vY);
                } else {
                    cellModel.setPointUY(posUYPoint + vX);
                    cellModel.setPointVY(posVYPoint + vY);
                }

            } else if (!wXAfterOPoint && !wYAfterOPoint) {
                if (posUXPoint >= posVXPoint) {
                    cellModel.setPointUX(posUXPoint - uX);
                    cellModel.setPointVX(posVXPoint - uY);
                } else {
                    cellModel.setPointUX(posUXPoint - uX);
                    cellModel.setPointVX(posVXPoint - uY);
                }

                if (posUYPoint >= posVYPoint) {
                    cellModel.setPointUY(posUYPoint - vX);
                    cellModel.setPointVY(posVYPoint - vY);
                } else {
                    cellModel.setPointUY(posUYPoint - vX);
                    cellModel.setPointVY(posVYPoint - vY);
                }

            } else if (wXAfterOPoint && !wYAfterOPoint) {
                if (posUXPoint >= posVXPoint) {
                    cellModel.setPointUX(posUXPoint + uX);
                    cellModel.setPointVX(posVXPoint + uY);
                } else {
                    cellModel.setPointUX(posUXPoint - uX);
                    cellModel.setPointVX(posVXPoint - uY);
                }

                if (posUYPoint >= posVYPoint) {
                    cellModel.setPointUY(posUYPoint - vX);
                    cellModel.setPointVY(posVYPoint - vY);
                } else {
                    cellModel.setPointUY(posUYPoint + vX);
                    cellModel.setPointVY(posVYPoint + vY);
                }

            } else {
                if (posUXPoint >= posVXPoint) {
                    cellModel.setPointUX(posUXPoint + uX);
                    cellModel.setPointVX(posVXPoint + uY);
                } else {
                    cellModel.setPointUX(posUXPoint - uX);
                    cellModel.setPointVX(posVXPoint - uY);
                }
                if (posUYPoint >= posVYPoint) {
                    cellModel.setPointUY(posUYPoint - vX);
                    cellModel.setPointVY(posVYPoint - vY);
                } else {
                    cellModel.setPointUY(posUYPoint + vX);
                    cellModel.setPointVY(posVYPoint + vY);
                }
            }
        });
    }

    private void unbindEdgesPositionsAll() {
        edgeOU.startXProperty().unbind();
        edgeOU.startYProperty().unbind();
        edgeOU.endXProperty().unbind();
        edgeOU.endYProperty().unbind();
        edgeOV.startXProperty().unbind();
        edgeOV.startYProperty().unbind();
        edgeOV.endXProperty().unbind();
        edgeOV.endYProperty().unbind();
        edgeUW.startXProperty().unbind();
        edgeUW.startYProperty().unbind();
        edgeUW.endXProperty().unbind();
        edgeUW.endYProperty().unbind();
        edgeVW.startXProperty().unbind();
        edgeVW.startYProperty().unbind();
        edgeVW.endXProperty().unbind();
        edgeVW.endYProperty().unbind();
        edgeOW.startXProperty().unbind();
        edgeOW.startYProperty().unbind();
        edgeOW.endXProperty().unbind();
        edgeOW.endYProperty().unbind();
        edgeVU.startXProperty().unbind();
        edgeVU.startYProperty().unbind();
        edgeVU.endXProperty().unbind();
        edgeVU.endYProperty().unbind();
    }

}
