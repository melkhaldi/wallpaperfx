/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.artifact.tile;

import wallpaperfx.design.artifact.motif.MotifController;

/**
 *
 * @author Maher Elkhaldi
 */
public class TileController {

    public  TileModel model;
    public  TileView view;
    public  MotifController motifController;

    public TileController(TileModel tileModel) {
        this.model = tileModel;
        this.view = new TileView(tileModel.cellModel);
        motifController = new MotifController(model.motifModel);
        link();
        set();
    }

    public final TileView set() {
        view.addChild(motifController.set());
        return view;
    }

    public final void dispose() {
        view.dispose();
        motifController.dispose();
        nullify();
    }

    private void nullify(){
        view = null;
        model = null;
        motifController=null;
    }
    private void link() {
        view.rotateRadians.bind(model.rotateRadians);
        view.positionX.bind(model.positionX);
        view.positionY.bind(model.positionY);
        view.rotatePivotX.bind(model.cellModel.pointCXProperty());
        view.rotatePivotY.bind(model.cellModel.pointCYProperty());
        view.scaleX.bind(model.scaleX);
        view.scaleY.bind(model.scaleY);
        view.reflection.bind(model.reflection);
        view.reflectionAxis.bind(model.reflectionAxis);
    }

}
