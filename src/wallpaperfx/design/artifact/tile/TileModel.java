/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.artifact.tile;

import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import wallpaperfx.design.artifact.motif.MotifModel;
import wallpaperfx.design.core.Enums.Axis;
import static wallpaperfx.design.core.Enums.Axis.IK_AXIS;
import wallpaperfx.design.core.Enums.Pivot;
import wallpaperfx.design.structure.cell.CellModel;
import wallpaperfx.designrules.rules.DesignRule;
import wallpaperfx.symmetry.model.SymmetryModel;
import wallpaperfx.symmetry.model.SymmetryModelFactory;

/**
 *
 * @author Maher Elkhaldi
 */
public class TileModel {

    //settable parameters:
    public SimpleDoubleProperty rotateDeltaRadians = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty positionDeltaX = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty positionDeltaY = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty scaleX = new SimpleDoubleProperty(1);
    public SimpleDoubleProperty scaleY = new SimpleDoubleProperty(1);
    public SimpleBooleanProperty reflection = new SimpleBooleanProperty(false);
    public SimpleObjectProperty<Axis> reflectionAxis = new SimpleObjectProperty<>();
    public Pivot pivot = Pivot.CPOINT;

    //computed parameters:
    public ReadOnlyDoubleWrapper rotateRadians = new ReadOnlyDoubleWrapper(0);
    public ReadOnlyDoubleWrapper positionX = new ReadOnlyDoubleWrapper(0);
    public ReadOnlyDoubleWrapper positionY = new ReadOnlyDoubleWrapper(0);
    private SimpleDoubleProperty rotateInitRadians = new SimpleDoubleProperty(0);
    private SimpleDoubleProperty positionInitX = new SimpleDoubleProperty(0);
    private SimpleDoubleProperty positionInitY = new SimpleDoubleProperty(0);

    //fixed parameters
    public ReadOnlyDoubleWrapper rotatePivotX = new ReadOnlyDoubleWrapper(0);
    public ReadOnlyDoubleWrapper rotatePivotY = new ReadOnlyDoubleWrapper(0);
    //essential parameters
    public MotifModel motifModel;
    public SymmetryModel symmetryModel;
    public CellModel cellModel;
    public DesignRule designRule;

    public TileModel(CellModel cellModel, DesignRule rule) {
        this.designRule = rule;
        this.cellModel = cellModel;

        settableParameters();
        computedParameters();
        buildOthers();
    }

    private void settableParameters() {
        this.reflectionAxis.set(IK_AXIS);
        this.positionInitX.bind(cellModel.pointCXProperty().divide(2));
        this.positionInitY.bind(cellModel.pointCYProperty().divide(2));
    }

    private void computedParameters() {
        this.rotateRadians.bind(rotateDeltaRadians.add(rotateInitRadians));
        this.positionX.bind(positionDeltaX.add(positionInitX));
        this.positionY.bind(positionDeltaY.add(positionInitY));
    }

    private void buildOthers() {
        if (this.symmetryModel != null) {
            symmetryModel.dispose();
        }
        if (this.motifModel != null) {
            this.motifModel.dispose();
        }
        this.symmetryModel = SymmetryModelFactory.getSymmetryModel(cellModel, designRule.symmetryType, designRule.adaptive);
        this.motifModel = new MotifModel(this, symmetryModel.artworkSeeds);

    }

    public void dispose() {
        this.positionDeltaX.unbind();
        this.positionDeltaY.unbind();
        this.reflectionAxis.unbind();
        this.reflection.unbind();
        this.rotateRadians.unbind();
        this.positionX.unbind();
        this.positionY.unbind();
        this.rotateInitRadians.unbind();
        this.rotateDeltaRadians.unbind();
        this.positionInitX.unbind();
        this.positionInitY.unbind();
        this.scaleX.unbind();
        this.scaleY.unbind();
        this.rotatePivotX.unbind();
        this.rotatePivotY.unbind();
        this.motifModel.dispose();
        this.symmetryModel.dispose();
        nullify();
    }

    private void nullify() {
        this.positionDeltaX = null;
        this.positionDeltaY = null;
        this.reflectionAxis = null;
        this.reflection = null;
        this.rotateRadians = null;
        this.positionX = null;
        this.positionY = null;
        this.rotateInitRadians = null;
        this.rotateDeltaRadians = null;
        this.positionInitX = null;
        this.positionInitY = null;
        this.scaleX = null;
        this.scaleY = null;
        this.rotatePivotX = null;
        this.rotatePivotY = null;
        this.motifModel = null;
        this.symmetryModel = null;
    }

    public void setDesignRule(DesignRule rule) {
        this.designRule = rule;
        buildOthers();
    }

}
