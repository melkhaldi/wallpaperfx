/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.artifact.artwork;

import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import wallpaperfx.design.artifact.tile.TileModel;
import wallpaperfx.design.core.Enums.Axis;
import wallpaperfx.design.core.Enums.Pivot;

/**
 *
 * @author Maher Elkhaldi
 */
public class ArtworkModel {

    //settable parameters:
    public SimpleDoubleProperty rotateDeltaRadians = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty positionDeltaX = new SimpleDoubleProperty(0);
    public SimpleDoubleProperty positionDeltaY = new SimpleDoubleProperty(0);

    public SimpleDoubleProperty scaleX = new SimpleDoubleProperty(1);
    public SimpleDoubleProperty scaleY = new SimpleDoubleProperty(1);

    //computed parameters:
    public ReadOnlyDoubleWrapper rotateRadians = new ReadOnlyDoubleWrapper(0);
    public ReadOnlyDoubleWrapper positionX = new ReadOnlyDoubleWrapper(0);
    public ReadOnlyDoubleWrapper positionY = new ReadOnlyDoubleWrapper(0);
    public ReadOnlyObjectWrapper<Pivot> pivot = new ReadOnlyObjectWrapper<>();
    public SimpleBooleanProperty reflection = new SimpleBooleanProperty(false);
    public SimpleObjectProperty<Axis> reflectionAxis = new SimpleObjectProperty<>();
    public ReadOnlyDoubleWrapper rotateInitRadians = new ReadOnlyDoubleWrapper(0);
    private SimpleDoubleProperty positionInitX = new SimpleDoubleProperty(0);
    private SimpleDoubleProperty positionInitY = new SimpleDoubleProperty(0);

    //fixed parameters
    //essential parameters
    public final TileModel tileModel;

    public final ArtworkSeed seed;

    /**
     *
     * @param tileModel
     * @param seed
     */
    public ArtworkModel(TileModel tileModel, ArtworkSeed seed) {
        this.tileModel = tileModel;

        this.seed = seed;
        setInitAngle(tileModel.designRule.adaptive);

        computedParameters();

    }

    private void computedParameters() {
        this.pivot.set(seed.pivot);
        this.reflectionAxis.set(seed.axis);
        this.reflection.bind(seed.reflection);
        this.rotateRadians.bind(rotateDeltaRadians.add(rotateInitRadians));
        this.positionX.bind(positionDeltaX.add(positionInitX));
        this.positionY.bind(positionDeltaY.add(positionInitY));
        setInitPosition();
    }

  

    private void setInitAngle(boolean val) {
        rotateInitRadians.unbind();
        if (!val) {
            rotateInitRadians.bind(seed.rotateInitRadians);
        } else {
            rotateInitRadians.bind(seed.rotateInitRadians.add(tileModel.cellModel.radiansOUProperty()));
        }
    }

    protected final void setInitPosition() {
        final SimpleDoubleProperty posX = new SimpleDoubleProperty(0);
        final SimpleDoubleProperty posY = new SimpleDoubleProperty(0);
        double factor = 2;
        switch (pivot.get()) {
            case CPOINT:
                posX.bind(tileModel.cellModel.pointCXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointCYProperty().divide(factor));
                break;
            case OPOINT:
                posX.bind(tileModel.cellModel.pointOXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointOYProperty().divide(factor));
                break;
            case UPOINT:
                posX.bind(tileModel.cellModel.pointUXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointUYProperty().divide(factor));
                break;
            case VPOINT:
                posX.bind(tileModel.cellModel.pointVXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointVYProperty().divide(factor));
                break;
            case WPOINT:
                posX.bind(tileModel.cellModel.pointWXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointWYProperty().divide(factor));
                break;
            case MPOINT:
                posX.bind(tileModel.cellModel.pointMXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointMYProperty().divide(factor));
                break;
            case NPOINT:
                posX.bind(tileModel.cellModel.pointNXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointNYProperty().divide(factor));
                break;
            case CRPOINT:
                posX.bind(tileModel.cellModel.pointCRXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointCRYProperty().divide(factor));
                break;
            case CLPOINT:
                posX.bind(tileModel.cellModel.pointCLXProperty().divide(factor));
                posY.bind(tileModel.cellModel.pointCLYProperty().divide(factor));
                break;
            
        }

        positionInitX.bind(posX.subtract(tileModel.cellModel.pointCXProperty().divide(factor)));
        positionInitY.bind(posY.subtract(tileModel.cellModel.pointCYProperty().divide(factor)));

    }

    public void dispose() {
        this.pivot.unbind();
        this.reflectionAxis.unbind();
        this.reflection.unbind();
        this.rotateRadians.unbind();
        this.positionX.unbind();
        this.positionY.unbind();
        this.rotateInitRadians.unbind();
        this.rotateDeltaRadians.unbind();
        this.positionInitX.unbind();
        this.positionInitY.unbind();
        this.positionDeltaX.unbind();
        this.positionDeltaY.unbind();
        this.scaleX.unbind();
        this.scaleY.unbind();
        nullify ();

    }
    
    public void nullify (){
        this.pivot = null;
        this.reflectionAxis = null;
        this.reflection = null;
        this.rotateRadians = null;
        this.positionX = null;
        this.positionY = null;
        this.rotateInitRadians = null;
        this.rotateDeltaRadians = null;
        this.positionInitX = null;
        this.positionInitY = null;
        this.positionDeltaX = null;
        this.positionDeltaY = null;
        this.scaleX = null;
        this.scaleY = null;
    }

}
