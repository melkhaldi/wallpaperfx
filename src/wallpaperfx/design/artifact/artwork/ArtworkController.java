/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.artifact.artwork;

import javafx.beans.binding.DoubleExpression;
import static wallpaperfx.design.core.Enums.Pivot.CPOINT;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PGG;

/**
 *
 * @author Maher Elkhaldi
 */
public class ArtworkController {

    public final ArtworkModel model;
    public final ArtworkView view;

    public ArtworkController(ArtworkModel shapeModel) {
        this.model = shapeModel;
        this.view = new ArtworkView(this.model.tileModel.cellModel);
        link();

    }

    public void dispose() {
        view.dispose();
        model.dispose();
    }

    public ArtworkView set() {
        this.view.clear();
        this.view.addChild(model.tileModel.designRule.artFactory.build());
        return view;
    }

    private void link() {

        if (model.seed.symmetryModelType == PGG && model.seed.pivot == CPOINT) {
            view.positionX.set(model.positionX.get());
            view.positionY.set(model.positionY.get());
            view.rotateRadians.bind(model.rotateRadians);
            view.scaleX.bind(model.scaleX);
            view.scaleY.bind(model.scaleY);
            view.reflection.bind(model.reflection);
            view.reflectionAxis.bind(model.reflectionAxis);
        } else if (model.seed.engage) {
            view.positionX.bind(model.positionX);
            view.positionY.bind(model.positionY);
            view.rotateRadians.bind(model.rotateRadians);
            view.scaleX.bind(model.scaleX);
            view.scaleY.bind(model.scaleY);
            view.reflection.bind(model.reflection);
            view.reflectionAxis.bind(model.reflectionAxis);
        }
    }

    public void bindDeltaPositionXPropertyTo(DoubleExpression expression) {
        model.positionDeltaX.bind(expression);
    }

    public void bindDeltaPositionYPropertyTo(DoubleExpression expression) {
        model.positionDeltaY.bind(expression);
    }

    public void bindScaleXPropertyTo(DoubleExpression expression) {
        model.scaleX.bind(expression);
    }

    public void bindScaleYPropertyTo(DoubleExpression expression) {
        model.scaleY.bind(expression);
    }

    public void bindDeltaRadiansPropertyTo(DoubleExpression expression) {
        model.rotateDeltaRadians.bind(expression);
    }

    public void unbindPositionXPropertyTo() {
        model.positionDeltaX.unbind();
    }

    public void unbindPositionYPropertyTo() {
        model.positionDeltaX.unbind();
    }

    public void unbindDeltaRadiansPropertyTo() {
        model.positionDeltaX.unbind();
    }

}
