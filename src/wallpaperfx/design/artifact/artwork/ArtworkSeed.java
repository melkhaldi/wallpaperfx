/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.artifact.artwork;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import wallpaperfx.design.core.Enums.Axis;
import wallpaperfx.design.core.Enums.Pivot;
import wallpaperfx.design.core.Enums.SymmetryModelType;

/**
 *
 * @author Maher Elkhaldi
 */
public class ArtworkSeed {

    public Pivot pivot;
    public SimpleDoubleProperty rotateInitRadians = new SimpleDoubleProperty();
    public SimpleBooleanProperty reflection = new SimpleBooleanProperty(false);
    public Axis axis;
    public boolean angleAdaptive;
    public SymmetryModelType symmetryModelType;
    public final boolean engage;
   

    public ArtworkSeed(SymmetryModelType symmetryModelType, Pivot pivot, double rotateInitRadians, Axis axis, boolean reflectionToggle, boolean angleAdaptive, boolean engage) {
        this.engage = engage;
         this.pivot = pivot;
        this.rotateInitRadians.set(rotateInitRadians);
        this.axis = axis;
        this.symmetryModelType = symmetryModelType;
        this.reflection.set(reflectionToggle);
        this.angleAdaptive = angleAdaptive;
    }
    
    public final void dispose() {
        rotateInitRadians.unbind();
        reflection.unbind();
    }

   

    
    
}
