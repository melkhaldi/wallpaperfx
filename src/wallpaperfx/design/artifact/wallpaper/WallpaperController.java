/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wallpaperfx.design.artifact.wallpaper;

import java.util.ArrayList;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import wallpaperfx.design.artifact.tile.TileController;
import wallpaperfx.design.core.ElementGrid;

/**
 *
 * @author Maher Elkhaldi
 */
public final class WallpaperController extends ElementGrid {

    private final ReadOnlyIntegerWrapper release = new ReadOnlyIntegerWrapper(0);

    public final ReadOnlyObjectWrapper<TileController> uMinvMinTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<TileController> uMaxvMaxTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<TileController> uMaxvMinTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<TileController> uMinvMaxTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public ArrayList<ArrayList<TileController>> tileControllerLists = new ArrayList();
    public WallpaperModel model;
    public WallpaperView view;
    private final ChangeListener<Number> modelListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
        refresh();
    };

    public WallpaperController(WallpaperModel wallpaperModel, WallpaperView view) {
        this.model = wallpaperModel;
        this.view = view;
        this.model.releaseProperty().addListener(modelListener);
        this.model.releaseProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            refresh();
        });

        this.uMaxvMaxTileControllerProperty.addListener((ObservableValue<? extends TileController> observable, TileController oldValue, TileController newValue) -> {
            this.release.set(release.get() + 1);
        });
        refresh();
    }

    public int getRelease() {
        return release.get();
    }

    private void refresh() {
        setUV(model.latticeModel.getUCount(), model.latticeModel.getVCount());
        setCorners();
    }

    /**
     * @return the current release as {@link ReadOnlyIntegerProperty}
     */
    public ReadOnlyIntegerProperty releaseProperty() {
        return release;
    }

    @Override
    public String toString() {
        String val = "\n[" + super.toString() + "]" + "\n>>";
        for (int i = 0; i < tileControllerLists.size(); i++) {
            for (int j = 0; j < tileControllerLists.get(i).size(); j++) {
                TileController tileController = tileControllerLists.get(i).get(j);
                val += "\n (" + (tileController.toString() + ") of Index<" + i + "," + j + "> with #" + tileController.motifController.artworkControllers.size() + " artwork controllers");
            }
        }
        return val + "\n<<";
    }

    @Override
    protected void setUV(int newCountI, int newCountJ) {
        setU(newCountI);
        setV(newCountJ);
    }

    @Override
    protected void setU(int newCount) {
        if (newCount > tileControllerLists.size()) {
            addU(newCount);
        } else {
            removeU(newCount);
        }
    }

    @Override
    protected void addU(int newCount) {
        for (int u = tileControllerLists.size(); u < newCount; u++) {
            tileControllerLists.add(new ArrayList());
            for (int v = 0; v < tileControllerLists.get(0).size(); v++) {
                addElement(u, v);
            }
        }
    }

    @Override
    protected void removeU(int newCount) {
        while (tileControllerLists.size() > newCount && tileControllerLists.size() > 0) {
            tileControllerLists.get(tileControllerLists.size() - 1).stream().forEach((controller) -> {
                controller.dispose();
            });
            tileControllerLists.remove(tileControllerLists.size() - 1);
        }

    }

    @Override
    protected void setV(int newCount) {

        if (newCount > tileControllerLists.get(0).size()) {

            addV(newCount);
        } else {
            removeV(newCount);
        }
    }

    @Override
    protected void removeV(int newCount) {
        tileControllerLists.stream().forEach(list -> {
            while (list.size() > newCount) {
                TileController controller = list.get(list.size() - 1);
                controller.dispose();
                list.remove(controller);
            }
        });

    }

    @Override
    protected void addV(int newCount) {
        for (int u = 0; u < tileControllerLists.size(); u++) {
            for (int v = tileControllerLists.get(u).size(); v < newCount; v++) {
                addElement(u, v);
            }
        }
    }

    @Override
    protected void addElement(int i, int j) {
        TileController tileController = new TileController(model.tileModelLists.get(i).get(j));
        tileControllerLists.get(i).add(tileController);
    }

    private void setCorners() {
        uMinvMinTileControllerProperty.set(tileControllerLists.get(0).get(0));
        uMinvMaxTileControllerProperty.set(tileControllerLists.get(0).get(model.latticeModel.getVCount() - 1));
        uMaxvMinTileControllerProperty.set(tileControllerLists.get(tileControllerLists.size() - 1).get(0));
        uMaxvMaxTileControllerProperty.set(tileControllerLists.get(model.latticeModel.getUCount() - 1).get(model.latticeModel.getVCount() - 1));
    }

    @Override
    public final void dispose() {
        this.model.releaseProperty().removeListener(modelListener);
        this.model.clearDesignRuleListExceptDefaultDesignRule();

        this.tileControllerLists.forEach(list -> {
            list.forEach(controller -> {
                controller.dispose();
            });
        });
        this.model.dispose();
        this.view.dispose();
        this.tileControllerLists.clear();
        nullify();
    }

    private void nullify() {
        this.model = null;
        this.view = null;
        this.tileControllerLists = null;
    }

    public void updateRelease() {
        this.release.set(release.get() + 1);
    }

   

}
