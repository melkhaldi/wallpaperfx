/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.artifact.wallpaper;

import java.util.ArrayList;
import java.util.Arrays;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import wallpaperfx.design.artifact.tile.TileModel;
import wallpaperfx.design.core.ElementGrid;
import wallpaperfx.design.structure.lattice.LatticeModel;
import wallpaperfx.designrules.rules.DesignRule;

/**
 *
 * @author Maher Elkhaldi
 */
public final class WallpaperModel extends ElementGrid {

    public final ReadOnlyObjectWrapper<TileModel> uMinvMinTileModelProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<TileModel> uMaxvMaxTileModelProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<TileModel> uMaxvMinTileModelProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<TileModel> uMinvMaxTileModelProperty = new ReadOnlyObjectWrapper<>();
    private final ReadOnlyIntegerWrapper releaseProperty = new ReadOnlyIntegerWrapper(0);

    public final ArrayList<ArrayList<TileModel>> tileModelLists = new ArrayList();
    protected LatticeModel latticeModel;
    private ArrayList<DesignRule> designRules = new ArrayList();
    private DesignRule defaultDesignRule;

    private final ChangeListener<Number> latticeListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
        setUV(latticeModel.getUCount(), latticeModel.getVCount());
    };

    public WallpaperModel(LatticeModel latticeModel, DesignRule defaultDesignRule) {
        this.latticeModel = latticeModel;
        this.defaultDesignRule = defaultDesignRule;
        addDesignRuleToList(this.defaultDesignRule);
        this.latticeModel.releaseProperty().addListener(latticeListener);
        this.uMaxvMaxTileModelProperty.addListener((ObservableValue<? extends TileModel> observable, TileModel oldValue, TileModel newValue) -> {
            this.releaseProperty.set(releaseProperty.get() + 1);
        });
        setUV(latticeModel.getUCount(), latticeModel.getVCount());

    }

    public WallpaperModel(LatticeModel latticeModel, DesignRule defaultDesignRule, DesignRule... designRules) {
        this.latticeModel = latticeModel;
        this.defaultDesignRule = defaultDesignRule;
        addDesignRuleToList(this.defaultDesignRule);
        for (DesignRule designRule : designRules) {
            addDesignRuleToList(designRule);
        }
        this.latticeModel.releaseProperty().addListener(latticeListener);
        this.uMaxvMaxTileModelProperty.addListener((ObservableValue<? extends TileModel> observable, TileModel oldValue, TileModel newValue) -> {
            this.releaseProperty.set(releaseProperty.get() + 1);
        });
        setUV(latticeModel.getUCount(), latticeModel.getVCount());

    }

    public void addDesignRuleToList(DesignRule designRule) {
        if (!this.designRules.contains(designRule)) {
            this.designRules.add(designRule);
        }
    }

    public void clearDesignRuleListExceptDefaultDesignRule() {
        this.designRules.clear();
        this.designRules.add(defaultDesignRule);
    }

    public int getRelease() {
        return releaseProperty.get();
    }

    /**
     * @return the current release as {@link ReadOnlyIntegerProperty}
     */
    public ReadOnlyIntegerProperty releaseProperty() {
        return releaseProperty;
    }

    public ReadOnlyListProperty<DesignRule> designRulesReadOnly() {
        return new ReadOnlyListWrapper(FXCollections.observableArrayList(designRules)).getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty<DesignRule> defaultDesignRuleReadOnly() {
        return new ReadOnlyObjectWrapper(defaultDesignRule).getReadOnlyProperty();
    }

    public void changeDesignRules(DesignRule... rules) {
        this.designRules.clear();
        this.designRules.addAll(Arrays.asList(rules));
        this.defaultDesignRule = designRules.get(0);
        releaseProperty.set(releaseProperty.get() + 1);
    }

    public void addDesignRules(DesignRule... rules) {
        this.designRules.addAll(Arrays.asList(rules));
        releaseProperty.set(releaseProperty.get() + 1);
    }

    @Override
    public String toString() {
        String val = "\n[" + super.toString() + "]" + "\n>>";
        for (int i = 0; i < tileModelLists.size(); i++) {
            for (int j = 0; j < tileModelLists.get(i).size(); j++) {
                TileModel tileModel = tileModelLists.get(i).get(j);
                val += "\n (" + (tileModel.toString() + ") of Index<" + i + "," + j + "> with {" + tileModel.symmetryModel.getClass().getName() + "}");
            }
        }
        return val + "\n<<";
    }

    @Override
    protected void setUV(int u, int v) {
        if (tileModelLists.size() < 1) {//initial setup
            for (int i = 0; i < u; i++) {
                tileModelLists.add(new ArrayList());
                for (int j = 0; j < v; j++) {
                    addElement(i, j);
                }
            }
            setCorners();
        } else {
            setU(u);
            setV(v);
        }

    }

    @Override
    protected void setU(int newCount) {
        if (newCount > tileModelLists.size()) {
            addU(newCount);
        } else {
            removeU(newCount);
        }
        setCorners();
    }

    /**
     *
     * @param newCount
     */
    @Override
    protected void addU(int newCount) {
        for (int u = tileModelLists.size(); u < newCount; u++) {
            tileModelLists.add(new ArrayList());
            for (int v = 0; v < tileModelLists.get(0).size(); v++) {
                addElement(u, v);
            }
        }
    }

    @Override
    protected void removeU(int newCount) {
        while (tileModelLists.size() > newCount && tileModelLists.size() > 0) {
            tileModelLists.get(tileModelLists.size() - 1).stream().forEach((tileModel) -> {
                tileModel.dispose();
            });
            tileModelLists.remove(tileModelLists.size() - 1);
        }

    }

    @Override
    protected void setV(int newCount) {
        if (newCount > tileModelLists.get(0).size()) {
            addV(newCount);
        } else {
            removeV(newCount);
        }
        setCorners();
    }

    @Override
    protected void removeV(int newCount) {
        tileModelLists.stream().forEach(list -> {
            while (list.size() > newCount) {
                TileModel toRemove = list.get(list.size() - 1);
                toRemove.dispose();
                list.remove(toRemove);
            }
        });

    }

    private boolean assertU() {
        return tileModelLists.size() > 0 && tileModelLists.size() == latticeModel.getUCount();
    }

    private boolean assertV() {
        return tileModelLists.size() > 0 && tileModelLists.get(0).size() == latticeModel.getVCount();
    }

    @Override
    protected void addV(int newCount) {
        for (int u = 0; u < tileModelLists.size(); u++) {
            for (int v = tileModelLists.get(u).size(); v < newCount; v++) {
                addElement(u, v);
            }
        }
    }

    @Override
    protected void addElement(int i, int j) {
        TileModel tileModel = new TileModel(latticeModel.cellModelLists.get(i).get(j), defaultDesignRule);
        tileModelLists.get(i).add(tileModel);
    }

    private void setCorners() {
        if (assertU() && assertV()) {
            uMinvMinTileModelProperty.set(tileModelLists.get(0).get(0));
            uMinvMaxTileModelProperty.set(tileModelLists.get(0).get(latticeModel.getVCount() - 1));
            uMaxvMinTileModelProperty.set(tileModelLists.get(tileModelLists.size() - 1).get(0));
            uMaxvMaxTileModelProperty.set(tileModelLists.get(latticeModel.getUCount() - 1).get(latticeModel.getVCount() - 1));

        }
    }

    @Override
    public final void dispose() {
        this.latticeModel.releaseProperty().removeListener(latticeListener);
        this.designRules.clear();
        this.tileModelLists.forEach(list -> {
            list.stream().forEach(tile
                    -> tile.dispose());
        });
        tileModelLists.clear();
    }

}
