/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.artifact.wallpaper;

import wallpaperfx.design.structure.lattice.Lattice;
import wallpaperfx.designrules.rules.DesignRule;

/**
 *
 * @author Maher Elkhaldi
 */
public final class Wallpaper {

    public final WallpaperModel model;
    public WallpaperController controller;
    public WallpaperView view;

    public Wallpaper(Lattice lattice, DesignRule initDesigngRule) {
        model = new WallpaperModel(lattice.model, initDesigngRule);
        view = new WallpaperView();
        controller = new WallpaperController(model, view);
        setReferenceCellAnglesBasedOnReferenceCell();
    }

    public Wallpaper(Lattice lattice, DesignRule initDesigngRule, DesignRule... otherRules) {
        model = new WallpaperModel(lattice.model, initDesigngRule, otherRules);
        view = new WallpaperView();
        controller = new WallpaperController(model, view);
        setReferenceCellAnglesBasedOnReferenceCell();
    }

    public void setDesignRule(int i, int j, DesignRule rule) {
        model.tileModelLists.get(i).get(j).setDesignRule(rule);
        if (i == 0 && j == 0) {
            setReferenceCellAnglesBasedOnReferenceCell();
        }
    }

    private void setReferenceCellAnglesBasedOnReferenceCell() {
        double uRadians = 0;
        double vRadians = Math.PI / 2;

        switch (model.uMinvMinTileModelProperty.get().designRule.symmetryType) {
            case P1:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case P2:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case P3:
                uRadians = 0;
                vRadians = Math.PI * 2 / 3;
                break;
            case P31M:
                uRadians = 0;
                vRadians = Math.PI * 2 / 3;
                break;
            case P3M1:
                uRadians = 0;
                vRadians = Math.PI * 2 / 3;
                break;
            case P4:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case P4M:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case P4MG:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case P6:
                uRadians = 0;
                vRadians = Math.PI * 2 / 3;
                break;
            case P6M:
                uRadians = 0;
                vRadians = Math.PI * 2 / 3;
                break;
            case PG:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case PM:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case PMM:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case CMM:
                uRadians = Math.PI / 4;
                vRadians = -Math.PI / 4;
                break;
            case CM:
                uRadians = Math.PI / 4;
                vRadians = -Math.PI / 4;
                break;
            case PMG:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case PGG:
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;

        }
        model.uMinvMinTileModelProperty.get().cellModel.setRadiansOU(uRadians);
        model.uMinvMinTileModelProperty.get().cellModel.setRadiansOV(vRadians);

    }

}
