/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.structure.cell;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.shape.Line;

/**
 *
 * @author Maher Elkhaldi
 */
public class CellView extends Group {

    public final CellModel model;

    public SimpleBooleanProperty boundaryLoaded = new SimpleBooleanProperty(false);
    public SimpleBooleanProperty internalAxesLoaded = new SimpleBooleanProperty(false);
    public SimpleBooleanProperty medianAxesLoaded = new SimpleBooleanProperty(false);
    public SimpleBooleanProperty orthoAxesLoaded = new SimpleBooleanProperty(false);
    public SimpleBooleanProperty diagonalAxesLoaded = new SimpleBooleanProperty(false);

    public final String BOUNDARYCSS = "-fx-fill: transparent; -fx-stroke-width: 0.2; -fx-stroke: black; -fx-stroke-dash-array:8 8";
    public final String DIAGONALCSS = "-fx-fill: transparent; -fx-stroke-width: 0.5; -fx-stroke: blue; -fx-stroke-dash-array:10 10";
    public final String HORIZONTALCSS = "-fx-fill: transparent; -fx-stroke-width: 1.75; -fx-stroke: red;";
    public final String INTERNALCSS = "-fx-fill: transparent; -fx-stroke-width: 0.5; -fx-stroke: magenta; -fx-stroke-dash-array:5 5";
    public final String MEDIANCSS = "-fx-fill: transparent; -fx-stroke-width: 0.5; -fx-stroke: darkgray; -fx-stroke-dash-array:5 5";
    public final String VERTICALCSS = "-fx-fill: transparent; -fx-stroke-width: 1.75; -fx-stroke: green;";

    protected Line HLINE = new Line(0, 0, 0, 0);
    protected Line I_J = new Line(0, 0, 0, 0);
    protected Line I_K = new Line(0, 0, 0, 0);
    protected Line J_K = new Line(0, 0, 0, 0);
    protected Line K_L = new Line(0, 0, 0, 0);
    protected Line L_I = new Line(0, 0, 0, 0);
    protected Line L_J = new Line(0, 0, 0, 0);
    protected Line M_W = new Line(0, 0, 0, 0);
    protected Line N_W = new Line(0, 0, 0, 0);
    protected Line O_M = new Line(0, 0, 0, 0);
    protected Line O_N = new Line(0, 0, 0, 0);
    protected Line O_U = new Line(0, 0, 0, 0);
    protected Line O_V = new Line(0, 0, 0, 0);
    protected Line O_W = new Line(0, 0, 0, 0);
    protected Line U_W = new Line(0, 0, 0, 0);
    protected Line VLINE = new Line(0, 0, 0, 0);
    protected Line V_U = new Line(0, 0, 0, 0);
    protected Line V_W = new Line(0, 0, 0, 0);
    protected Group boundaryLines = new Group();
    protected Group diagonalAxes = new Group();
    protected Group internalAxes = new Group();
    protected Group medianAxes = new Group();
    protected Group orthoAxes = new Group();

    public CellView(CellModel cellModel) {
        this.model = cellModel;
        bindAll();
        addElements();
        setStyles();
        listen();
        sendViewToBack();
        showAllDecoration(true);
    }

    protected final void showAllDecoration(boolean val) {
        boundaryLoaded.set(val);
        internalAxesLoaded.set(val);
        medianAxesLoaded.set(val);
        orthoAxesLoaded.set(val);
        diagonalAxesLoaded.set(val);
    }

    private void setStyles() {
        setStyle(boundaryLines, BOUNDARYCSS);
        setStyle(diagonalAxes, DIAGONALCSS);
        setStyle(internalAxes, INTERNALCSS);
        setStyle(medianAxes, MEDIANCSS);
        VLINE.styleProperty().set(VERTICALCSS);
        HLINE.styleProperty().set(HORIZONTALCSS);
    }

    private void unbindToggles() {
        this.boundaryLoaded.unbind();
        this.internalAxesLoaded.unbind();
        this.medianAxesLoaded.unbind();
        this.orthoAxesLoaded.unbind();
        this.diagonalAxesLoaded.unbind();
    }

    private void clearContainers() {
        this.boundaryLines.getChildren().clear();
        this.diagonalAxes.getChildren().clear();
        this.internalAxes.getChildren().clear();
        this.medianAxes.getChildren().clear();
        this.orthoAxes.getChildren().clear();
    }

    public final void dispose() {
        unbindAllLines();
        unbindToggles();
        clearContainers();
        super.getChildren().clear();
        nullify();
    }

    private void nullify() {
        this.HLINE = null;
        this.I_J = null;
        this.I_K = null;
        this.J_K = null;
        this.K_L = null;
        this.L_I = null;
        this.L_J = null;
        this.M_W = null;
        this.N_W = null;
        this.O_M = null;
        this.O_N = null;
        this.O_U = null;
        this.O_V = null;
        this.O_W = null;
        this.U_W = null;
        this.VLINE = null;
        this.V_U = null;
        this.V_W = null;
        this.boundaryLines = null;
        this.diagonalAxes = null;
        this.internalAxes = null;
        this.medianAxes = null;
        this.orthoAxes = null;
        this.boundaryLoaded = null;
        this.internalAxesLoaded = null;
        this.medianAxesLoaded = null;
        this.orthoAxesLoaded = null;
        this.diagonalAxesLoaded = null;

    }

    private void listen() {

        boundaryLoaded.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue && !getChildren().contains(boundaryLines)) {
                super.getChildren().add(boundaryLines);
                boundaryLines.toFront();
                bindBoundary();
                sendViewToBack();
            } else {
                unbindBoundary();
                super.getChildren().remove(boundaryLines);
            }

        });

        internalAxesLoaded.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue && !getChildren().contains(internalAxes)) {
                super.getChildren().add(internalAxes);
                bindInternalAxes();
                internalAxes.toFront();
                sendViewToBack();
            } else {
                unbindInternalAxes();
                super.getChildren().remove(internalAxes);
            }
        });
        orthoAxesLoaded.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue && !getChildren().contains(orthoAxes)) {
                super.getChildren().add(orthoAxes);
                bindOrthogonalAxes();
                orthoAxes.toFront();
                sendViewToBack();
            } else {
                unbindOrthogonalAxes();
                super.getChildren().remove(orthoAxes);
            }
        });
        medianAxesLoaded.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue && !getChildren().contains(medianAxes)) {
                super.getChildren().add(medianAxes);
                bindMedianAxes();
                medianAxes.toFront();
                sendViewToBack();
            } else {
                unbindMedianAxes();
                super.getChildren().remove(medianAxes);
            }
        });
        diagonalAxesLoaded.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue && !getChildren().contains(diagonalAxes)) {
                super.getChildren().add(diagonalAxes);
                bindDiagonalAxes();
                diagonalAxes.toFront();
                sendViewToBack();
            } else {
                unbindDiagonalAxes();
                super.getChildren().remove(diagonalAxes);
            }
        });

    }

    private void addElements() {
        addBoundary();
        addInternalAxes();
        addDiagonalAxes();
        addMedianAxes();
        addOrthogonalAxes();
    }

    private void addBoundary() {
        boundaryLines.getChildren().addAll(O_U, U_W, V_W, O_V);
        boundaryLines.toBack();
        boundaryLines.setMouseTransparent(true);
    }

    private void addDiagonalAxes() {
        diagonalAxes.getChildren().add(O_W);
        diagonalAxes.getChildren().add(V_U);
        diagonalAxes.toBack();
        diagonalAxes.setMouseTransparent(true);
    }

    private void addInternalAxes() {
        internalAxes.getChildren().addAll(O_N, O_M, M_W, N_W);
        internalAxes.toBack();
        internalAxes.setMouseTransparent(true);
    }

    private void addMedianAxes() {
        medianAxes.getChildren().addAll(I_K, L_J, L_I, I_J, J_K, K_L);
        medianAxes.toBack();
        medianAxes.setMouseTransparent(true);
    }

    private void addOrthogonalAxes() {
        orthoAxes.getChildren().addAll(VLINE, HLINE);
        orthoAxes.toBack();
        orthoAxes.setMouseTransparent(true);
    }

    private void unbindAllLines() {
        unbindBoundary();
        unbindDiagonalAxes();
        unbindInternalAxes();
        unbindMedianAxes();
        unbindOrthogonalAxes();
    }

    private void unbindBoundary() {
        this.O_U.startXProperty().unbind();
        this.O_U.startYProperty().unbind();
        this.O_U.endXProperty().unbind();
        this.O_U.endYProperty().unbind();
        this.U_W.startXProperty().unbind();
        this.U_W.startYProperty().unbind();
        this.U_W.endXProperty().unbind();
        this.U_W.endYProperty().unbind();
        this.V_W.startXProperty().unbind();
        this.V_W.startYProperty().unbind();
        this.V_W.endXProperty().unbind();
        this.V_W.endYProperty().unbind();
        this.O_V.startXProperty().unbind();
        this.O_V.startYProperty().unbind();
        this.O_V.endXProperty().unbind();
        this.O_V.endYProperty().unbind();

    }

    private void unbindDiagonalAxes() {
        this.O_N.startXProperty().unbind();
        this.O_N.startYProperty().unbind();
        this.O_N.endXProperty().unbind();
        this.O_N.endYProperty().unbind();
        this.O_M.startXProperty().unbind();
        this.O_M.startYProperty().unbind();
        this.O_M.endXProperty().unbind();
        this.O_M.endYProperty().unbind();
        this.M_W.startXProperty().unbind();
        this.M_W.startYProperty().unbind();
        this.M_W.endXProperty().unbind();
        this.M_W.endYProperty().unbind();
        this.N_W.startXProperty().unbind();
        this.N_W.startYProperty().unbind();
        this.N_W.endXProperty().unbind();
        this.N_W.endYProperty().unbind();

    }

    private void unbindInternalAxes() {
        this.O_W.startXProperty().unbind();
        this.O_W.startYProperty().unbind();
        this.O_W.endXProperty().unbind();
        this.O_W.endYProperty().unbind();
        this.V_U.startXProperty().unbind();
        this.V_U.startYProperty().unbind();
        this.V_U.endXProperty().unbind();
        this.V_U.endYProperty().unbind();

    }

    private void unbindMedianAxes() {
        this.I_K.startXProperty().unbind();
        this.I_K.startYProperty().unbind();
        this.I_K.endXProperty().unbind();
        this.I_K.endYProperty().unbind();
        this.L_J.startXProperty().unbind();
        this.L_J.startYProperty().unbind();
        this.L_J.endXProperty().unbind();
        this.L_J.endYProperty().unbind();

        this.L_I.startXProperty().unbind();
        this.L_I.startYProperty().unbind();
        this.L_I.endXProperty().unbind();
        this.L_I.endYProperty().unbind();
        this.I_J.startXProperty().unbind();
        this.I_J.startYProperty().unbind();
        this.I_J.endXProperty().unbind();
        this.I_J.endYProperty().unbind();
        this.J_K.startXProperty().unbind();
        this.J_K.startYProperty().unbind();
        this.J_K.endXProperty().unbind();
        this.J_K.endYProperty().unbind();
        this.K_L.startXProperty().unbind();
        this.K_L.startYProperty().unbind();
        this.K_L.endXProperty().unbind();
        this.K_L.endYProperty().unbind();
    }

    private void unbindOrthogonalAxes() {

        this.VLINE.startXProperty().unbind();
        this.VLINE.startYProperty().unbind();
        this.VLINE.endXProperty().unbind();
        this.VLINE.endYProperty().unbind();
        this.HLINE.startXProperty().unbind();
        this.HLINE.startYProperty().unbind();
        this.HLINE.endXProperty().unbind();
        this.HLINE.endYProperty().unbind();
    }

    private void bindAll() {
        bindBoundary();
        bindDiagonalAxes();
        bindInternalAxes();
        bindMedianAxes();
        bindOrthogonalAxes();
    }

    public final void sendViewToBack() {

        //lLine I_K
        V_U.toBack();
        O_W.toBack();
        I_K.toBack();
        L_J.toBack();
        O_U.toBack();
        U_W.toBack();
        V_W.toBack();
        O_V.toBack();
        O_N.toBack();
        O_M.toBack();
        M_W.toBack();
        N_W.toBack();
        L_I.toBack();
        I_J.toBack();
        J_K.toBack();
        K_L.toBack();
        this.toBack();
    }

    private void bindBoundary() {
        O_U.startXProperty().bind(model.pointOXProperty());
        O_U.startYProperty().bind(model.pointOYProperty());
        O_U.endXProperty().bind(model.pointUXProperty());
        O_U.endYProperty().bind(model.pointUYProperty());
        U_W.startXProperty().bind(model.pointUXProperty());
        U_W.startYProperty().bind(model.pointUYProperty());
        U_W.endXProperty().bind(model.pointWXProperty());
        U_W.endYProperty().bind(model.pointWYProperty());
        V_W.startXProperty().bind(model.pointWXProperty());
        V_W.startYProperty().bind(model.pointWYProperty());
        V_W.endXProperty().bind(model.pointVXProperty());
        V_W.endYProperty().bind(model.pointVYProperty());
        O_V.startXProperty().bind(model.pointVXProperty());
        O_V.startYProperty().bind(model.pointVYProperty());
        O_V.endXProperty().bind(model.pointOXProperty());
        O_V.endYProperty().bind(model.pointOYProperty());
    }

    private void bindDiagonalAxes() {
        O_W.startXProperty().bind(model.pointOXProperty());
        O_W.startYProperty().bind(model.pointOYProperty());
        O_W.endXProperty().bind(model.pointWXProperty());
        O_W.endYProperty().bind(model.pointWYProperty());
        V_U.startXProperty().bind(model.pointVXProperty());
        V_U.startYProperty().bind(model.pointVYProperty());
        V_U.endXProperty().bind(model.pointUXProperty());
        V_U.endYProperty().bind(model.pointUYProperty());
    }

    private void bindInternalAxes() {
        O_N.startXProperty().bind(model.pointOXProperty());
        O_N.startYProperty().bind(model.pointOYProperty());
        O_N.endXProperty().bind(model.pointNXProperty());
        O_N.endYProperty().bind(model.pointNYProperty());
        O_M.startXProperty().bind(model.pointOXProperty());
        O_M.startYProperty().bind(model.pointOYProperty());
        O_M.endXProperty().bind(model.pointMXProperty());
        O_M.endYProperty().bind(model.pointMYProperty());
        M_W.startXProperty().bind(model.pointMXProperty());
        M_W.startYProperty().bind(model.pointMYProperty());
        M_W.endXProperty().bind(model.pointWXProperty());
        M_W.endYProperty().bind(model.pointWYProperty());
        N_W.startXProperty().bind(model.pointNXProperty());
        N_W.startYProperty().bind(model.pointNYProperty());
        N_W.endXProperty().bind(model.pointWXProperty());
        N_W.endYProperty().bind(model.pointWYProperty());
    }

    private void bindMedianAxes() {
        I_K.startXProperty().bind(model.pointIXProperty());
        I_K.startYProperty().bind(model.pointIYProperty());
        I_K.endXProperty().bind(model.pointKXProperty());
        I_K.endYProperty().bind(model.pointKYProperty());
        L_J.startXProperty().bind(model.pointLXProperty());
        L_J.startYProperty().bind(model.pointLYProperty());
        L_J.endXProperty().bind(model.pointJXProperty());
        L_J.endYProperty().bind(model.pointJYProperty());
        L_I.startXProperty().bind(model.pointLXProperty());
        L_I.startYProperty().bind(model.pointLYProperty());
        L_I.endXProperty().bind(model.pointIXProperty());
        L_I.endYProperty().bind(model.pointIYProperty());
        I_J.startXProperty().bind(model.pointIXProperty());
        I_J.startYProperty().bind(model.pointIYProperty());
        I_J.endXProperty().bind(model.pointJXProperty());
        I_J.endYProperty().bind(model.pointJYProperty());
        J_K.startXProperty().bind(model.pointJXProperty());
        J_K.startYProperty().bind(model.pointJYProperty());
        J_K.endXProperty().bind(model.pointKXProperty());
        J_K.endYProperty().bind(model.pointKYProperty());
        K_L.startXProperty().bind(model.pointKXProperty());
        K_L.startYProperty().bind(model.pointKYProperty());
        K_L.endXProperty().bind(model.pointLXProperty());
        K_L.endYProperty().bind(model.pointLYProperty());
    }

    private void bindOrthogonalAxes() {
        VLINE.startXProperty().bind(model.pointCXProperty());
        VLINE.startYProperty().bind(model.pointCYProperty());
        VLINE.endXProperty().bind(model.pointCXProperty());
        VLINE.endYProperty().bind(model.pointCYProperty().add(model.lengthOVProperty().divide(5)));
        HLINE.startXProperty().bind(model.pointCXProperty());
        HLINE.startYProperty().bind(model.pointCYProperty());
        HLINE.endXProperty().bind(model.pointCXProperty().add(model.lengthOUProperty().divide(5)));
        HLINE.endYProperty().bind(model.pointCYProperty());
    }

  
    private void setStyle(Group group, String style) {
        group.getChildren().stream().forEach(child -> {
            child.styleProperty().set(style);
        });
    }

}
