/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.structure.cell;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.stage.Popup;
import wallpaperfx.design.core.MathFX;

/**
 *
 * @author Maher Elkhaldi
 */
public class CellModel {

    private ReadOnlyDoubleWrapper cellArea = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper cosOU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper cosOv = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper deltaXOU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper deltaXOV = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper deltaYOU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper deltaYOV = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper height = new ReadOnlyDoubleWrapper(0);
    private final int indexI, indexJ;
    private ReadOnlyDoubleWrapper jlRadians = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper lengthOU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper lengthOV = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper lengthOW = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointCx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointCy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointIx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointIy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointJx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointJy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointKx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointKy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointLx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointLy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointMx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointMy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointNx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointNy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointOx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointOy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointUx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointUy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointVx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointVy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointWx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointWy = new ReadOnlyDoubleWrapper(0);
    
    private ReadOnlyDoubleWrapper pointCRx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointCRy = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointCLx = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper pointCLy = new ReadOnlyDoubleWrapper(0);
    
    
    private ReadOnlyDoubleWrapper radiansIK = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansOM = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansON = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansOU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansOV = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansOW = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansVOU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansVU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansWM = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper radiansWN = new ReadOnlyDoubleWrapper(0);

    private ReadOnlyIntegerWrapper releaseProperty = new ReadOnlyIntegerWrapper(0);
    private ReadOnlyDoubleWrapper sinOU = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyDoubleWrapper sinOV = new ReadOnlyDoubleWrapper(0);

    private ReadOnlyDoubleWrapper width = new ReadOnlyDoubleWrapper(0);

    private ChangeListener<Number> cellModelChangeListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
        releaseProperty.set(releaseProperty.get() + 1);
    };
    public Popup popup = new Popup();
    public double offsetX, offsetY;
    public Parent popupParent;
    ChangeListener<Number> cXListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
        setAnchorXPosition();
    };

    ChangeListener<Number> cYListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
        setAnchorYPosition();
    };
    ChangeListener<Boolean> showListener = (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
        if (newValue) {
            setAnchorXYPosition();
        }
    };
    private CellModel referenceCell;
    private ChangeListener<Number> referenceCellLisneter;

    public CellModel(CellModel referenceCell, int i, int j) {

        this.indexI = i;
        this.indexJ = j;
        this.referenceCellLisneter = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            pointOx.set(referenceCell.pointOXProperty().add(referenceCell.lengthOUProperty().multiply(indexI).multiply(MathFX.cos(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(indexJ).multiply(MathFX.cos(referenceCell.radiansOVProperty()))).get());
            pointOy.set(referenceCell.pointOYProperty().add(referenceCell.lengthOUProperty().multiply(indexI).multiply(MathFX.sin(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(indexJ).multiply(MathFX.sin(referenceCell.radiansOVProperty()))).get());
            pointUx.set(referenceCell.pointUXProperty().add(referenceCell.lengthOUProperty().multiply(indexI).multiply(MathFX.cos(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(indexJ).multiply(MathFX.cos(referenceCell.radiansOVProperty()))).get());
            pointUy.set(referenceCell.pointUYProperty().add(referenceCell.lengthOUProperty().multiply(indexI).multiply(MathFX.sin(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(indexJ).multiply(MathFX.sin(referenceCell.radiansOVProperty()))).get());
            pointVx.set(referenceCell.pointVXProperty().add(referenceCell.lengthOUProperty().multiply(indexI).multiply(MathFX.cos(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(indexJ).multiply(MathFX.cos(referenceCell.radiansOVProperty()))).get());
            pointVy.set(referenceCell.pointVYProperty().add(referenceCell.lengthOUProperty().multiply(i).multiply(MathFX.sin(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(j).multiply(MathFX.sin(referenceCell.radiansOVProperty()))).get());
        };
        this.referenceCell = referenceCell;
        this.referenceCell.releaseProperty.addListener(referenceCellLisneter);
        pointOx.set(referenceCell.pointOXProperty().add(referenceCell.lengthOUProperty().multiply(i).multiply(MathFX.cos(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(j).multiply(MathFX.cos(referenceCell.radiansOVProperty()))).get());
        pointOy.set(referenceCell.pointOYProperty().add(referenceCell.lengthOUProperty().multiply(i).multiply(MathFX.sin(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(j).multiply(MathFX.sin(referenceCell.radiansOVProperty()))).get());
        pointUx.set(referenceCell.pointUXProperty().add(referenceCell.lengthOUProperty().multiply(i).multiply(MathFX.cos(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(j).multiply(MathFX.cos(referenceCell.radiansOVProperty()))).get());
        pointUy.set(referenceCell.pointUYProperty().add(referenceCell.lengthOUProperty().multiply(i).multiply(MathFX.sin(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(j).multiply(MathFX.sin(referenceCell.radiansOVProperty()))).get());
        pointVx.set(referenceCell.pointVXProperty().add(referenceCell.lengthOUProperty().multiply(i).multiply(MathFX.cos(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(j).multiply(MathFX.cos(referenceCell.radiansOVProperty()))).get());
        pointVy.set(referenceCell.pointVYProperty().add(referenceCell.lengthOUProperty().multiply(i).multiply(MathFX.sin(referenceCell.radiansOUProperty()))).add(referenceCell.lengthOVProperty().multiply(j).multiply(MathFX.sin(referenceCell.radiansOVProperty()))).get());
        bind();
    }

    public CellModel(Point2D origin, double uLength, double uRadians, double vLength, double vRadians, int i, int j) {

        this.indexI = i;
        this.indexJ = j;
        this.pointOx.set(origin.getX());
        this.pointOy.set(origin.getY());
        this.pointUx.set(pointOXProperty().get() + uLength * Math.cos(uRadians));
        this.pointUy.set(pointOYProperty().get() + uLength * Math.sin(uRadians));
        this.pointVx.set(pointOXProperty().get() + vLength * Math.cos(vRadians));
        this.pointVy.set(pointOYProperty().get() + vLength * Math.sin(vRadians));
        bind();
    }

    public CellModel(Point2D origin, Point2D uPoint, Point2D vPoint, int i, int j) {

        this.indexI = i;
        this.indexJ = j;

        this.pointOx.set(origin.getX());
        this.pointOy.set(origin.getY());

        this.pointUx.set(uPoint.getX());
        this.pointUy.set(uPoint.getY());

        this.pointVx.set(vPoint.getX());
        this.pointVy.set(vPoint.getY());

        bind();
    }

    public final ReadOnlyDoubleProperty cellAreaProperty() {
        return cellArea.getReadOnlyProperty();
    }

    /**
     * @return the <code>Cosin of U</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty cosOUProperty() {
        return cosOU.getReadOnlyProperty();
    }

    /**
     * @return the <code>Cosin of V</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty cosOVProperty() {
        return cosOv.getReadOnlyProperty();
    }

    /**
     * @return the <code>UdeltaX</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty deltaXOUProperty() {
        return deltaXOU.getReadOnlyProperty();
    }

    /**
     * @return the <code>VdeltaX</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty deltaXOVProperty() {
        return deltaXOV.getReadOnlyProperty();
    }

    /**
     * @return the <code>UdeltaY</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty deltaYOUProperty() {
        return deltaYOU.getReadOnlyProperty();
    }

    /**
     * @return the <code>VdeltaY</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty deltaYOVProperty() {
        return deltaYOV.getReadOnlyProperty();
    }

    /**
     *
     * @return centroid Y
     */
    public final double getPointCY() {
        return pointCy.get();
    }

    public final double getCellArea() {
        return cellArea.get();
    }

    public final double getCosOU() {
        return cosOU.get();
    }

    public final double getCosOV() {
        return cosOv.get();
    }

    /**
     * @return the <code>UdeltaX</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getDeltaXOU() {
        return deltaXOU.get();
    }

    /**
     * @return the <code>VdeltaX</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getDeltaXOV() {
        return deltaXOV.get();
    }

    /**
     * @return the <code>UdeltaY</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getDeltaYOU() {
        return deltaYOU.get();
    }

    /**
     * @return the <code>VdeltaY</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getDeltaYOV() {
        return deltaYOV.get();
    }

    /**
     * @return the <code>originX</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getHeight() {
        return height.get();
    }

    public final int getIndexI() {
        return indexI;
    }

    public final int getIndexJ() {
        return indexJ;
    }

    /**
     * @return the <code>uCellDistance</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getLengthOU() {
        return lengthOU.get();
    }

    /**
     * @return the <code>vCellDistance</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getLengthOV() {
        return lengthOV.get();
    }

    /**
     * @return the <code>wCellDistance</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getLengthOW() {
        return lengthOW.get();
    }

    /**
     *
     * @return centroid X
     */
    public final double getPointCX() {
        return pointCx.get();
    }

    public final ReadOnlyDoubleWrapper getPointIX() {
        return pointIx;
    }

    public final ReadOnlyDoubleWrapper getPointIY() {
        return pointIy;
    }

    public final ReadOnlyDoubleWrapper getPointJX() {
        return pointJx;
    }

    public final ReadOnlyDoubleWrapper getPointJY() {
        return pointJy;
    }

    public final ReadOnlyDoubleWrapper getPointKX() {
        return pointKx;
    }

    public final ReadOnlyDoubleWrapper getPointKY() {
        return pointKy;
    }

    public final ReadOnlyDoubleWrapper getPointLX() {
        return pointLx;
    }

    public final ReadOnlyDoubleWrapper getPointLY() {
        return pointLy;
    }

    /**
     *
     * @return uvDiagPointOneThirdX
     */
    public final double getPointMX() {
        return pointMx.get();

    }

    /**
     *
     * @return uvDiagPointOneThirdY
     */
    public final double getPointMY() {
        return pointMy.get();

    }

    /**
     *
     * @return uvDiagPointTwoThirdsX
     */
    public final double getPointNX() {
        return pointNx.get();

    }

    /**
     *
     * @return uvDiagPointTwoThirdsY
     */
    public final double getPointNY() {
        return pointNy.get();

    }

    /**
     * @return the <code>originX</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getPointOX() {
        return pointOx.get();
    }

    /**
     * @return the <code>originY</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getPointOY() {
        return pointOy.get();
    }

    /**
     * @return the <code>uPointX</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getPointUX() {
        return pointUx.get();
    }

    /**
     * @return the <code>uPointY</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getPointUY() {
        return pointUy.get();
    }

    /**
     * @return the <code>vPointX</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getPointVX() {
        return pointVx.get();
    }

    /**
     * @return the <code>vPointY</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getPointVY() {
        return pointVy.get();
    }

    /**
     *
     * @return UVpoint X
     */
    public final double getPointWX() {
        return pointWx.get();
    }

    /**
     *
     * @return UVpoint Y
     */
    public final double getPointWY() {
        return pointWy.get();
    }

    public final double getRadiansIK() {
        return radiansIK.get();
    }

    public final double getRadiansJL() {
        return jlRadians.get();
    }

    public final double getRadiansOM() {
        return radiansOM.get();
    }

    public final double getRadiansON() {
        return radiansON.get();
    }

    /**
     * @return the <code>uRadians</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getRadiansOU() {
        return radiansOU.get();
    }

    /**
     * @return the <code>vRadians</code> {@link ReadOnlyDoubleProperty}
     */
    public final double getRadiansOV() {
        return radiansOV.get();
    }

    public final double getRadiansOW() {
        return radiansOW.get();
    }

    public final double getRadiansVOU() {
        return radiansVOU.get();
    }

    public final double getRadiansVU() {
        return radiansVU.get();
    }

    public final double getRadiansWM() {
        return radiansWM.get();
    }

    public final double getRadiansWN() {
        return radiansWN.get();
    }

    /**
     * @return the current release as {@link ReadOnlyIntegerProperty}
     */
    public final int getRelease() {
        return releaseProperty.get();
    }

    public final double getSinOU() {
        return sinOU.get();
    }

    public final double getSinOV() {
        return sinOV.get();
    }

    public final double getWidth() {
        return width.get();
    }

    /**
     * @return the <code>originX</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty heightProperty() {
        return height.getReadOnlyProperty();
    }

    /**
     * @return the <code>uCellDistance</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty lengthOUProperty() {
        return lengthOU.getReadOnlyProperty();
    }

    /**
     * @return the <code>vCellDistance</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty lengthOVProperty() {
        return lengthOV.getReadOnlyProperty();
    }

    /**
     * @return the <code>wCellDistance</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty lengthOWProperty() {
        return lengthOW.getReadOnlyProperty();
    }

    /**
     *
     * @return centroid X
     */
    public final ReadOnlyDoubleProperty pointCXProperty() {
        return pointCx.getReadOnlyProperty();
    }

    /**
     *
     * @return centroid Y
     */
    public final ReadOnlyDoubleProperty pointCYProperty() {
        return pointCy.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointIXProperty() {
        return pointIx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointIYProperty() {
        return pointIy.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointJXProperty() {
        return pointJx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointJYProperty() {
        return pointJy.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointKXProperty() {
        return pointKx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointKYProperty() {
        return pointKy.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointLXProperty() {
        return pointLx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointLYProperty() {
        return pointLy.getReadOnlyProperty();
    }

    /**
     *
     * @return uvDiagPointOneThirdX
     */
    public final ReadOnlyDoubleProperty pointMXProperty() {
        return pointMx.getReadOnlyProperty();

    }

    /**
     *
     * @return uvDiagPointOneThirdY
     */
    public final ReadOnlyDoubleProperty pointMYProperty() {
        return pointMy.getReadOnlyProperty();

    }

    /**
     *
     * @return uvDiagPointTwoThirdsX
     */
    public final ReadOnlyDoubleProperty pointNXProperty() {
        return pointNx.getReadOnlyProperty();

    }

    /**
     *
     * @return uvDiagPointTwoThirdsY
     */
    public final ReadOnlyDoubleProperty pointNYProperty() {
        return pointNy.getReadOnlyProperty();

    }

    /**
     * @return the <code>originX</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty pointOXProperty() {
        return pointOx.getReadOnlyProperty();
    }

    /**
     * @return the <code>originY</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty pointOYProperty() {
        return pointOy.getReadOnlyProperty();
    }

    /**
     * @return the <code>uPointX</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty pointUXProperty() {
        return pointUx;
    }

    /**
     * @return the <code>uPointY</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty pointUYProperty() {
        return pointUy.getReadOnlyProperty();
    }

    /**
     * @return the <code>vPointX</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty pointVXProperty() {
        return pointVx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointVYProperty() {
        return pointVy.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointWXProperty() {
        return pointWx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointWYProperty() {
        return pointWy.getReadOnlyProperty();
    }
    
    public final ReadOnlyDoubleProperty pointCRXProperty() {
        return pointCRx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointCRYProperty() {
        return pointCRy.getReadOnlyProperty();
    }
    
    public final ReadOnlyDoubleProperty pointCLXProperty() {
        return pointCLx.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty pointCLYProperty() {
        return pointCLy.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansIKProperty() {
        return radiansIK.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansJLProperty() {
        return jlRadians.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansOMProperty() {
        return radiansOM.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansONProperty() {
        return radiansON.getReadOnlyProperty();
    }

    /**
     * @return the <code>uRadians</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty radiansOUProperty() {
        return radiansOU.getReadOnlyProperty();
    }

    /**
     * @return the <code>vRadians</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty radiansOVProperty() {
        return radiansOV.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansOWProperty() {
        return radiansOW.getReadOnlyProperty();
    }

    /**
     * @return
     */
    public final ReadOnlyDoubleProperty radiansVOUProperty() {
        return radiansVOU.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansVUProperty() {
        return radiansVU.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansWMProperty() {
        return radiansWM.getReadOnlyProperty();
    }

    public final ReadOnlyDoubleProperty radiansWNProperty() {
        return radiansWN.getReadOnlyProperty();
    }

    /**
     * @return the current release as {@link ReadOnlyIntegerProperty}
     */
    public final ReadOnlyIntegerProperty releaseProperty() {
        return releaseProperty.getReadOnlyProperty();
    }

    /**
     * * Set the <code>uDistance</code> of the cell
     *
     * @param distance coordinate value
     */
    public final void setLengthOU(double distance) {
        if (referenceCellLisneter == null) {
            double uAngle = radiansOUProperty().get();
            setPointUX(pointOXProperty().get() + distance * Math.cos(uAngle));
            setPointUY(pointOYProperty().get() + distance * Math.sin(uAngle));
        }
        else{
            System.err.println("Not possible to LengthOU. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    /**
     * * Set the <code>vDistance</code> of the cell
     *
     * @param distance coordinate value
     */
    public final void setLengthOV(double distance) {
        if (referenceCellLisneter == null) {
            double vAngle = radiansOVProperty().get();
            setPointVX(pointOXProperty().get() + distance * Math.cos(vAngle));
            setPointVY(pointOYProperty().get() + distance * Math.sin(vAngle));
        }
        else{
            System.err.println("Not possible to LengthOV. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }

    }

    public final void setLengthOW(double distance) {
        if (referenceCellLisneter == null) {
            double distanceU = distance * Math.cos(Math.abs(radiansOWProperty().get() - radiansOUProperty().get()));
            double distanceV = distance * Math.sin(Math.abs(radiansOWProperty().get() - radiansOVProperty().get()));
            setLengthOU(distanceU);
            setLengthOV(distanceV);
        }else{
            System.err.println("Not possible to LengthOW. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setPointOX(double x) {
        if (referenceCellLisneter == null) {
            pointOx.set(x);
        }
        else{
            System.err.println("Not possible to PointOX. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setPointOY(double y) {
        if (referenceCellLisneter == null) {
            pointOy.set(y);
        }
        else{
            System.err.println("Not possible to PointOY. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }

    }

    public final void setPointUX(double x) {
        if (referenceCellLisneter == null) {
            pointUx.set(x);
        }
        else{
            System.err.println("Not possible to PointUX. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setPointUY(double y) {
        if (referenceCellLisneter == null) {
            pointUy.set(y);
        }
        else{
            System.err.println("Not possible to PointUY. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setPointVX(double x) {
        if (referenceCellLisneter == null) {
            pointVx.set(x);
        }
        else{
            System.err.println("Not possible to PointVX. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setPointVY(double y) {
        if (referenceCellLisneter == null) {
            pointVy.set(y);
        }
        else{
            System.err.println("Not possible to PointVY. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setRadiansOU(double radians) {
        if (referenceCellLisneter == null) {
            double x, y;
            double angleRad = radians - getRadiansOU();

            x = Math.cos(angleRad) * (getPointUX() - getPointOX()) - Math.sin(angleRad) * (getPointUY() - getPointOY()) + getPointOX();
            y = Math.sin(angleRad) * (getPointUX() - getPointOX()) + Math.cos(angleRad) * (getPointUY() - getPointOY()) + getPointOY();

            setPointUX(x);
            setPointUY(y);
        }
        else{
            System.err.println("Not possible to RadiansOU. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setRadiansOV(double radians) {
        if (referenceCellLisneter == null) {
            double x, y;
            double angleRad = radians - getRadiansOV();
            x = Math.cos(angleRad) * (getPointVX() - getPointOX()) - Math.sin(angleRad) * (getPointVY() - getPointOY()) + getPointOX();
            y = Math.sin(angleRad) * (getPointVX() - getPointOX()) + Math.cos(angleRad) * (getPointVY() - getPointOY()) + getPointOY();
            setPointVX(x);
            setPointVY(y);
        }
        else{
            System.err.println("Not possible to RadiansOV. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    public final void setRadiansOW(double radians) {
        if (referenceCellLisneter == null) {
            double angleRad = radians;

            double oldAngleU = radiansOUProperty().get() - radiansOWProperty().get();
            double oldAngleV = radiansOVProperty().get() - radiansOWProperty().get();

            setRadiansOU(angleRad + oldAngleU);
            setRadiansOV(angleRad + oldAngleV);
        }
        else{
            System.err.println("Not possible to RadiansOW. Cell: " + indexI + "," + indexJ + " is managed by a reference cell");
        }
    }

    /**
     * @return the <code>Sine of U</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty sinOUProperty() {
        return sinOU.getReadOnlyProperty();
    }

    /**
     * @return the <code>Sine of V</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty sinOVProperty() {
        return sinOV.getReadOnlyProperty();
    }

    /**
     * @return the <code>originX</code> {@link ReadOnlyDoubleProperty}
     */
    public final ReadOnlyDoubleProperty widthProperty() {
        return width.getReadOnlyProperty();
    }

    public final void dispose() {
        this.popup.hide();
        this.popup.getContent().clear();
        this.popup.showingProperty().removeListener(showListener);
        this.pointCx.removeListener(cXListener);
        this.pointCy.removeListener(cYListener);
        this.cellArea.unbind();
        this.cosOU.unbind();
        this.cosOv.unbind();
        this.deltaXOU.unbind();
        this.deltaXOV.unbind();
        this.deltaYOU.unbind();
        this.deltaYOV.unbind();
        this.height.unbind();
        this.jlRadians.unbind();
        this.lengthOU.unbind();
        this.lengthOV.unbind();
        this.lengthOW.unbind();
        this.pointCx.unbind();
        this.pointCy.unbind();
        this.pointIx.unbind();
        this.pointIy.unbind();
        this.pointJx.unbind();
        this.pointJy.unbind();
        this.pointKx.unbind();
        this.pointKy.unbind();
        this.pointLx.unbind();
        this.pointLy.unbind();
        this.pointMx.unbind();
        this.pointMy.unbind();
        this.pointNx.unbind();
        this.pointNy.unbind();
        this.pointOx.unbind();
        this.pointOy.unbind();
        this.pointUx.unbind();
        this.pointUy.unbind();
        this.pointVx.unbind();
        this.pointVy.unbind();
        this.pointWx.unbind();
        this.pointWy.unbind();
        this.radiansIK.unbind();
        this.radiansOM.unbind();
        this.radiansON.unbind();
        this.radiansOU.unbind();
        this.radiansOV.unbind();
        this.radiansOW.unbind();
        this.radiansVOU.unbind();
        this.radiansVU.unbind();
        this.radiansWM.unbind();
        this.radiansWN.unbind();
        this.releaseProperty.unbind();
        this.sinOU.unbind();
        this.sinOV.unbind();
        this.width.unbind();

        this.pointWx.removeListener(cellModelChangeListener);
        this.pointWy.removeListener(cellModelChangeListener);
        this.pointOx.removeListener(cellModelChangeListener);
        this.pointOy.removeListener(cellModelChangeListener);
        this.referenceCell.releaseProperty.removeListener(referenceCellLisneter);

        this.pointCRx.unbind();
        this.pointCRy.unbind();
        this.pointCLx.unbind();
        this.pointCLy.unbind();
        
        nullify();
    }

    public void nullify() {
        this.popup = null;
        this.popupParent = null;
        this.cellArea = null;
        this.cosOU = null;
        this.cosOv = null;
        this.deltaXOU = null;
        this.deltaXOV = null;
        this.deltaYOU = null;
        this.deltaYOV = null;
        this.height = null;
        this.jlRadians = null;
        this.lengthOU = null;
        this.lengthOV = null;
        this.lengthOW = null;
        this.pointCx = null;
        this.pointCy = null;
        this.pointIx = null;
        this.pointIy = null;
        this.pointJx = null;
        this.pointJy = null;
        this.pointKx = null;
        this.pointKy = null;
        this.pointLx = null;
        this.pointLy = null;
        this.pointMx = null;
        this.pointMy = null;
        this.pointNx = null;
        this.pointNy = null;
        this.pointOx = null;
        this.pointOy = null;
        this.pointUx = null;
        this.pointUy = null;
        this.pointVx = null;
        this.pointVy = null;
        this.pointWx = null;
        this.pointWy = null;
        this.radiansIK = null;
        this.radiansOM = null;
        this.radiansON = null;
        this.radiansOU = null;
        this.radiansOV = null;
        this.radiansOW = null;
        this.radiansVOU = null;
        this.radiansVU = null;
        this.radiansWM = null;
        this.radiansWN = null;
        this.releaseProperty = null;
        this.sinOU = null;
        this.sinOV = null;
        this.width = null;
        this.cellModelChangeListener = null;
        this.referenceCellLisneter = null;
        this.showListener = null;
        this.cXListener = null;
        this.cYListener = null;
        this.pointCRx=null;
        this.pointCRy=null;
        this.pointCLx=null;
        this.pointCLy=null;
        
    }

    private void setArea() {
        double diffUX, diffUY, diffVX, diffVY;
        diffUX = getPointOX() - getPointUX();
        diffUY = getPointOY() - getPointUY();
        diffVX = getPointOX() - getPointVX();
        diffVY = getPointOY() - getPointVY();
        this.cellArea.set(Math.round(Math.abs(diffUX * diffVY - diffUY * diffVX)));
    }

    private void bind() {
        this.deltaXOU.bind(this.pointUx.subtract(this.pointOx));
        this.deltaYOU.bind(this.pointUy.subtract(this.pointOy));
        this.deltaXOV.bind(this.pointVx.subtract(this.pointOx));
        this.deltaYOV.bind(this.pointVy.subtract(this.pointOy));
        this.width.bind(this.pointUx.subtract(pointVx));
        this.height.bind(this.pointVy.subtract(pointUy));
        this.pointCx.bind((this.pointUx.add(this.pointVx)).divide(2));
        this.pointCy.bind((this.pointUy.add(this.pointVy)).divide(2));
        this.pointWx.bind(this.pointCx.multiply(2).subtract(pointOx));
        this.pointWy.bind(this.pointCy.multiply(2).subtract(pointOy));

        
        this.pointCRx.bind(this.pointJx.add(((this.pointLx).subtract(this.pointJx)).divide(4)));
        this.pointCRy.bind(this.pointJy.add(((this.pointLy).subtract(this.pointJy)).divide(4)));
        this.pointCLx.bind(this.pointJx.add(((this.pointLx).subtract(this.pointJx).multiply(2)).divide(4)));
        this.pointCLy.bind(this.pointJy.add(((this.pointLy).subtract(this.pointJy).multiply(2)).divide(4)));

        this.lengthOW.bind(MathFX.hypot(pointWx.subtract(pointOx), pointWy.subtract(pointOy)));
        this.jlRadians.bind(radiansOU.add(Math.PI / 2));
        this.radiansIK.bind(radiansOV.add(Math.PI / 2));

        this.releaseProperty.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            setArea();
        });

        this.cosOU.bind(MathFX.cos(this.radiansOU));
        this.sinOU.bind(MathFX.sin(this.radiansOU));
        this.cosOv.bind(MathFX.cos(this.radiansOV));
        this.sinOV.bind(MathFX.sin(this.radiansOV));

        this.pointMx.bind(this.pointVx.add(((this.pointUx).subtract(this.pointVx)).divide(3)));
        this.pointMy.bind(this.pointVy.add(((this.pointUy).subtract(this.pointVy)).divide(3)));
        this.pointNx.bind(this.pointVx.add(((this.pointUx).subtract(this.pointVx).multiply(2)).divide(3)));
        this.pointNy.bind(this.pointVy.add(((this.pointUy).subtract(this.pointVy).multiply(2)).divide(3)));

        this.pointIx.bind(this.pointOx.add(((this.pointUx).subtract(this.pointOx)).divide(2)));
        this.pointIy.bind(this.pointOy.add(((this.pointUy).subtract(this.pointOy)).divide(2)));
        this.pointLx.bind(this.pointOx.add(((this.pointVx).subtract(this.pointOx)).divide(2)));
        this.pointLy.bind(this.pointOy.add(((this.pointVy).subtract(this.pointOy)).divide(2)));
        this.pointKx.bind(this.pointVx.add(((this.pointWx).subtract(this.pointVx)).divide(2)));
        this.pointKy.bind(this.pointVy.add(((this.pointWy).subtract(this.pointVy)).divide(2)));
        this.pointJx.bind(this.pointUx.add(((this.pointWx).subtract(this.pointUx)).divide(2)));
        this.pointJy.bind(this.pointUy.add(((this.pointWy).subtract(this.pointUy)).divide(2)));

        this.radiansVOU.bind(this.radiansOV.subtract(this.radiansOU));

        this.radiansOW.bind(MathFX.atan2(pointWy.subtract(pointOy), pointWx.subtract(pointOx)));

        this.radiansVU.bind(MathFX.atan2(pointVy.subtract(pointUy), pointVx.subtract(pointUx)));
        this.radiansWN.bind(MathFX.atan2(pointNy.subtract(pointWy), pointNx.subtract(pointWx)));
        this.radiansWM.bind(MathFX.atan2(pointMy.subtract(pointWy), pointMx.subtract(pointWx)));
        this.radiansOM.bind(MathFX.atan2(pointMy.subtract(pointOy), pointMx.subtract(pointOx)));
        this.radiansON.bind(MathFX.atan2(pointNy.subtract(pointOy), pointNx.subtract(pointOx)));

        this.lengthOV.bind(MathFX.hypot(deltaXOVProperty(), deltaYOVProperty()));
        this.lengthOU.bind(MathFX.hypot(deltaXOUProperty(), deltaYOUProperty()));
        this.radiansOU.bind(MathFX.atan2(deltaYOUProperty(), deltaXOUProperty()));
        this.radiansOV.bind(MathFX.atan2(deltaYOVProperty(), deltaXOVProperty()));

        this.pointWx.addListener(cellModelChangeListener);
        this.pointWy.addListener(cellModelChangeListener);
        this.pointOx.addListener(cellModelChangeListener);
        this.pointOy.addListener(cellModelChangeListener);
    }

    public final void setPopup(Node content, double offsetX, double offsetY, Parent popupParent, boolean showAsWell) {
        this.popup.getContent().add(content);
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.popupParent = popupParent;
        this.pointCx.addListener(cXListener);
        this.pointCy.addListener(cYListener);
        this.popup.showingProperty().addListener(showListener);
        if (showAsWell) {
            popup.show(popupParent.getParent().getScene().getWindow());
        }
    }

    public void showPopup() {
        if (popupParent != null) {
            popup.show(popupParent.getParent().getScene().getWindow());
        }
    }

    public void hidePopup() {
        popup.hide();
    }

    private void setAnchorXYPosition() {
        setAnchorXPosition();
        setAnchorYPosition();
    }

    private void setAnchorXPosition() {
        popup.setAnchorX(pointCx.get() + offsetX + popupParent.getLayoutX() + popupParent.getScene().getWindow().getX());
    }

    private void setAnchorYPosition() {
        popup.setAnchorY(pointCy.get() + offsetY + popupParent.getLayoutY() + popupParent.getScene().getWindow().getY());
    }
}
