/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.structure.lattice;

import javafx.geometry.Point2D;
import wallpaperfx.design.core.Enums.CellType;

/**
 *
 * @author Maher Elkhaldi
 */
public class Lattice {

    public final LatticeModel model;
    public final LatticeController controller;
    public final LatticeView view;

    public final Point2D origin;
    public final Point2D uPoint;
    public final Point2D vPoint;

    public Lattice(Point2D origin, Point2D uPoint, Point2D vPoint, int countU, int countV) {
        this.origin = origin;
        this.uPoint = uPoint;
        this.vPoint = vPoint;
        model = new LatticeModel(origin, uPoint, vPoint, countU, countV);
        view = new LatticeView(model);
        controller = new LatticeController(model, view);
    }
    
    public Lattice(Point2D origin, CellType cellType, double uLen, double vLen, int countU, int countV) {
        this.origin = origin;

        double uRadians = 0, vRadians = 0;
        switch (cellType) {
            case HEXAGONAL:
                vLen = uLen;
                uRadians = 0;
                vRadians = Math.PI * 2 / 3;
                break;
            case OBLIQUE:
                vLen = 1.5 * uLen;
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case RECTANGULAR:
                vLen = 2 * uLen;
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
            case RHOMBIC:
                vLen = uLen;
                uRadians = Math.PI / 2;
                vRadians = -Math.PI / 2;
                break;
            case SQUARE:
                vLen = uLen;
                uRadians = 0;
                vRadians = Math.PI / 2;
                break;
        }

        this.uPoint = new Point2D(this.origin.getX() + uLen * Math.cos(uRadians), this.origin.getY() + uLen * Math.sin(uRadians));
        this.vPoint = new Point2D(this.origin.getX() + vLen * Math.cos(vRadians), this.origin.getY() + vLen * Math.sin(vRadians));

        model = new LatticeModel(origin, uPoint, vPoint, countU, countV);
        view = new LatticeView(model);
        controller = new LatticeController(model, view);
    }

}
