/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.structure.lattice;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.shape.Line;
import static wallpaperfx.design.control.HandleEnums.HandleShape.LINE;
import wallpaperfx.design.structure.cell.CellView;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;
import static wallpaperfx.design.control.DesignControlHandleFactory.build;

/**
 *
 * @author Maher Elkhaldi
 */
public class LatticeView extends Group {

    public Group cellViews = new Group();
    private Line VW, UW, OV, OU;
    public SimpleBooleanProperty boundaryLoaded = new SimpleBooleanProperty(true);
    public SimpleBooleanProperty internalAxesLoaded = new SimpleBooleanProperty(true);
    public SimpleBooleanProperty medianAxesLoaded = new SimpleBooleanProperty(true);
    public SimpleBooleanProperty orthoAxesLoaded = new SimpleBooleanProperty(true);
    public SimpleBooleanProperty diagonalAxesLoaded = new SimpleBooleanProperty(true);
    public SimpleBooleanProperty borderLoaded = new SimpleBooleanProperty(false);

    public final static String PASSIVE = "-fx-alignment: center; -fx-fill: gray ;-fx-stroke: gray; -fx-stroke-width: 1;-fx-stroke-dash-array: 3 3.0";

    Group border = new Group();

    protected LatticeView(LatticeModel model) {
        VW = (Line) build(LINE, 2, PASSIVE);
        UW = (Line) build(LINE, 2, PASSIVE);
        OV = (Line) build(LINE, 2, PASSIVE);
        OU = (Line) build(LINE, 2, PASSIVE);

        border.getChildren().addAll(VW, UW, OV, OU);
        super.getChildren().add(cellViews);
        borderLoaded.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                OV.startXProperty().bind(model.uMinvMinCellModelProperty().get().pointOXProperty());
                OV.startYProperty().bind(model.uMinvMinCellModelProperty().get().pointOYProperty());
                OV.endXProperty().bind(model.uMinvMaxCellModelProperty().get().pointVXProperty());
                OV.endYProperty().bind(model.uMinvMaxCellModelProperty().get().pointVYProperty());

                OU.startXProperty().bind(model.uMinvMinCellModelProperty().get().pointOXProperty());
                OU.startYProperty().bind(model.uMinvMinCellModelProperty().get().pointOYProperty());
                OU.endXProperty().bind(model.uMaxvMinCellModelProperty().get().pointUXProperty());
                OU.endYProperty().bind(model.uMaxvMinCellModelProperty().get().pointUYProperty());

                VW.startXProperty().bind(model.uMinvMaxCellModelProperty().get().pointVXProperty());
                VW.startYProperty().bind(model.uMinvMaxCellModelProperty().get().pointVYProperty());
                VW.endXProperty().bind(model.uMaxvMaxCellModelProperty().get().pointWXProperty());
                VW.endYProperty().bind(model.uMaxvMaxCellModelProperty().get().pointWYProperty());

                UW.startXProperty().bind(model.uMaxvMinCellModelProperty().get().pointUXProperty());
                UW.startYProperty().bind(model.uMaxvMinCellModelProperty().get().pointUYProperty());
                UW.endXProperty().bind(model.uMaxvMaxCellModelProperty().get().pointWXProperty());
                UW.endYProperty().bind(model.uMaxvMaxCellModelProperty().get().pointWYProperty());
                getChildren().add(border);
            } else {
                OV.startXProperty().unbind();
                OV.startYProperty().unbind();
                OV.endXProperty().unbind();
                OV.endYProperty().unbind();

                OU.startXProperty().unbind();
                OU.startYProperty().unbind();
                OU.endXProperty().unbind();
                OU.endYProperty().unbind();

                VW.startXProperty().unbind();
                VW.startYProperty().unbind();
                VW.endXProperty().unbind();
                VW.endYProperty().unbind();

                UW.startXProperty().unbind();
                UW.startYProperty().unbind();
                UW.endXProperty().unbind();
                UW.endYProperty().unbind();
                getChildren().remove(border);
            }
        });
        borderLoaded.set(true);
    }

    public void dispose() {
        setElementVisible(false);
        setSubElementsVisible(false);
        OV.startXProperty().unbind();
        OV.startYProperty().unbind();
        OV.endXProperty().unbind();
        OV.endYProperty().unbind();

        OU.startXProperty().unbind();
        OU.startYProperty().unbind();
        OU.endXProperty().unbind();
        OU.endYProperty().unbind();

        VW.startXProperty().unbind();
        VW.startYProperty().unbind();
        VW.endXProperty().unbind();
        VW.endYProperty().unbind();

        UW.startXProperty().unbind();
        UW.startYProperty().unbind();
        UW.endXProperty().unbind();
        UW.endYProperty().unbind();
        getChildren().remove(border);
        this.cellViews.getChildren().forEach(childGroup -> {
            Group subGroup = (Group) childGroup;
            subGroup.getChildren().forEach(child -> {
                CellView cellView = (CellView) child;
                cellView.dispose();
            });
            subGroup.getChildren().clear();
        });
        this.cellViews.getChildren().clear();
        nullify();

    }

    private void nullify() {
        this.cellViews = null;
        this.VW = null;
        this.UW = null;
        this.OV = null;
        this.OU = null;
        this.boundaryLoaded = null;
        this.internalAxesLoaded = null;
        this.medianAxesLoaded = null;
        this.orthoAxesLoaded = null;
        this.diagonalAxesLoaded = null;
        this.borderLoaded = null;
    }
    
    public final void setElementVisible(boolean val){
       borderLoaded.set(val);
    }
    
    public final void setSubElementsVisible(boolean val){
       boundaryLoaded.set(val);
       orthoAxesLoaded.set(val);
       internalAxesLoaded.set(val);
       medianAxesLoaded.set(val);
       diagonalAxesLoaded.set(val);
    }
}
