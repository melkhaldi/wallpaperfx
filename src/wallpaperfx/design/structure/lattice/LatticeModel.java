/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.design.structure.lattice;

import java.util.ArrayList;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import wallpaperfx.design.core.ElementGrid;
import wallpaperfx.design.structure.cell.CellModel;
import wallpaperfx.design.core.Enums.SymmetryModelType;

/**
 *
 * @author Maher Elkhaldi
 */
public final class LatticeModel extends ElementGrid {

    public ArrayList<ArrayList<CellModel>> cellModelLists = new ArrayList<>();

    public final ReadOnlyObjectWrapper<CellModel> uMinvMinCellModelProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<CellModel> uMaxvMaxCellModelProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<CellModel> uMaxvMinCellModelProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<CellModel> uMinvMaxCellModelProperty = new ReadOnlyObjectWrapper<>();
    private final ReadOnlyIntegerWrapper release = new ReadOnlyIntegerWrapper(0);

    private ReadOnlyIntegerWrapper uCount = new ReadOnlyIntegerWrapper(0);
    private ReadOnlyIntegerWrapper vCount = new ReadOnlyIntegerWrapper(0);
    public SimpleObjectProperty<SymmetryModelType> symmetryModelProperty = new SimpleObjectProperty();

    Point2D origin, uPoint, vPoint;

    public LatticeModel(Point2D origin, Point2D uPoint, Point2D vPoint, int uCount, int vCount) {
        this.origin = origin;
        this.uPoint = uPoint;
        this.vPoint = vPoint;
        this.uCount.set(uCount);
        this.vCount.set(vCount);
        this.uMaxvMaxCellModelProperty.addListener((ObservableValue<? extends CellModel> observable, CellModel oldValue, CellModel newValue) -> {
            this.release.set(release.get() + 1);
        });
        setUV(this.uCount.get(), this.vCount.get());

    }

    

    public int getUCount() {
        return uCount.intValue();
    }

    public int getVCount() {
        return vCount.intValue();
    }

    public ReadOnlyIntegerProperty uCountProperty() {
        return uCount.getReadOnlyProperty();
    }

    public ReadOnlyIntegerProperty vCountProperty() {
        return vCount.getReadOnlyProperty();
    }

    public final void setUCount(int u) {
        if (u > 0) {
            uCount.set(u);
            setU(uCount.get());
            setCorners();

        }
    }

    public final void setVCount(int v) {
        if (v > 0) {
            vCount.set(v);
            setV(vCount.get());
            setCorners();

        }
    }

    public int getRelease() {
        return release.get();
    }

    /**
     * Returns unmodifiable {@link ObservableList} containing
     * {@link ObservableList} of Affine objects.
     *
     * @return unmodifiable <code> ObservableList&lt;ObservableList&lt;Affine&gt;&gt;
     * </code>
     */
    public ObservableList<ObservableList<CellModel>> getUnmodifable() {

        ObservableList<ObservableList<CellModel>> main = FXCollections.observableArrayList();
        cellModelLists.stream().forEach((contentStream) -> {
            main.add(FXCollections.unmodifiableObservableList(FXCollections.observableArrayList(contentStream)));
        });
        return FXCollections.unmodifiableObservableList(main);
    }

    /**
     * @return the current release as {@link ReadOnlyIntegerProperty}
     */
    public ReadOnlyIntegerProperty releaseProperty() {
        return release;
    }

    public ReadOnlyObjectProperty<CellModel> uMinvMinCellModelProperty() {
        return uMinvMinCellModelProperty.getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty<CellModel> uMaxvMaxCellModelProperty() {
        return uMaxvMaxCellModelProperty.getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty<CellModel> uMaxvMinCellModelProperty() {
        return uMaxvMinCellModelProperty.getReadOnlyProperty();
    }

    public ReadOnlyObjectProperty<CellModel> uMinvMaxCellModelProperty() {
        return uMinvMaxCellModelProperty.getReadOnlyProperty();

    }

    @Override
    public String toString() {
        String val = "\n[" + super.toString() + "]" + "\n>>";
        for (int i = 0; i < cellModelLists.size(); i++) {
            for (int j = 0; j < cellModelLists.get(i).size(); j++) {
                CellModel cellModel = cellModelLists.get(i).get(j);
                val += "\n (" + (cellModel.toString() + ") located [X@" + cellModel.getPointOX() + ", Y@" + cellModel.getPointOX() + "] of Index<" + i + ", " + j + ">");
            }
        }
        return val + "\n<<";
    }

    @Override
    protected void addElement(int i, int j) {
        cellModelLists.get(i).add(new CellModel(cellModelLists.get(0).get(0), i, j));
    }

    @Override
    protected void setUV(int u, int v) {
        for (int i = 0; i < u; i++) {
            cellModelLists.add(new ArrayList());
            for (int j = 0; j < v; j++) {
                if (i == 0 && j == 0) {
                    cellModelLists.get(0).add(new CellModel(origin, uPoint, vPoint, 0, 0));
                } else {
                    addElement(i, j);
                }
            }
        }

        setCorners();

    }

    @Override
    public void dispose() {
        this.cellModelLists.forEach(list -> {
            list.stream().forEach(cellModel -> cellModel.dispose());
        });
        cellModelLists = null;
    }

    private void setCorners() {
        uMinvMinCellModelProperty.set(cellModelLists.get(0).get(0));
        uMinvMaxCellModelProperty.set(cellModelLists.get(0).get(vCount.get() - 1));
        uMaxvMinCellModelProperty.set(cellModelLists.get(cellModelLists.size() - 1).get(0));
        uMaxvMaxCellModelProperty.set(cellModelLists.get(uCount.get() - 1).get(vCount.get() - 1));
    }

    @Override
    protected void setU(int newCount) {
        if (newCount > cellModelLists.size()) {
            addU(newCount);
        } else {
            removeU(newCount);
        }
    }

    @Override
    protected void addU(int newCount) {
        for (int u = cellModelLists.size(); u < newCount; u++) {
            cellModelLists.add(new ArrayList());
            for (int v = 0; v < cellModelLists.get(0).size(); v++) {
                addElement(u, v);
            }
        }
    }

    @Override
    protected void removeU(int newCount) {
        while (cellModelLists.size() > newCount && cellModelLists.size() > 0) {

            cellModelLists.get(cellModelLists.size() - 1).stream().forEach((cm) -> {
                cm.dispose();
            });
            cellModelLists.get(cellModelLists.size() - 1).clear();
            cellModelLists.remove(cellModelLists.size() - 1);
        }
    }

    @Override
    protected void setV(int newCount) {
        if (newCount > cellModelLists.get(0).size()) {
            addV(newCount);
        } else {
            removeV(newCount);
        }
    }

    @Override
    protected void removeV(int newCount) {
        cellModelLists.stream().forEach(list -> {
            while (list.size() > newCount) {
                CellModel toRemove = list.get(list.size() - 1);
                toRemove.dispose();
                list.remove(toRemove);
            }
        });

    }

    @Override
    protected void addV(int newCount) {
        for (int u = 0; u < cellModelLists.size(); u++) {
            for (int v = cellModelLists.get(u).size(); v < newCount; v++) {
                addElement(u, v);
            }
        }
    }

    

}
