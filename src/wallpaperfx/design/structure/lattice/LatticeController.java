/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wallpaperfx.design.structure.lattice;

import java.util.ArrayList;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import wallpaperfx.design.core.ElementGrid;
import wallpaperfx.design.structure.cell.CellController;
import wallpaperfx.design.structure.cell.CellView;

/**
 *
 * @author Maher Elkhaldi
 */
public final class LatticeController extends ElementGrid {

    public final ReadOnlyObjectWrapper<CellController> uMinvMinTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<CellController> uMaxvMaxTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<CellController> uMaxvMinTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public final ReadOnlyObjectWrapper<CellController> uMinvMaxTileControllerProperty = new ReadOnlyObjectWrapper<>();
    public ArrayList<ArrayList<CellController>> cellControllerLists = new ArrayList();
    public LatticeModel model;
    public LatticeView view;
    private final ChangeListener<Number> modelListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
        setUV(model.getUCount(), model.getVCount());
        refresh();
    };

    public LatticeController(LatticeModel model, LatticeView view) {
        this.model = model;
        this.view = view;
        this.model.releaseProperty().addListener(modelListener);
        setUV(model.getUCount(), model.getVCount());
        refresh();
    }

    protected void refresh() {
        setCorners();
        buildView();

    }

    public final void buildView() {
        for (int i = 0; i < cellControllerLists.size(); i++) {
            view.cellViews.getChildren().add(new Group());
            for (int j = 0; j < cellControllerLists.get(i).size(); j++) {
                Group curGroup = (Group) view.cellViews.getChildren().get(i);
                if (!curGroup.getChildren().contains(cellControllerLists.get(i).get(j).view)) {
                    curGroup.getChildren().add(cellControllerLists.get(i).get(j).view);
                }
            }
        }
    }

    @Override
    public String toString() {
        String val = "\n[" + super.toString() + "]" + "\n>>";
        for (int i = 0; i < cellControllerLists.size(); i++) {
            for (int j = 0; j < cellControllerLists.get(i).size(); j++) {
                CellController cellController = cellControllerLists.get(i).get(j);
                val += "\n (" + (cellController.toString() + ") of Index<" + i + "," + j + "> ");
            }
        }
        return val + "\n<<";
    }

    @Override
    protected void setUV(int newCountI, int newCountJ) {
        setU(newCountI);
        setV(newCountJ);
    }

    @Override
    protected void setU(int newCount) {

        if (newCount > cellControllerLists.size()) {
            addU(newCount);
        } else {
            removeU(newCount);
        }
    }

    @Override
    protected void addU(int newCount) {
        for (int u = cellControllerLists.size(); u < newCount; u++) {
            cellControllerLists.add(new ArrayList());
            for (int v = 0; v < cellControllerLists.get(0).size(); v++) {
                addElement(u, v);
            }
        }
    }

    @Override
    protected void removeU(int newCount) {
        while (cellControllerLists.size() > newCount && cellControllerLists.size() > 0) {
            cellControllerLists.get(cellControllerLists.size() - 1).stream().forEach((controller) -> {
                controller.dispose();
            });
            cellControllerLists.remove(cellControllerLists.size() - 1);
        }

    }

    @Override
    protected void setV(int newCount) {
        if (newCount > cellControllerLists.get(0).size()) {
            addV(newCount);
        } else {
            removeV(newCount);
        }
    }

    @Override
    protected void removeV(int newCount) {
        cellControllerLists.stream().forEach(list -> {
            while (list.size() > newCount) {
                CellController controller = list.get(list.size() - 1);
                controller.dispose();
                list.remove(controller);
            }
        });

    }

    @Override
    protected void addV(int newCount) {
        for (int u = 0; u < cellControllerLists.size(); u++) {
            for (int v = cellControllerLists.get(u).size(); v < newCount; v++) {
                addElement(u, v);
            }
        }
    }

    @Override
    protected void addElement(int i, int j) {
        CellController cellController = new CellController(model.cellModelLists.get(i).get(j), new CellView(model.cellModelLists.get(i).get(j)));
        cellController.view.boundaryLoaded.bind(view.boundaryLoaded);
        cellController.view.diagonalAxesLoaded.bind(view.diagonalAxesLoaded);
        cellController.view.medianAxesLoaded.bind(view.medianAxesLoaded);
        cellController.view.internalAxesLoaded.bind(view.internalAxesLoaded);
        cellController.view.orthoAxesLoaded.bind(view.orthoAxesLoaded);
        cellController.view.sendViewToBack();
        cellControllerLists.get(i).add(cellController);
    }

    private void setCorners() {
        uMinvMinTileControllerProperty.set(cellControllerLists.get(0).get(0));
        uMinvMaxTileControllerProperty.set(cellControllerLists.get(0).get(model.getVCount() - 1));
        uMaxvMinTileControllerProperty.set(cellControllerLists.get(cellControllerLists.size() - 1).get(0));
        uMaxvMaxTileControllerProperty.set(cellControllerLists.get(model.getUCount() - 1).get(model.getVCount() - 1));
    }

    @Override
    public final void dispose() {
        this.model.releaseProperty().removeListener(modelListener);
        this.cellControllerLists.forEach(list -> {
            list.forEach(controller -> {
                controller.dispose();
            });
        });
        this.model.dispose();
        this.view.dispose();
        this.view.getChildren().clear();
        this.cellControllerLists.clear();

        nullify();
    }

    private void nullify() {
        this.model = null;
        this.view = null;
        this.cellControllerLists = null;
    }

}
