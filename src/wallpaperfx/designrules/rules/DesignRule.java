/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.designrules.rules;

import wallpaperfx.design.core.Enums.SymmetryModelType;
import wallpaperfx.designrules.artfactory.ArtFactory;

/**
 *
 * @author Maher Elkhaldi
 */
public class DesignRule {

    public final Context context;
    public final boolean overriding;
    public final ArtFactory artFactory;
    public final SymmetryModelType symmetryType;
    public final boolean adaptive;
    /**
     *
     * @param context
     * @param symmetryType
     * @param overriding
     * @param adaptive
     * @param artFactory
     */
    public DesignRule(SymmetryModelType symmetryType, DesignContextBasic context, boolean overriding, boolean adaptive, ArtFactory artFactory) {
        this.symmetryType = symmetryType;
        this.overriding = overriding;
        this.adaptive = adaptive;
        this.context = context;
        this.artFactory = artFactory;
    }

    public DesignRule(SymmetryModelType symmetryType, DesignContextCA context, boolean overriding, boolean adaptive, ArtFactory artFactory) {
        this.symmetryType = symmetryType;
        this.overriding = overriding;
        this.adaptive = adaptive;
        this.context = context;
        this.artFactory = artFactory;
    }

    public boolean equals(DesignRule anotherRule) {
        return anotherRule.symmetryType == this.symmetryType
                && anotherRule.adaptive == this.adaptive
                && anotherRule.context.equals(this.context);
    }

}
