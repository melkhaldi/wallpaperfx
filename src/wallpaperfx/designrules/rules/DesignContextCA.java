/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wallpaperfx.designrules.rules;

import wallpaperfx.design.artifact.wallpaper.WallpaperController;
import wallpaperfx.design.core.Enums.SymmetryModelType;
import wallpaperfx.designrules.artfactory.AnyArtFactory;
import wallpaperfx.designrules.artfactory.ArtFactory;

/**
 *
 * @author melkhaldi
 */
public class DesignContextCA extends Context {

    public final SymmetryModelType symmetryBefore, symmetryCurrent, symmetryAfter;
    public final ArtFactory beforeArtfactory, currentArtfactory, afterArtfactory;

    /**
     *
     * @param symmetryBefore
     * @param symmetryCurrent
     * @param symmetryAfter
     * @param artfactoryBefore
     * @param artfactoryCurrent
     * @param artfactoryAfter
     */
    public DesignContextCA(SymmetryModelType symmetryBefore, SymmetryModelType symmetryCurrent, SymmetryModelType symmetryAfter, ArtFactory artfactoryBefore, ArtFactory artfactoryCurrent, ArtFactory artfactoryAfter) {
        this.symmetryBefore = symmetryBefore;
        this.symmetryCurrent = symmetryCurrent;
        this.symmetryAfter = symmetryAfter;
        this.beforeArtfactory = artfactoryBefore;
        this.currentArtfactory = artfactoryCurrent;
        this.afterArtfactory = artfactoryAfter;
    }

    @Override
    public boolean equals(Context otherContext) {
        if (otherContext.getClass().equals(this.getClass())) {
            DesignContextCA context = (DesignContextCA) otherContext;
            return context.symmetryBefore == this.symmetryBefore && context.symmetryCurrent == this.symmetryCurrent && this.symmetryAfter == context.symmetryAfter && this.beforeArtfactory == context.beforeArtfactory && this.currentArtfactory == context.currentArtfactory && this.afterArtfactory == context.afterArtfactory;

        } else {
            return false;
        }
    }

    @Override
    public boolean isSatisfied(int i, int j, WallpaperController wallpaperController) {
        int maxJ = wallpaperController.tileControllerLists.get(0).size() - 1;
        int jNext, jBefore;
        jNext = Math.min(maxJ, j + 1);
        jBefore = Math.max(0, j - 1);

        SymmetryModelType beforeSymmetry = wallpaperController.tileControllerLists.get(i).get(jBefore).model.designRule.symmetryType;
        ArtFactory beforeArtFactory = wallpaperController.tileControllerLists.get(i).get(jBefore).model.designRule.artFactory;

        SymmetryModelType currentSymmetry = wallpaperController.tileControllerLists.get(i).get(j).model.designRule.symmetryType;
        ArtFactory currentArtFactory = wallpaperController.tileControllerLists.get(i).get(j).model.designRule.artFactory;

        SymmetryModelType afterSymmetry = wallpaperController.tileControllerLists.get(i).get(jNext).model.designRule.symmetryType;
        ArtFactory afterArtFactory = wallpaperController.tileControllerLists.get(i).get(jNext).model.designRule.artFactory;
        
        boolean sBefore = beforeSymmetry.equals(this.symmetryBefore);
        boolean sCurrent = currentSymmetry.equals(this.symmetryCurrent);
        boolean sAfter = afterSymmetry.equals(this.symmetryAfter);
        boolean aBefore, aCurrent, aAfter;
        if (this.beforeArtfactory.getClass().equals(AnyArtFactory.class)) {
            aAfter = true;
        } else {
            aAfter = afterArtFactory.equals(this.afterArtfactory);
        }
        if (this.currentArtfactory.getClass().equals(AnyArtFactory.class)) {
            aCurrent = true;
        } else {
            aCurrent = currentArtFactory.equals(this.currentArtfactory);
        }
        if (this.afterArtfactory.getClass().equals(AnyArtFactory.class)) {
            aBefore = true;
        } else {
            aBefore = beforeArtFactory.equals(this.beforeArtfactory);
        }
        return (sBefore && sAfter && sCurrent
                && aAfter && aBefore && aCurrent);

    }

}
