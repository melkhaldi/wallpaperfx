/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.designrules.rules;

import wallpaperfx.design.core.Enums.SymmetryModelType;
import static wallpaperfx.design.core.Enums.SymmetryModelType.*;
import wallpaperfx.designrules.artfactory.ArtFactory;

/**
 * @author Maher Elkhaldi
 */
public class DesignRulesFactory {

    public static DesignRule getSolidDesignRule(SymmetryModelType type, boolean overriding, boolean adaptiveAngle, ArtFactory factory) {
        return new DesignRule(type, new DesignContextBasic(1, 0, 1, 0), overriding, adaptiveAngle, factory);
    }

    public static DesignRule getSolidAnyDesignRule(boolean overriding, boolean adaptiveAngle, ArtFactory factory) {
        int random = (int) ((Math.random()) * 12);
        switch (random) {
            case 0:
                return new DesignRule(P1, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 1:
                return new DesignRule(P2, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 2:
                return new DesignRule(P3, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 3:
                return new DesignRule(P31M, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 4:
                return new DesignRule(P3M1, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 5:
                return new DesignRule(P4, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 6:
                return new DesignRule(P4M, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 7:
                return new DesignRule(P4MG, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 8:
                return new DesignRule(P6, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 9:
                return new DesignRule(P6M, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 10:
                return new DesignRule(PG, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 11:
                return new DesignRule(PM, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            case 12:
                return new DesignRule(EMPTY, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
            default:
                return new DesignRule(P1, new DesignContextBasic(1, 0, 1, 0), adaptiveAngle, overriding, factory);
        }
    }

    public static DesignRule[] getCheckeredDesignRuleSet(SymmetryModelType solidType,  ArtFactory solidFactory, SymmetryModelType voidType, ArtFactory voidFactory) {
        DesignRule solidRule1 = new DesignRule(solidType, new DesignContextBasic(2, 0, 2, 0), true, true, solidFactory);
        DesignRule voidRule1 = new DesignRule(voidType, new DesignContextBasic(2, 1, 2, 0), true, true, voidFactory);
        DesignRule solidRule2 = new DesignRule(solidType, new DesignContextBasic(2, 1, 2, 1), true, true, solidFactory);
        DesignRule voidRule2 = new DesignRule(voidType, new DesignContextBasic(2, 0, 2, 1), true, true, voidFactory);

        DesignRule rules[] = {solidRule1, voidRule1, solidRule2, voidRule2};
        return rules;
    }

    
}
