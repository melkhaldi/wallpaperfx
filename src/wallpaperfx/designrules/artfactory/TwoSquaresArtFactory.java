/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.designrules.artfactory;

import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 *
 * @author Maher Elkhaldi
 */
public class TwoSquaresArtFactory extends ArtFactory {

    double sideLength = 10;
    String cssStyle = "-fx-fill: rgb(" + (int) (Math.random() * 255) + "," + (int) (Math.random() * 255) + "," + (int) (Math.random() * 255) + ");" ;

    public TwoSquaresArtFactory() {
    }

    public TwoSquaresArtFactory(double sideLength, String cssStyle) {
        this.sideLength = sideLength;
    }

    public TwoSquaresArtFactory(double sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public Shape[] build() {
        Rectangle sqr1 = new Rectangle(sideLength, sideLength);
        Rectangle sqr2 = new Rectangle(sideLength, sideLength);
        sqr2.setLayoutY(sideLength);
        sqr2.setLayoutX(sideLength / 2);

        sqr1.setStyle(cssStyle);
        sqr2.setStyle(cssStyle);
        Shape shapes[] = {sqr1, sqr2};
        return shapes;
    }

    @Override
    public boolean equals(ArtFactory anotherFactory) {
        if (anotherFactory.getClass().equals(this.getClass())) {
            TwoSquaresArtFactory aF = (TwoSquaresArtFactory) anotherFactory;
            return this.sideLength == aF.sideLength && cssStyle.equals(aF.cssStyle);
        } else {
            return false;
        }
    }

   

}
