/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.designrules.artfactory;

import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

/**
 *
 * @author Maher Elkhaldi
 */
public class ASCIArtFactory extends ArtFactory {

    String textValue = "F";
    String cssBaseStyle;

    public ASCIArtFactory() {
    }

    public ASCIArtFactory(Text text, String cssStyle) {
        this.textValue = text.getText();
        this.cssBaseStyle=cssStyle;
    }

    public ASCIArtFactory(Text text) {
        this.textValue = text.getText();
    }

    public void setText(String textValue) {
        this.textValue = textValue;
    }

    @Override
    public Shape[] build() {
        Text text = new Text(textValue);
        text.setStyle(cssBaseStyle);
        Shape shapes[] = {text};
        return shapes;
    }

    @Override
    public boolean equals(ArtFactory anotherFactory) {
        if (anotherFactory.getClass().equals(this.getClass())) {
            ASCIArtFactory aF = (ASCIArtFactory) anotherFactory;
            return this.cssBaseStyle.equals(aF.cssBaseStyle) && this.textValue.equals(aF.textValue);
        } else {
            return false;
        }
    }

 

}
