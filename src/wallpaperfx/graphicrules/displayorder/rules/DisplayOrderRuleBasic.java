/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.graphicrules.displayorder.rules;

import wallpaperfx.design.core.Enums.Order;
import wallpaperfx.graphicrules.css.rules.CssBasicContext;

/**
 *
 * @author Maher Elkhaldi
 */
public class DisplayOrderRuleBasic extends DisplayOrderRule{

   protected CssBasicContext context;
   
    public DisplayOrderRuleBasic(Order order, int[] itemSeries){
        super(order, true, itemSeries);
         
        this.context = new CssBasicContext( 1, 0, 1, 0);
       
    }
    
    
    public DisplayOrderRuleBasic(Order order, boolean overriding, CssBasicContext context, int[] itemSeries) {
        super(order, overriding, itemSeries);
        this.context = context;
    }
    
    
   

    public boolean equals(DisplayOrderRuleBasic anotherRule) {
        return anotherRule.getClass() == this.getClass()
                && anotherRule.order.equals(order)
                && anotherRule.context.equals(this.context);
    }
    
}
