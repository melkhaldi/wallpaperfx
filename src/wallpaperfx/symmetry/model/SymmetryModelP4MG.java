/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wallpaperfx.symmetry.model;

import wallpaperfx.design.artifact.artwork.ArtworkSeed;
import wallpaperfx.design.core.Enums;
import static wallpaperfx.design.core.Enums.Axis.IJ_AXIS;
import static wallpaperfx.design.core.Enums.Axis.IK_AXIS;
import static wallpaperfx.design.core.Enums.Axis.JK_AXIS;
import static wallpaperfx.design.core.Enums.Axis.KL_AXIS;
import static wallpaperfx.design.core.Enums.Axis.LI_AXIS;
import static wallpaperfx.design.core.Enums.Pivot.CPOINT;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P4MG;
import wallpaperfx.design.structure.cell.CellModel;

public final class SymmetryModelP4MG extends SymmetryModel {

                    public final Enums.SymmetryModelType type = P4MG;

    public SymmetryModelP4MG(CellModel cellModel, boolean angleAdaptive) {
        super(cellModel, P4MG, angleAdaptive);

    }

    @Override
    public final void setArtworkSeeds() {
         artworkSeeds.add(new ArtworkSeed(symmetryModelType,CPOINT, 0,              IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT,  Math.PI / 2,   IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT,  Math.PI,       IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT,  3*Math.PI/2,   IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT,  0,             IJ_AXIS, true,  angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT,  Math.PI / 2,   JK_AXIS, true,  angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT,  Math.PI,       KL_AXIS, true,  angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT,  3*Math.PI/2,   LI_AXIS, true,  angleAdaptive, true));

    }

}
