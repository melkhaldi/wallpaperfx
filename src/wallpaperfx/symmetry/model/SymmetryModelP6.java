/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.symmetry.model;

import wallpaperfx.design.artifact.artwork.ArtworkSeed;
import wallpaperfx.design.core.Enums;
import static wallpaperfx.design.core.Enums.Axis.IK_AXIS;
import static wallpaperfx.design.core.Enums.Pivot.MPOINT;
import static wallpaperfx.design.core.Enums.SymmetryModelType.P6;
import wallpaperfx.design.structure.cell.CellModel;

public final class SymmetryModelP6 extends SymmetryModel {

    public final Enums.SymmetryModelType type = P6;

    public SymmetryModelP6(CellModel cellModel, boolean angleAdaptive) {
        super(cellModel, P6, angleAdaptive);
    }

    @Override
    public final void setArtworkSeeds() {
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, MPOINT, Math.PI / 3, IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, MPOINT, 2 * Math.PI / 3, IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, MPOINT, Math.PI, IK_AXIS, false, angleAdaptive, true));

        artworkSeeds.add(new ArtworkSeed(symmetryModelType, MPOINT, 4 * Math.PI / 3, IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, MPOINT, 5 * Math.PI / 3, IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, MPOINT, 2 * Math.PI, IK_AXIS, false, angleAdaptive, true));

    }

}
