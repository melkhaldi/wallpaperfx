/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF RANDOM KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR RANDOM CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.symmetry.model;

import wallpaperfx.design.core.Enums.SymmetryModelType;
import wallpaperfx.design.structure.cell.CellModel;

/**
 *
 * @author Maher Elkhaldi
 */
public class SymmetryModelFactory {

    public static SymmetryModel getSymmetryModel(CellModel cellModel, SymmetryModelType type, boolean angleAdaptive) {
        switch (type) {
            case P1:
                return new SymmetryModelP1(cellModel, angleAdaptive);
            case P2:
                return new SymmetryModelP2(cellModel, angleAdaptive);
            case P3:
                return new SymmetryModelP3(cellModel, angleAdaptive);
            case P3M1:
                return new SymmetryModelP3M1(cellModel, angleAdaptive);
            case P31M:
                return new SymmetryModelP31M(cellModel, angleAdaptive);
            case P4:
                return new SymmetryModelP4(cellModel, angleAdaptive);
            case P4M:
                return new SymmetryModelP4M(cellModel, angleAdaptive);
            case P4MG:
                return new SymmetryModelP4MG(cellModel, angleAdaptive);
            case P6:
                return new SymmetryModelP6(cellModel, angleAdaptive);
            case P6M:
                return new SymmetryModelP6M(cellModel, angleAdaptive);
            case PM:
                return new SymmetryModelPM(cellModel, angleAdaptive);
            case PMG:
                return new SymmetryModelPMG(cellModel, angleAdaptive);
            case PMM:
                return new SymmetryModelPMM(cellModel, angleAdaptive);
            case CMM:
                return new SymmetryModelCMM(cellModel, angleAdaptive);
            case CM:
                return new SymmetryModelCM(cellModel, angleAdaptive);          
            case PG:
                return new SymmetryModelPG(cellModel, angleAdaptive);
            case PGG:
                return new SymmetryModelPGG(cellModel, angleAdaptive);
            case EMPTY:
                return new SymmetryModelEMPTY(cellModel, angleAdaptive);
            default:
                return new SymmetryModelP1(cellModel, angleAdaptive);
        }
    }
}
