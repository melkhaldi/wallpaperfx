/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wallpaperfx.symmetry.model;

import wallpaperfx.design.artifact.artwork.ArtworkSeed;
import wallpaperfx.design.core.Enums;
import static wallpaperfx.design.core.Enums.Axis.IK_AXIS;
import static wallpaperfx.design.core.Enums.Pivot.CLPOINT;
import static wallpaperfx.design.core.Enums.Pivot.CRPOINT;
import static wallpaperfx.design.core.Enums.SymmetryModelType.PMG;
import wallpaperfx.design.structure.cell.CellModel;

/**
 *
 * @author melkhaldi
 */
public class SymmetryModelPMG extends SymmetryModel {

    public final Enums.SymmetryModelType type = PMG;

    public SymmetryModelPMG(CellModel cellModel, boolean angleAdaptive) {
        super(cellModel, PMG, angleAdaptive);
    }

    @Override
    protected void setArtworkSeeds() {
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CRPOINT, 0, IK_AXIS, true, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CLPOINT, Math.PI, IK_AXIS, true, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CRPOINT, 0, IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CLPOINT, Math.PI, IK_AXIS, false, angleAdaptive, true));
    }

}
