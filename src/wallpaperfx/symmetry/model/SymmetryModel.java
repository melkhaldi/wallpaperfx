/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.symmetry.model;

import java.util.ArrayList;
import wallpaperfx.design.artifact.artwork.ArtworkSeed;
import wallpaperfx.design.core.Enums.SymmetryModelType;
import wallpaperfx.design.structure.cell.CellModel;

/**
 *
 * @author Maher Elkhaldi
 */
public abstract class SymmetryModel {

    public final CellModel cellModel;
    
    public final ArrayList<ArtworkSeed> artworkSeeds = new ArrayList();
    public final SymmetryModelType symmetryModelType;
    public final boolean angleAdaptive;
    public SymmetryModel(CellModel cellModel, SymmetryModelType symmetryModelType, boolean liveUpdate) {
        this.cellModel = cellModel;
        this.symmetryModelType = symmetryModelType;
        this.angleAdaptive=(liveUpdate);
        setArtworkSeeds();
        
    }


    protected abstract void setArtworkSeeds();
    
    public void dispose(){
        artworkSeeds.stream().forEach(motifSeed->motifSeed.dispose());
        artworkSeeds.clear();
    }
    

}
