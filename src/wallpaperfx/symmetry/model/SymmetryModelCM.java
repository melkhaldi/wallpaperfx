/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wallpaperfx.symmetry.model;

import wallpaperfx.design.artifact.artwork.ArtworkSeed;
import wallpaperfx.design.core.Enums;
import static wallpaperfx.design.core.Enums.Axis.IK_AXIS;
import static wallpaperfx.design.core.Enums.Pivot.CPOINT;
import static wallpaperfx.design.core.Enums.SymmetryModelType.CM;
import wallpaperfx.design.structure.cell.CellModel;

public final class SymmetryModelCM extends SymmetryModel {

    public final Enums.SymmetryModelType type = CM;

    public SymmetryModelCM(CellModel cellModel, boolean angleAdaptive) {
        super(cellModel, CM, angleAdaptive);
    }

    @Override
    public final void setArtworkSeeds() {
            artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT, 0, IK_AXIS, false, angleAdaptive, true));
        artworkSeeds.add(new ArtworkSeed(symmetryModelType, CPOINT, 0, IK_AXIS, true, angleAdaptive, true));

    }

}
