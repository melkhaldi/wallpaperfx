/*
 * The MIT License
 *
 * Copyright 2016 Maher Elkhaldi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package wallpaperfx.display;

import javafx.scene.Group;
import wallpaperfx.design.control.CellDesignControl;
import wallpaperfx.design.control.LatticeDesignControl;
import wallpaperfx.design.control.MotifDesignControl;
import wallpaperfx.design.control.WallpaperDesignControl;

/**
 *
 * @author Maher Elkhaldi
 */
public class ControlPane extends Group {

    public ControlPane() {

    }

    public void addWallpaperGFXController(WallpaperDesignControl control) {
        this.getChildren().add(control);

    }

    public void removeWallpaperGFXController(WallpaperDesignControl control) {
        this.getChildren().remove(control);
    }

    public void addMotifGFXController(MotifDesignControl control) {
        this.getChildren().add(control);
    }

    public void removeMotifGFXController(MotifDesignControl control) {
        this.getChildren().remove(control);
    }

    public void addCellGFXController(CellDesignControl control) {
        this.getChildren().add(control);
       
    }

    public void removeCellGFXController(CellDesignControl control) {
        this.getChildren().remove(control);
   

    }

    public void addLatticeGFXController(LatticeDesignControl control) {
        this.getChildren().add(control);
        
    }

    public void removeLatticeGFXController(LatticeDesignControl control) {
        this.getChildren().remove(control);
        
    }

}
